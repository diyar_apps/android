package kw.gov.paci.PACIMobileID.utils;

import kw.gov.paci.PACIMobileID.BuildConfig;

public interface Constants {
    String GET_PDF_URL = BuildConfig.GET_PDF_URL;
    String mBearer = "bearer ";
    String ANDROID_APP_VERSION = "Android";
}
