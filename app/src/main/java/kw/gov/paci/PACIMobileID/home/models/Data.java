
package kw.gov.paci.PACIMobileID.home.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("ServiceProviderId")
    @Expose
    private String serviceProviderId;
    @SerializedName("PromptAr")
    @Expose
    private String promptAr;
    @SerializedName("PromptEn")
    @Expose
    private String promptEn;
    @SerializedName("RequestChannelId")
    @Expose
    private Integer requestChannelId;
    @SerializedName("PersonCivilNo")
    @Expose
    private String personCivilNo;
    @SerializedName("AdditionalSPData")
    @Expose
    private String additionalSPData;
    @SerializedName("Challenge")
    @Expose
    private String challenge;
    @SerializedName("CallbackURL")
    @Expose
    private String callbackURL;
    @SerializedName("ResponseData")
    @Expose
    private String responseData;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;
    @SerializedName("CreatedAt")
    @Expose
    private String createdAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getPromptAr() {
        return promptAr;
    }

    public void setPromptAr(String promptAr) {
        this.promptAr = promptAr;
    }

    public String getPromptEn() {
        return promptEn;
    }

    public void setPromptEn(String promptEn) {
        this.promptEn = promptEn;
    }

    public Integer getRequestChannelId() {
        return requestChannelId;
    }

    public void setRequestChannelId(Integer requestChannelId) {
        this.requestChannelId = requestChannelId;
    }

    public Object getPersonCivilNo() {
        return personCivilNo;
    }

    public void setPersonCivilNo(String personCivilNo) {
        this.personCivilNo = personCivilNo;
    }

    public Object getAdditionalSPData() {
        return additionalSPData;
    }

    public void setAdditionalSPData(String additionalSPData) {
        this.additionalSPData = additionalSPData;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getCallbackURL() {
        return callbackURL;
    }

    public void setCallbackURL(String callbackURL) {
        this.callbackURL = callbackURL;
    }

    public Object getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
