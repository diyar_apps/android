
package kw.gov.paci.PACIMobileID.splash.models;

/**
 * Created by VAMANPALLI SUNIL KUMAR on 09/08/2018.
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceStautsModel {

    @SerializedName("Error")
    @Expose
    private Object error;
    @SerializedName("Data")
    @Expose
    private Data data;

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
