package kw.gov.paci.PACIMobileID.feedback.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.feedback.controllers.FeedbackMobileCivilIDInteractionListener;

/**
 * Created by SUNIL KUMAR V on 11/9/2018.
 */
public class FeedBackHome extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public EditText mFeedbackEdittext;

    private FeedbackMobileCivilIDInteractionListener mListener;

    public FeedBackHome() {
        // Required empty public constructor
    }

    public static FeedBackHome newInstance() {
        FeedBackHome fragment = new FeedBackHome();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FeedbackMobileCivilIDInteractionListener) {
            mListener = (FeedbackMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FeedbackMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.about_mobile_civilid_home_header, container, false);
        mHeader.addView(header);
        mHeader.setVisibility(View.VISIBLE);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.feed_back_home_fragment, container, false);
        mFeedbackEdittext =profileBody.findViewById(R.id.ed_text_feedback);
        mFeedbackEdittext.setSelection(0);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mBody.setVisibility(View.GONE);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.INVISIBLE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.qr_code_civil_id:
                break;*/
        }
    }

    public void setImageResource() {

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}

