package kw.gov.paci.PACIMobileID.home.views.fragments.DigitalSign;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostUpdateSigningRequest.Data;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostUpdateSigningRequest.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostUpdateSigningRequest.MyIDpostUpdateSigningRequestModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.LoginActivityScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddPinSuccessfulScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.HeadlessUnlockPinCallbackHandler;
import kw.gov.paci.PACIMobileID.utils.CSRHelper;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomEditTextView;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.fxn.stash.Stash;
import com.google.gson.JsonObject;
import com.intercede.myIDSecurityLibrary.IPinCallback;
import com.intercede.myIDSecurityLibrary.IReturnUserPin;
import com.intercede.myIDSecurityLibrary.ISigningResult;
import com.intercede.myIDSecurityLibrary.InvalidContextException;
import com.intercede.myIDSecurityLibrary.MyIDSecurityLibrary;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationMechanisms;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationParameters;
import com.intercede.myIDSecurityLibrary.MyIdSignDataResponse;
import com.intercede.myIDSecurityLibrary.MyIdSigningParameters;
import com.intercede.myIDSecurityLibrary.SigningAlgorithm;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 11/12/2018.
 */
public class AddDigitalSignDeclinePinScreen extends MobileCivilIDBaseFragment implements View.OnClickListener /*OnPageChangeListener OnLoadCompleteListener*/, ISigningResult, IPinCallback {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public static String mQrData;
    public CustomTextView mPlsEnterHeadertxt, mPinMustBetxt, mItisHighlytxt;
    public LinearLayout mOldPinViewLL, mConfirmPinViewLL, mEnterPinViewLL;
    public CustomButton mConfirmRequestPinBtn;
    public SharedPreferences preferences, preferencese_splash;
    public SharedPreferences.Editor editor, editor_splash;
    public String mConfirmPinStr;
    public CustomEditTextView mEnterPinValue;
    public RetroApiInterface apiService;
    public String mDeviceIdValue;
    public String sign;
    public CustomEditTextView mConfirmPInED;
    byte[] sigBytes;
    public String failStatus;
    public String mChallengeIDValue;
    public String mIDValue;
    public static String mFileNamePath1;
    public Boolean mCancelDigitalSignStatus;
    private boolean mIsLoginEnable = true;
    private int mCurrentLoginFailCount = 0;
    private Long mLastEnteredWrongPassword = 0L;
    public String mCurrentLoginFailCountShared_pref;
    //private ProgressDialog pDialog;
    public LinearLayout mProgressbarll;
    public static int SPLASH_TIME_OUT = 1000;
    MyIDSecurityLibrary myIDSecurityLibrary;
    MyIdSigningParameters signingParameters;
    MyIdAuthenticationParameters authenticationParameters;
    public String mAddDigiSignDeclinePinStatus = "mAddDigiSignDeclinePinStatus";
    public String mAddDigiSignDeclinePinCancelStatus = "mAddDigiSignDeclinePinCancelStatus";
    String authorzation_tokentype, mSerialNumber, CivilNo;
    static HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;
    Boolean pinlockedBooleanType;
    private ArrayList<AddActivityLogModel> addActivityLogModelsArrayList;
    public static final String ACTIVITY_LOGS_DIGITAL = "ACTIVITY_LOGS_DIGITAL";
    public static final String ACTIVITY_LOGS_DIGITAL_FAIL = "ACTIVITY_LOGS_DIGITAL_FAIL";
    private AlertDialog pinDialog;
    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";
    int count;

    public AddDigitalSignDeclinePinScreen() {
        // Required empty public constructor
    }

    public static AddDigitalSignDeclinePinScreen newInstance(String msg, String mFileNamePath) {
        AddDigitalSignDeclinePinScreen fragment = new AddDigitalSignDeclinePinScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
            mFileNamePath1 = mFileNamePath;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_home_enter_pin, container, false);
        mPlsEnterHeadertxt = profileBody.findViewById(R.id.header_enter_pin_edtxt);
        mEnterPinValue = profileBody.findViewById(R.id.et_new_pin);
        mConfirmPInED = profileBody.findViewById(R.id.et_confrm_pin);
        mConfirmRequestPinBtn = profileBody.findViewById(R.id.confirm_request_pin);
        mItisHighlytxt = profileBody.findViewById(R.id.it_is_highly_txt);
        mPinMustBetxt = profileBody.findViewById(R.id.pin_must_txt);
        mOldPinViewLL = profileBody.findViewById(R.id.pin_old_layout_root);
        mConfirmPinViewLL = profileBody.findViewById(R.id.pin_confrm_layout_root);
        mEnterPinViewLL = profileBody.findViewById(R.id.pin_new_layout_root);
        mProgressbarll = profileBody.findViewById(R.id.progress_bar_ll);
        mPlsEnterHeadertxt.setText(R.string.AuthoPIN_Content_one);
        mItisHighlytxt.setVisibility(View.INVISIBLE);
        mPinMustBetxt.setVisibility(View.INVISIBLE);
        mConfirmPinViewLL.setVisibility(View.INVISIBLE);
        mOldPinViewLL.setVisibility(View.INVISIBLE);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mConfirmRequestPinBtn.setOnClickListener(this);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_request_pin:
                mConfirmRequestPinBtn.setEnabled(false);
                headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
                headlessUnlockPinCallbackHandler.initialise(getActivity());
                pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
                if (!pinlockedBooleanType) {
                    if (mEnterPinValue.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else if (mEnterPinValue.getText().toString().length() <= 3) {
                        Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else {
                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        mConfirmPinStr = preferences.getString(("mConfirmPinED"), "default");
                        if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                            if (mQrData.equals("ClickedDigiSiginButton")) {
                                mCancelDigitalSignStatus = false;
                                getDataFromServer();
                            } else {
                                mCancelDigitalSignStatus = true;
                                getDataFromServer();
                            }
                        } else {
                            getDataFromServer();
                            // Toast.makeText(getContext(), R.string.Invalid_PIN, Toast.LENGTH_SHORT).show();
                           /* boolean exceptionThrown = false;
                            try {
                                Authentication auth = new Authentication(getActivity());
                                auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                            } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                                e.printStackTrace();
                            } catch (IncorrectPinException e) {
                                exceptionThrown = true;
                                StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                errorMsg.append("\n");
                                errorMsg.append(e.numberOfAttemptsRemaining);
                                errorMsg.append(" attempts remaining.");
                                Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }if (!exceptionThrown) {
                                //result.setText(getString(R.string.pin_successfully_changed));
                                mDigitalSignService(mCancelDigitalSignStatus);

                            }*/
                        }
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mConfirmPinStr = preferences.getString(("mConfirmPinED"), "default");
            if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                mDigitalSignService(mCancelDigitalSignStatus);
            } else {
                mDigitalSignService(mCancelDigitalSignStatus);
             /*   boolean exceptionThrown = false;
                try {
                    Authentication auth = new Authentication(getActivity());
                    auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                } catch (IncorrectPinException e) {
                    exceptionThrown = true;
                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                    errorMsg.append("\n");
                    errorMsg.append(e.numberOfAttemptsRemaining);
                    errorMsg.append(" attempts remaining.");
                    //result.setText(errorMsg);
                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    exceptionThrown = true;
                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                    errorMsg.append("\n");
                    errorMsg.append(e.getLocalizedMessage());
                    //result.setText(errorMsg);
                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                }
                if (!exceptionThrown) {
                    //result.setText(getString(R.string.pin_successfully_changed));
                    mDigitalSignService(mCancelDigitalSignStatus);

                }*/
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void mDigitalSignService(Boolean mCancelDigitalSignStatus) {
        if (Utilities.isNetworkAvailable(getActivity())) {
            preferencese_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
            editor_splash = preferencese_splash.edit();
            editor_splash.apply();

            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();

            mDeviceIdValue = preferences.getString(("mCarrierID"), ("default"));
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mChallengeIDValue = preferences.getString("mChallaengeIdDigitalSignList", "default");
            mIDValue = preferences.getString("mIDDigitalSignList", "default");
            sigBytes = Base64.decode(mChallengeIDValue, Base64.NO_WRAP);
            headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
            headlessUnlockPinCallbackHandler.initialise(getActivity());
            pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
            if (!pinlockedBooleanType) {
                mMyIDSignData(sigBytes);
            } else {
                Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setImageResource() {
    }

    private void mDigitalSigningRequestSignPost(String mRequestId, String mDeviceID, String
            mSignatureData, int mStatusId) {
        mProgressbarll.setVisibility(View.VISIBLE);
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mSerialNumber, CivilNo);
        Data data_object = new Data(mRequestId, mDeviceID, mSignatureData, mStatusId);
        MyIDpostUpdateSigningRequestModel myIDpostUpdateSigningRequestModel = new MyIDpostUpdateSigningRequestModel(identity, data_object);
        Call<JsonObject> call = apiService.postUpdateSigningRequest(authorzation_tokentype, myIDpostUpdateSigningRequestModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mProgressbarll.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        if (!mCancelDigitalSignStatus) {
                           /* Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.signing_txt), getString(R.string.sign_txt), getString(R.string.sigining_status), getString(R.string.successfully_txt), dateToStr));
                            saveToSharedPreference(addActivityLogModelsArrayList);*/
                            Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_sign), getString(R.string.activity_log_bank_of_sign_status), getString(R.string.activity_log_document_signed_succesfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                            failStatus = "successStatusDigitalSign";
                        } else {
                            Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_sign), getString(R.string.activity_log_bank_of_sign_status), getString(R.string.activity_log_document_declined_succesfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                            failStatus = "failStatusDigitalSign";
                        }
                    } else {
                       /* Date today = new Date();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                        String dateToStr = format.format(today);
                        System.out.println(dateToStr);
                        addActivityLogModelsArrayList = new ArrayList<>();
                        addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.signing_txt), getString(R.string.sign_txt), getString(R.string.sigining_status), getString(R.string.failed_txt), dateToStr));
                        saveToSharedPreference(addActivityLogModelsArrayList);*/
                        Date today = new Date();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                        String dateToStr = format.format(today);
                        System.out.println(dateToStr);
                        MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_sign), getString(R.string.activity_log_bank_of_sign_status), getString(R.string.activity_log_document_declined_succesfully), dateToStr));
                        saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                        failStatus = "failStatusDigitalSign";
                    }
                    AddPinSuccessfulScreen argumentFragment = new AddPinSuccessfulScreen();//Get Fragment Instance
                    Bundle data = new Bundle();//Use bundle to pass data
                    data.putString("failStatus", failStatus);//put string, int, etc in bundle with a key value
                    argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                    setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance(failStatus),
                            AddPinSuccessfulScreen.class.getSimpleName());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                    mProgressbarll.setVisibility(View.GONE);
                    Date today = new Date();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                    String dateToStr = format.format(today);
                    System.out.println(dateToStr);
                    MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_sign), getString(R.string.activity_log_bank_of_sign_status), getString(R.string.activity_log_document_declined_succesfully), dateToStr));
                    saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                    failStatus = "failStatusDigitalSign";
                }
            }
        });
    }

    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }

    /*private void saveToSharedPreferenceFail(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {

        Stash.put(ACTIVITY_LOGS_DIGITAL_FAIL, addActivityLogModelsArrayList);
    }*/

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        try {
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(fragmentContainer, fragment, id);
            //fragmentTransaction.commit();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mMyIDSignData(byte[] sigBytes) {
        try {
            count = 0;
            myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(getActivity()));
            myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
            myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
            myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
            signingParameters = new MyIdSigningParameters();
            signingParameters.algorithm = SigningAlgorithm.PreSHA256_PKCS15_PKCS1;
            authenticationParameters = new MyIdAuthenticationParameters();
            authenticationParameters.permittedAuthentication = MyIdAuthenticationMechanisms.PIN_ONLY;
            authenticationParameters.pinCallback = this;
            // authenticationParameters.pinCallback.provideUserPin(this);
            myIDSecurityLibrary.signData(sigBytes, signingParameters, authenticationParameters, this);
        } catch (InvalidContextException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void signingSuccess(MyIdSignDataResponse signDataResponse) {
        // Log.d("signdataResp", "" + signDataResponse);

        try {
            if (signDataResponse != null) {
                if (signDataResponse.signedData == null) {
                    Toast.makeText(getActivity(), "Data Was Not Signed", Toast.LENGTH_SHORT).show();
                } else {
                    // sign = bytesToHex(signDataResponse.signedData);
                    byte[] signedBytes = signDataResponse.signedData;
                    try {
                        sign = CSRHelper.encryptBASE64(signedBytes);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                    editor = preferences.edit();
                    editor.apply();
                    editor.putString("DigitalSignList", sign);
                    editor.apply();
                    //Log.d("==sign1==", sign);
                    if (signDataResponse.certificateData != null) {
                        // Log.d("==sign1==", ""+signDataResponse.certificateData);
                    } else {
                        Toast.makeText(getActivity(), "No certificate data returned", Toast.LENGTH_SHORT).show();
                    }
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    if (!mCancelDigitalSignStatus) {
                        if (Utilities.isNetworkAvailable(getActivity())) {
                            // mDigitalSigningRequestSignPost(mIDValue, mDeviceIdValue, sign, 2);
                           /* tokenIdentity = new TokenIdentity(getActivity());
                            tokenIdentity.mGetAccessTokenRequest(mAddDigiSignDeclinePinStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
*/
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            if (Utilities.isNetworkAvailable(getActivity())) {
                                mDigitalSigningRequestSignPost(mIDValue, mDeviceIdValue, sign, 2);
                            }
                        }
                    } else {
                        if (Utilities.isNetworkAvailable(getActivity())) {
                            //mDigitalSigningRequestSignPost(mIDValue, mDeviceIdValue, sign, 3);
                           /* tokenIdentity = new TokenIdentity(getActivity());
                            tokenIdentity.mGetAccessTokenRequest(mAddDigiSignDeclinePinCancelStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
*/
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            if (Utilities.isNetworkAvailable(getActivity())) {
                                mDigitalSigningRequestSignPost(mIDValue, mDeviceIdValue, sign, 3);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void signingError(Exception e) {
        Toast.makeText(getActivity(), "Error:" + e, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        if (myIDSecurityLibrary != null) {
            myIDSecurityLibrary.onStop();
            myIDSecurityLibrary = null;
        }

    }

    /*@Override
    public void setResult(String s) {
        Log.d("pass", s);

    }

    @Override
    public boolean isFirstAttempt() {
        return false;
    }

    @Override
    public Integer attemptsRemaining() {
        return null;
    }*/

    @Override
    public void provideUserPin(final IReturnUserPin callback) {
        /*AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        if (!callback.isFirstAttempt()) {
            dialog.setTitle(R.string.Invalid_PIN);
            dialog.setMessage("\n" + callback.attemptsRemaining());
            count = 1;
        } else if (callback.isFirstAttempt()) {
            callback.setResult(mEnterPinValue.getText().toString());
        }
        dialog.setCancelable(false);
        if (count == 1) {
            dialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // pinDialog = null;
                    dialog.cancel();
                    callback.setResult("");
                    dialog.dismiss();
                    dialog.cancel();
                    mConfirmRequestPinBtn.setEnabled(true);
                }
            });
        }
        if (count == 1) {
            dialog.show();
        }
    }*/
        final Dialog alert = new Dialog(Objects.requireNonNull(getActivity()));
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.setContentView(R.layout.custom_dialog_wrong_pin);
        final CustomTextView mCountText = alert.findViewById(R.id.count_remaining);
        final CustomButton mOkBtn = alert.findViewById(R.id.ok_btn);

        if (!callback.isFirstAttempt()) {
            mCountText.setText(String.valueOf(callback.attemptsRemaining()));
            count = 1;
        } else if (callback.isFirstAttempt()) {
            callback.setResult(mEnterPinValue.getText().toString());
        }
        alert.setCancelable(false);
        if (count == 1) {
            mOkBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.cancel();
                    callback.setResult("");
                    alert.dismiss();
                    alert.cancel();
                    mConfirmRequestPinBtn.setEnabled(true);
                }
            });
        }
        if (count == 1) {
            alert.show();
        }
    }
}