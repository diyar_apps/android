
package kw.gov.paci.PACIMobileID.home.models.MyIDpostUpdateSigningRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("RequestId")
    @Expose
    private String requestId;
    @SerializedName("DeviceID")
    @Expose
    private String deviceID;
    @SerializedName("SignatureData")
    @Expose
    private String signatureData;
    @SerializedName("StatusId")
    @Expose
    private int statusId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param statusId
     * @param requestId
     * @param signatureData
     * @param deviceID
     */
    public Data(String requestId, String deviceID, String signatureData, int statusId) {
        super();
        this.requestId = requestId;
        this.deviceID = deviceID;
        this.signatureData = signatureData;
        this.statusId = statusId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getSignatureData() {
        return signatureData;
    }

    public void setSignatureData(String signatureData) {
        this.signatureData = signatureData;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

}
