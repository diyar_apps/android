
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("CivilNo")
    @Expose
    private String civilNo;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("TransactionId")
    @Expose
    private String transactionId;

    /**
     * No args constructor for use in serialization
     */
    public Data() {
    }

    /**
     * @param civilNo
     * @param transactionId
     * @param deviceId
     */
    public Data(String civilNo, String transactionId, String deviceId) {
        super();
        this.civilNo = civilNo;
        this.transactionId = transactionId;
        this.deviceId = deviceId;
    }

    public String getCivilNo() {
        return civilNo;
    }

    public void setCivilNo(String civilNo) {
        this.civilNo = civilNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

}
