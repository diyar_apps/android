package kw.gov.paci.PACIMobileID.home.controllers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.home.models.AuthenSignModel.Datum;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 12/4/2018.
 */
public class AuthenSignNotifiAdapter extends RecyclerView.Adapter<AuthenSignNotifiAdapter.CustomViewHolder> {
    private ArrayList<kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum> mDsigns;
    public Context context;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mNotifiIDValue;
    public static int SERVER_TIME_OUT = 100;

    public AuthenSignNotifiAdapter(ArrayList<kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum> mDsigns, Context context) {
        this.mDsigns = mDsigns;
        this.context = context;
    }

    @Override
    public AuthenSignNotifiAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_recycleview_authen_sign_items, parent, false);

        return new AuthenSignNotifiAdapter.CustomViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final AuthenSignNotifiAdapter.CustomViewHolder holder, final int position) {
        final kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum msign = mDsigns.get(position);
        holder.mChallenge.setText(msign.getChallenge());
        holder.mId.setText(msign.getId());
        holder.mPersonCivilNo.setText(msign.getPersonCivilNo());
        holder.mPromptAr.setText(msign.getPromptAr());
        holder.mPromptEn.setText(msign.getPromptEn());
        holder.mServiceNameEN.setText(msign.getServiceNameEN());
        holder.mServiceNameAR.setText(msign.getServiceNameAR());
        holder.mRID.setText(Integer.toString(msign.getRID()));
        holder.mStatusId.setText(Integer.toString(msign.getStatusId()));
        try {
            preferences = Objects.requireNonNull(context).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mNotifiIDValue = preferences.getString(("mNotifiIDValue"), "0");
            mNotifiIDValue = (mNotifiIDValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        holder.mRootViewLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(context, "position" + position, Toast.LENGTH_SHORT).show();
                String mPropEnValue = msign.getPromptEn();//Integer.toString(msign.getRID());
                String mServiceEnValue = msign.getServiceNameEN();
                String mQrcodeID = msign.getId();
                preferences = Objects.requireNonNull(context).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                editor.putString(("mRIDAuthenList"), (Integer.toString(msign.getRID())));
                editor.putString(("mIdAuthenList"), (msign.getId()));
                editor.putString(("mStatusIdAuthenList"), (Integer.toString(msign.getStatusId())));
                editor.apply();
                Intent intent = new Intent("custom-message-authentication");
                intent.putExtra("mPrompEN", mPropEnValue);
                intent.putExtra("mServiceEN", mServiceEnValue);
                intent.putExtra("mQrcodeID", mQrcodeID);
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        // return mDsigns.size();
        if (null != mDsigns) {
            return mDsigns.size();
        } else {
            return 0;
        }
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        CustomTextView mChallenge, mId, mPersonCivilNo, mPromptAr, mPromptEn, mServiceNameEN, mServiceNameAR, mRID, mStatusId;
        LinearLayout mRootViewLinear;

        CustomViewHolder(View view) {
            super(view);
            mRootViewLinear = view.findViewById(R.id.root_linear_view);
            mChallenge = view.findViewById(R.id.header_challenge);
            mId = view.findViewById(R.id.header_id);
            mPersonCivilNo = view.findViewById(R.id.civilid);
            mPromptAr = view.findViewById(R.id.prompt_ar);
            mPromptEn = view.findViewById(R.id.prompt_en);
            mServiceNameEN = view.findViewById(R.id.service_name_en);
            mServiceNameAR = view.findViewById(R.id.service_name_ar);
            mRID = view.findViewById(R.id.rid);
            mStatusId = view.findViewById(R.id.status_id);
        }
    }
}