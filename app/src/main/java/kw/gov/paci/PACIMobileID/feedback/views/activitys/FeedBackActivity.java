package kw.gov.paci.PACIMobileID.feedback.views.activitys;

import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilID;
import kw.gov.paci.PACIMobileID.feedback.controllers.FeedbackMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.feedback.views.fragments.FeedBackHome;

/**
 * Created by SUNIL KUMAR V on 11/9/2018.
 */
public class FeedBackActivity extends MobileCivilID implements FeedbackMobileCivilIDInteractionListener {
    String message;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_root_view);
        super.onCreate(savedInstanceState);
        setFragment(R.id.root_content, FeedBackHome.newInstance(), FeedBackHome.class.getSimpleName());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            goToPreviousScreen();
        }
    }
}
