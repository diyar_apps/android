
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.CivilIDPersonalDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @SerializedName("Governerate")
    @Expose
    private String governerate;
    @SerializedName("Area")
    @Expose
    private String area;
    @SerializedName("PaciBuildingNumber")
    @Expose
    private String paciBuildingNumber;
    @SerializedName("BuildingType")
    @Expose
    private String buildingType;
    @SerializedName("FloorNumber")
    @Expose
    private String floorNumber;
    @SerializedName("BuildingNumber")
    @Expose
    private String buildingNumber;
    @SerializedName("BlockNumber")
    @Expose
    private String blockNumber;
    @SerializedName("UnitNumber")
    @Expose
    private String unitNumber;
    @SerializedName("StreetName")
    @Expose
    private String streetName;
    @SerializedName("UnitType")
    @Expose
    private String unitType;

    public String getGovernerate() {
        return governerate;
    }

    public void setGovernerate(String governerate) {
        this.governerate = governerate;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getPaciBuildingNumber() {
        return paciBuildingNumber;
    }

    public void setPaciBuildingNumber(String paciBuildingNumber) {
        this.paciBuildingNumber = paciBuildingNumber;
    }

    public String getBuildingType() {
        return buildingType;
    }

    public void setBuildingType(String buildingType) {
        this.buildingType = buildingType;
    }

    public String getFloorNumber() {
        return floorNumber;
    }

    public void setFloorNumber(String floorNumber) {
        this.floorNumber = floorNumber;
    }

    public String getBuildingNumber() {
        return buildingNumber;
    }

    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    public String getBlockNumber() {
        return blockNumber;
    }

    public void setBlockNumber(String blockNumber) {
        this.blockNumber = blockNumber;
    }

    public String getUnitNumber() {
        return unitNumber;
    }

    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

}
