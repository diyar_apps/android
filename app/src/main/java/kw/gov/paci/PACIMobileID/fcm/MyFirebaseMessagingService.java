package kw.gov.paci.PACIMobileID.fcm;

/**
 * Created by SUNIL KUMAR V on 10/2/2018.
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.AboutMobileCivilIDActivity;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    // ALL JSON node names
    private static final String TAG_APS = "aps";
    private static final String TAG_BADGE = "badge";
    private static final String TAG_ALERT = "alert";
    private static final String TAG_SUBTITLE = "subtitle";
    private static final String TAG_TITLE = "title";
    private static final String TAG_BODY = "body";
    private static final String TAG_ID = "Id";
    private static final String TAG_CATEGORY = "category";
    private static final String TAG_AUTHENTICATION = "AUTHENTICATION";
    private static final String TAG_SIGNING = "SIGNING";
    private static final String TAG_CANCEL = "CANCEL";
    private static final String TAG_OTHER = "OTHER";
    public static int SPLASH_TIME_OUT = 3000;

    public String subtitle, title, body, Id, category;
    public int badge;
    private PendingIntent pendingIntent;
    public String mBadgeCount;
    public String mIDValue;
    //public int mTotalCounts;
    public Notification notification;
    public SharedPreferences preferences_pls_wait;
    public SharedPreferences.Editor editor_pls_wait;
    public String mCivilID, mConfirmPinStr;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log.e("DATA", remoteMessage.getData().toString());
        // Check if message contains a data payload.
        try {
            preferences_pls_wait = Objects.requireNonNull(this).getSharedPreferences("plswait", MODE_PRIVATE);
            editor_pls_wait = preferences_pls_wait.edit();
            editor_pls_wait.apply();
            Map<String, String> params = remoteMessage.getData();
            String aps = params.get(TAG_APS);
            JSONObject object = new JSONObject(aps);
            mBadgeCount = object.get("badge").toString();
            mIDValue = object.get("Id").toString();
            String alert = object.getString(TAG_ALERT);
            JSONObject alertObj = new JSONObject(alert);
            category = object.getString(TAG_CATEGORY);
            mCivilID = preferences_pls_wait.getString("CivilNo", "default");
            mConfirmPinStr = preferences_pls_wait.getString("mConfirmPinED", "default");
            sendNotification(alertObj.getString(TAG_BODY), category, mBadgeCount, mIDValue);

        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d("json_exec", e.toString());
        }
        if (remoteMessage.getData().size() > 0) {
           // Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (/* Check if data needs to be processed by long running job */ true) {
                // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }
        }
        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            //Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification());
        }
    }

    @Override
    public void onNewToken(String token) {
        //Log.d(TAG, "Refreshed token: " + token);
        // Write
        preferences_pls_wait = Objects.requireNonNull(this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor_pls_wait = preferences_pls_wait.edit();
        editor_pls_wait.apply();
        editor_pls_wait.putString("TokenID", token);
        editor_pls_wait.apply(); // Or commit if targeting old devices
        sendRegistrationToServer(token);
    }

    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }

    private void handleNow() {
        //Log.d(TAG, "Short lived task is done.");
    }

    private void sendRegistrationToServer(String token) {

    }

    private void sendNotification(String msgBody, String category, String mBadgeCount, String mIDValue) {
        switch (category) {
            case TAG_AUTHENTICATION:
                mAuthenticationScreenListItem(category, mBadgeCount, mIDValue);
                break;
            case TAG_SIGNING:
                mDigitalSignListItemSelect(mBadgeCount, mIDValue);
                break;
            case TAG_CANCEL:
                mCancelScreenSelection(mBadgeCount, mIDValue);
                break;
            case TAG_OTHER:
                mOtherScreenSelection(mBadgeCount, mIDValue);
                break;
            default:
                mOtherScreenSelection(mBadgeCount, mIDValue);
                break;
        }
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.notifi_icon)
                        .setContentTitle(getString(R.string.fcm_message))
                        .setContentText(msgBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);
        }

        assert notificationManager != null;
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void mOtherScreenSelection(String mBadgeCount, String mIDValue) {
        if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
            mClearSharedPref();
            new mInitialScreenHandler().invoke();
        } else {
            Intent intent = new Intent(this, AddHomeActivity.class);
            intent.putExtra("categoryId", TAG_OTHER);
            intent.putExtra("mNotifiBadgeCountNumber", mBadgeCount);
            intent.putExtra("mNotifiIDValue", mIDValue);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }
    }

    private void mCancelScreenSelection(String mBadgeCount, String mIDValue) {
        if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
            mClearSharedPref();
            new mInitialScreenHandler().invoke();
        } else {
            Intent intent = new Intent(this, AddHomeActivity.class);
            intent.putExtra("categoryId", TAG_CANCEL);
            intent.putExtra("mNotifiBadgeCountNumber", mBadgeCount);
            intent.putExtra("mNotifiIDValue", mIDValue);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }
    }

    private void mDigitalSignListItemSelect(String mBadgeCount, String mIDValue) {
        if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
            mClearSharedPref();
            new mInitialScreenHandler().invoke();
        } else {
            Intent intent = new Intent(this, AddHomeActivity.class);
            intent.putExtra("categoryId", TAG_SIGNING);
            intent.putExtra("mNotifiBadgeCountNumber", mBadgeCount);
            intent.putExtra("mNotifiIDValue", mIDValue);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }
    }

    private void mAuthenticationScreenListItem(String category, String mBadgeCount, String mIDValue) {
        if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
            mClearSharedPref();
            new mInitialScreenHandler().invoke();
        } else {
            Intent intent = new Intent(this, AddHomeActivity.class);
            intent.putExtra("categoryId", category);
            intent.putExtra("mNotifiBadgeCountNumber", mBadgeCount);
            intent.putExtra("mNotifiIDValue", mIDValue);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);
        }
    }

    private class mInitialScreenHandler {
        void invoke() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(MyFirebaseMessagingService.this, AboutMobileCivilIDActivity.class);
                    startActivity(intent);
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void mClearSharedPref() {
        preferences_pls_wait = this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
        editor_pls_wait = preferences_pls_wait.edit();
        editor_pls_wait.clear();
        editor_pls_wait.apply();
    }
}
