
package kw.gov.paci.PACIMobileID.home.models.MyIDIntializeUnlockPinModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;
    @SerializedName("SdkChallenge")
    @Expose
    private String sdkChallenge;
    @SerializedName("QrChallenge")
    @Expose
    private String qrChallenge;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param qrChallenge
     * @param sdkChallenge
     * @param transactionID
     * @param deviceId
     */
    public Data(String deviceId, String transactionID, String sdkChallenge, String qrChallenge) {
        super();
        this.deviceId = deviceId;
        this.transactionID = transactionID;
        this.sdkChallenge = sdkChallenge;
        this.qrChallenge = qrChallenge;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getSdkChallenge() {
        return sdkChallenge;
    }

    public void setSdkChallenge(String sdkChallenge) {
        this.sdkChallenge = sdkChallenge;
    }

    public String getQrChallenge() {
        return qrChallenge;
    }

    public void setQrChallenge(String qrChallenge) {
        this.qrChallenge = qrChallenge;
    }

}
