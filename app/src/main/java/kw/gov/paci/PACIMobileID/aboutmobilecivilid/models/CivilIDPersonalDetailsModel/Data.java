
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.CivilIDPersonalDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("MobileCivilIdData")
    @Expose
    private MobileCivilIdData mobileCivilIdData;
    @SerializedName("MobileCredentialSettings")
    @Expose
    private MobileCredentialSettings mobileCredentialSettings;

    public MobileCivilIdData getMobileCivilIdData() {
        return mobileCivilIdData;
    }

    public void setMobileCivilIdData(MobileCivilIdData mobileCivilIdData) {
        this.mobileCivilIdData = mobileCivilIdData;
    }

    public MobileCredentialSettings getMobileCredentialSettings() {
        return mobileCredentialSettings;
    }

    public void setMobileCredentialSettings(MobileCredentialSettings mobileCredentialSettings) {
        this.mobileCredentialSettings = mobileCredentialSettings;
    }

}
