package kw.gov.paci.PACIMobileID.baseclasses;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import com.fxn.stash.Stash;

import java.security.Security;
import java.util.ArrayList;
import java.util.Locale;

import kw.gov.paci.PACIMobileID.baseclasses.ApplicationLifecycleHandler;

/**
 * Created by Sunil Kumar on 09/11/18.
 */

public class MobileCivilIDApplication extends Application {
    public static ArrayList<AddActivityLogModel> mGlobalAddactivitylogArraylist = new ArrayList<AddActivityLogModel>();
    private static Context mContext;
    private Locale mLocale = null;
    public ApplicationLifecycleHandler handler;
    private Activity mCurrentActivity = null;
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    String language;
    private static int pendingNotificationsCount = 0;

    /**
     * Returns the application context
     *
     * @return application context
     */
    public static Context getContext() {
        return mContext;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    public Activity getCurrentActivity() {
        return mCurrentActivity;
    }

    public void setCurrentActivity(Activity mCurrentActivity) {
        this.mCurrentActivity = mCurrentActivity;
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public void onCreate() {
        super.onCreate();
        Stash.init(this);
        mContext = getApplicationContext();
        // mGlobalAddactivitylogArraylist = new ArrayList<AddActivityLogModel>();
        sharedPreferences = getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        updateLocale();
        handler = new ApplicationLifecycleHandler();
        registerActivityLifecycleCallbacks(handler);
        registerComponentCallbacks(handler);
    }

    public static int getPendingNotificationsCount() {
        return pendingNotificationsCount;
    }

    public static void setPendingNotificationsCount(int pendingNotifications) {
        pendingNotificationsCount = pendingNotifications;
    }

    public void updateLocale() {
        language = sharedPreferences.getString(Locale_KeyValue, "");
        if (language.equals("")) {
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
        }
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        mContext.createConfigurationContext(config);
        this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());
    }

    public void attachBaseContext(Context base) {
        super.attachBaseContext(getContextWrap(base));
    }

    private Context getContextWrap(Context base) {
        return MyContextWrapper.wrap(base, BuildConfig.LOCALE);
    }

    /*static {
        Security.insertProviderAt(new org.spongycastle.jce.provider.BouncyCastleProvider(), 1);
    }*/

}

