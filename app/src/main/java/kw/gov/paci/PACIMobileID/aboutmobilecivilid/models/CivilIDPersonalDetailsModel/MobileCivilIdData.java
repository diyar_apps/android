
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.CivilIDPersonalDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileCivilIdData {

    @SerializedName("CivilID")
    @Expose
    private String civilID;
    @SerializedName("FullNameAr")
    @Expose
    private String fullNameAr;
    @SerializedName("FullNameEn")
    @Expose
    private String fullNameEn;
    @SerializedName("BirthDate")
    @Expose
    private String birthDate;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("EmailAddress")
    @Expose
    private String emailAddress;
    @SerializedName("MobileNumber")
    @Expose
    private String mobileNumber;
    @SerializedName("NationalityEn")
    @Expose
    private String nationalityEn;
    @SerializedName("NationalityAr")
    @Expose
    private String nationalityAr;
    @SerializedName("NationalityFlag")
    @Expose
    private String nationalityFlag;
    @SerializedName("BloodGroup")
    @Expose
    private String bloodGroup;
    @SerializedName("Photo")
    @Expose
    private Object photo;
    @SerializedName("GovData")
    @Expose
    private GovData govData;
    @SerializedName("BusinessData")
    @Expose
    private BusinessData businessData;
    @SerializedName("Address")
    @Expose
    private Address address;

    public String getCivilID() {
        return civilID;
    }

    public void setCivilID(String civilID) {
        this.civilID = civilID;
    }

    public String getFullNameAr() {
        return fullNameAr;
    }

    public void setFullNameAr(String fullNameAr) {
        this.fullNameAr = fullNameAr;
    }

    public String getFullNameEn() {
        return fullNameEn;
    }

    public void setFullNameEn(String fullNameEn) {
        this.fullNameEn = fullNameEn;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getNationalityEn() {
        return nationalityEn;
    }

    public void setNationalityEn(String nationalityEn) {
        this.nationalityEn = nationalityEn;
    }

    public String getNationalityAr() {
        return nationalityAr;
    }

    public void setNationalityAr(String nationalityAr) {
        this.nationalityAr = nationalityAr;
    }

    public String getNationalityFlag() {
        return nationalityFlag;
    }

    public void setNationalityFlag(String nationalityFlag) {
        this.nationalityFlag = nationalityFlag;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    public GovData getGovData() {
        return govData;
    }

    public void setGovData(GovData govData) {
        this.govData = govData;
    }

    public BusinessData getBusinessData() {
        return businessData;
    }

    public void setBusinessData(BusinessData businessData) {
        this.businessData = businessData;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
