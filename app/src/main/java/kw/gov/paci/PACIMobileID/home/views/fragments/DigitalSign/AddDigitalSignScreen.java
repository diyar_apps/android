package kw.gov.paci.PACIMobileID.home.views.fragments.DigitalSign;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.Data;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.MyIDGetAllSigningRequests;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel.MyIDGetFilePostModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.MyIDJsonArrayListModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.HeadlessUnlockPinCallbackHandler;
import kw.gov.paci.PACIMobileID.utils.Constants;
import kw.gov.paci.PACIMobileID.utils.CustomDialogWindowBlock;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import info.guardianproject.netcipher.NetCipher;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/11/2018.
 */
public class AddDigitalSignScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public CustomTextView mFullNameEN, mCivilIDNum, mPinlocked_bold;
    public SharedPreferences preferences, sharedPreferences;
    public SharedPreferences.Editor editor, editors;
    public String mCivilIDStr, mFullNameStr;
    private ArrayList<kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum> mDSignList;
    //private ProgressDialog pDialog;
    private RecyclerView recyclerView;
    private DigitalSignAdapter eAdapter;
    public RetroApiInterface apiService;
    private String data = null;
    public String ItemName;
    public String mFileName;
    public ResponseBody mResponsebody;
    public String mPublicURL = Constants.GET_PDF_URL;
    public ImageView mProfilePic;
    public String mProfilePhoto;
    public Bitmap decodedByte;
    private Resources mResources;
    String language, mFullNameAr;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    public LinearLayout mRootViewNoAthen, mBottomPanelUnlockPinLL;
    public SharedPreferences preferences_splash;
    public SharedPreferences.Editor editor_splash;
    private String mdeviceID;
    public JSONObject jObject;
    public int Status_ID, Notifications_Count;
    public LinearLayout mProgressBarLL;
    public String mDigiSignScreenStatus = "mDigiSignScreenStatus";
    public String mGetAllDigiSignScreenStatus = "mGetAllDigiSignScreenStatus";
    public String mGetTokenDigitalSignFile = "mGetTokenDigitalSignFile";
    String authorzation_tokentype, mSerialNumber, CivilNo;
    static HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;
    Boolean pinlockedBooleanType;
    String mNotificationNumberDisplay, mNotifyValueFromAllList;
    public static int SERVER_TIME_OUT = 500;

    public AddDigitalSignScreen() {
        // Required empty public constructor
    }

    public static AddDigitalSignScreen newInstance() {
        AddDigitalSignScreen fragment = new AddDigitalSignScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        if (pinlockedBooleanType) {
            /* mBottomPanelUnlockPinLL.setVisibility(View.GONE);*/
            mBottomPanelUnlockPinLL.setVisibility(View.VISIBLE);
            mPinlocked_bold.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mPinlocked_bold.startAnimation(anim);
        } else {
            mBottomPanelUnlockPinLL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_digital_sign_home_body_fragment, container, false);
        recyclerView = profileBody.findViewById(R.id.recycler_view_digital_sign_list);
        mBottomPanelUnlockPinLL = profileBody.findViewById(R.id.bottom_panel_unlock);
        mPinlocked_bold = profileBody.findViewById(R.id.pinlocked_bold);
        mProfilePic = profileBody.findViewById(R.id.profile_pic_imgv);
        mRootViewNoAthen = profileBody.findViewById(R.id.no_request_ll);
        mFullNameEN = profileBody.findViewById(R.id.full_en_name);
        mCivilIDNum = profileBody.findViewById(R.id.civiild_id_display);
        mProgressBarLL = profileBody.findViewById(R.id.progress_bar_ll_root);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        preferences_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor_splash = preferences_splash.edit();
        editor_splash.apply();

        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        if (pinlockedBooleanType) {
            mBottomPanelUnlockPinLL.setVisibility(View.GONE);
        } else {
            mBottomPanelUnlockPinLL.setVisibility(View.VISIBLE);
            mPinlocked_bold.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mPinlocked_bold.startAnimation(anim);
        }
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mdeviceID = preferences.getString(("mCarrierID"), ("default"));
        if (Utilities.isNetworkAvailable(getActivity())) {
            mProgressBarLL.setVisibility(View.VISIBLE);
            //getDeviceDetails(mdeviceID);
           /* tokenIdentity = new TokenIdentity(getActivity());
            tokenIdentity.mGetAccessTokenRequest(mDigiSignScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);*/
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            if (Utilities.isNetworkAvailable(getActivity())) {
                getDeviceDetails(mdeviceID);
            }
        }
        mResources = getResources();
        mSetProfilePicImage();
        setImageResource();
        getDataFromServer();
        mContentView.setVisibility(View.VISIBLE);
        mFullFrame.addView(profileBody);
    }

    private void mSetProfilePicImage() {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mProfilePhoto = preferences.getString(("Photo"), "default");
        mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
        if (mNotificationNumberDisplay.equals("0")) {
            ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
        } else {
            ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
        }
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    mRoundedImageConvert();
                } else {
                    mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mFullNameStr = preferences.getString(("FullNameEn"), "default");
            mFullNameAr = preferences.getString(("FullNameAr"), "default");
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Civil_ID_No), mCivilIDStr);
            mCivilIDNum.setText(text);
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                mFullNameEN.setText(mFullNameAr);
            } else {
                mFullNameEN.setText(mFullNameStr);
            }
            getAllListDatafromServer(getActivity());

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getAllListDatafromServer(Activity activity) {

        if (Utilities.isNetworkAvailable(getActivity())) {
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            mProgressBarLL.setVisibility(View.VISIBLE);
            // getCivilIDDataList(activity, mCivilIDStr);
            // tokenIdentity = new TokenIdentity(getActivity());
            // tokenIdentity.mGetAccessTokenRequest(mGetAllDigiSignScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            if (Utilities.isNetworkAvailable(getActivity())) {
                getCivilIDDataList(getActivity(), mCivilIDStr);
            }
        }
    }

    private void getCivilIDDataList(Activity activity, String mCivilIDStr) {
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mSerialNumber, CivilNo);
        Data data = new Data(mCivilIDStr);
        MyIDGetAllSigningRequests myIDGetAllSigningRequests = new MyIDGetAllSigningRequests(identity, data);
        Call<MyIDJsonArrayListModel> call = apiService.getDigitalSignList(authorzation_tokentype, myIDGetAllSigningRequests);
        call.enqueue(new Callback<MyIDJsonArrayListModel>() {
            @Override
            public void onResponse(@NonNull Call<MyIDJsonArrayListModel> call, @NonNull Response<MyIDJsonArrayListModel> response) {
                try {
                    //Log.d("res", "" + response);
                    mProgressBarLL.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            mDSignList = response.body().getData();
                            if (mDSignList != null) {
                                eAdapter = new DigitalSignAdapter(mDSignList, getActivity());
                                RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(eLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(eAdapter);
                            /*LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(mMessageReceiver,
                                    new IntentFilter("custom-message-digitalSign"));*/
                                if (mDSignList.size() == 0) {
                                    mRootViewNoAthen.setVisibility(View.VISIBLE);
                                } else {
                                    mRootViewNoAthen.setVisibility(View.GONE);
                                }
                            } else {
                                mRootViewNoAthen.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<MyIDJsonArrayListModel> call, Throwable t) {
                //Log.d("onFailure:", t.getLocalizedMessage());
                //Log.d("throw:", t.getMessage());
                //Log.d("throws:", t.toString());
            }
        });
    }

    private void mGetFilefromService(String itemName) {
        mProgressBarLL.setVisibility(View.VISIBLE);

        CustomDialogWindowBlock customDialog = new CustomDialogWindowBlock(getActivity());
        Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        customDialog.show();

        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel.Identity identity = new kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel.Identity(mSerialNumber, CivilNo);
        kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel.Data data = new kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel.Data(itemName);
        MyIDGetFilePostModel myIDGetAllSigningRequests = new MyIDGetFilePostModel(identity, data);
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Call<ResponseBody> call = apiService.GetFile(authorzation_tokentype, myIDGetAllSigningRequests);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    mProgressBarLL.setVisibility(View.GONE);
                    String depo = response.headers().get("Content-Disposition");
                    String depoSplit[] = depo.split("filename=");
                    mFileName = depoSplit[1].replace("filename=", "").replace("\"", "").trim();
                    new DownloadFileFromURL(response.body()).execute(mPublicURL);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //Log.d("onFailure:", t.getLocalizedMessage());
                //Log.d("throw:", t.getMessage());
                //Log.d("throws:", t.toString());
            }
        });
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok_btn:
                break;
        }
    }

    public void setImageResource() {

    }

   /* public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            try {
                ItemName = intent.getStringExtra("item");
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                editor.putString("FileItemName", ItemName);
                editor.apply();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Utilities.isNetworkAvailable(getActivity())) {
                // mGetFilefromService(ItemName);
                mGetTokenAndGetFile();
            }
        }
    };*/

    private void mGetTokenAndGetFile() {
       /* tokenIdentity = new TokenIdentity(getActivity());
        tokenIdentity.mGetAccessTokenRequest(mGetTokenDigitalSignFile, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);*/
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        if (Utilities.isNetworkAvailable(getActivity())) {
            String mFileItemName = preferences.getString("FileItemName", "default");
            mGetFilefromService(mFileItemName);
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        String filename = "";

        DownloadFileFromURL(ResponseBody responseBody) {
            mResponsebody = responseBody;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBarLL.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                String path = getActivity().getFilesDir().getAbsolutePath() + File.separator + mFileName;
                File file = new File(path);
                if (file.exists()) {
                    System.out.println("file is already there");
                } else {
                    System.out.println("Not find file ");
                }
                URL u = new URL(f_url[0]);
                HttpsURLConnection connection = NetCipher.getHttpsURLConnection(u);
                int lenghtOfFile = connection.getContentLength();

               /* URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                int lenghtOfFile = conection.getContentLength();*/

                InputStream inputStream = mResponsebody.byteStream();
                OutputStream output = new FileOutputStream(file);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = inputStream.read(data)) != -1) {
                    total += count;
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                // input.close();
            } catch (Exception e) {
                //Log.e("Error: ", e.getMessage());
                e.printStackTrace();
            }

            return null;
        }

        protected void onProgressUpdate(String... progress) {
        }


        @Override
        protected void onPostExecute(String file_url) {
            mProgressBarLL.setVisibility(View.GONE);
            // String FilePath = Environment.getExternalStorageDirectory().toString() + "/" + mFileName;
            String FilePath = getActivity().getFilesDir().getAbsolutePath() + File.separator + mFileName;
            //Log.v("FilePath", "" + FilePath);
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            Boolean mNotifiStatusBoolean = false;
            editor.putBoolean("mNotifiStatusBoolean", mNotifiStatusBoolean);
            editor.apply();
            mListener.goToAddWebviewSignScreen(FilePath, mFileName);
        }
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 0;
        int shadowWidth = 0;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mProfilePic.setImageDrawable(roundedBitmapDrawable);
    }

    private void getDeviceDetails(String deviceID) {
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        mdeviceID = preferences.getString(("mCarrierID"), ("default"));
        mCivilIDStr = preferences.getString("CivilNo", "default");
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity identity = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity(mCivilIDStr, mdeviceID);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data1 = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilIDStr, "", mdeviceID);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data1);
        Call<JsonObject> call = apiService.getdvst(BuildConfig.mBasicAuthorization, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mProgressBarLL.setVisibility(View.GONE);
                    data = response.body().toString();
                    if (data != null) {
                        jObject = new JSONObject(data);
                        if (jObject.isNull("Data")) {
                            Toast.makeText(getActivity(), Objects.requireNonNull(response.errorBody()).string(), Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                jObject = new JSONObject(data);
                                JSONObject objectData = jObject.getJSONObject("Data");
                                Status_ID = objectData.getInt("Status");
                                if (Status_ID == 2) {
                                    Notifications_Count = objectData.getInt("NotificationsCount");
                                    editor.putString(("NotificationsCount"), (Integer.toString(Notifications_Count)));
                                    editor.apply();
                                    ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setText(Integer.toString(Notifications_Count));
                                } else {
                                    Toast.makeText(getActivity(), Objects.requireNonNull(response.errorBody()).string(), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
//                    Log.e("splash", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "SocketTimeoutException", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "SocketTimeoutException", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public class DigitalSignAdapter extends RecyclerView.Adapter<DigitalSignAdapter.CustomViewHolder> {
        private ArrayList<kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum> mDsigns;
        public Context context;
        String language;
        public SharedPreferences preferences, sharedPreferences_Locale;
        public SharedPreferences.Editor editor, editor_locale;
        public String mNotifiIDValue;
        private static final String Locale_Preference = "Locale Preference";
        public static final String Locale_KeyValue = "Saved Locale";

        public DigitalSignAdapter(ArrayList<kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum> mDsigns, Context context) {
            this.mDsigns = mDsigns;
            this.context = context;
        }

        @Override
        public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_recycleview_digital_sign_items, parent, false);

            return new CustomViewHolder(itemView);
        }

        @SuppressLint({"SetTextI18n", "CommitPrefEdits"})
        @Override
        public void onBindViewHolder(final CustomViewHolder holder, final int position) {
            final kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum msign = mDsigns.get(position);
            holder.mHeaderSubTitle.setText((msign.getFileName()));
            holder.mRID.setText(Integer.toString(msign.getRID()));
            holder.mPromptAr.setText(msign.getPromptAr());
            holder.mPromptEn.setText(msign.getPromptEn());
            holder.mPersonCivilNo.setText(msign.getPersonCivilNo());
            holder.mChallenge.setText(msign.getChallenge());
            holder.mStatusId.setText(Integer.toString(msign.getStatusId()));
//        holder.mSigningData.setText(msign.getSigningData().toString());
            holder.mFileName.setText(msign.getFileName());
            holder.mFileType.setText(msign.getFileType());
            holder.mServiceNameEN.setText(msign.getServiceNameEN());
            holder.mServiceNameAR.setText(msign.getServiceNameAR());
            sharedPreferences_Locale = context.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editor_locale = sharedPreferences_Locale.edit();
            language = sharedPreferences_Locale.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                holder.mHeaderTitle.setText(msign.getPromptAr());
            } else {
                holder.mHeaderTitle.setText(msign.getPromptEn());
            }
            try {
                preferences = Objects.requireNonNull(context).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                mNotifiIDValue = preferences.getString(("mNotifiIDValue"), "0");
                mNotifyValueFromAllList = preferences.getString(("AddNotifiAutheDigitalSignListNotify"), "false");
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (Integer.toString(msign.getRID()).equals(mNotifiIDValue)) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String mRIDValue = msign.getId();//Integer.toString(msign.getRID());
                        preferences = Objects.requireNonNull(context).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        editor.putString("mIDDigitalSignList", mRIDValue);
                        editor.putString(("mStatusIdDigitalSignList"), (Integer.toString(msign.getStatusId())));
                        editor.putString("mChallaengeIdDigitalSignList", msign.getChallenge());
                        editor.apply();
                        // Intent intent = new Intent("custom-message-digitalSign");
                        //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
                        //intent.putExtra("item", mRIDValue);
                        // LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        holder.mRootViewLinear.performClick();
                    }
                }, SERVER_TIME_OUT);
            }
            if (mNotifyValueFromAllList.equals("true")) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        String mRIDValue = msign.getId();//Integer.toString(msign.getRID());
                        preferences = Objects.requireNonNull(context).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        editor.putString("mIDDigitalSignList", mRIDValue);
                        editor.putString(("mStatusIdDigitalSignList"), (Integer.toString(msign.getStatusId())));
                        editor.putString("mChallaengeIdDigitalSignList", msign.getChallenge());
                        editor.putString("AddNotifiAutheDigitalSignListNotify", "false");
                        editor.apply();
                        // Intent intent = new Intent("custom-message-digitalSign");
                        //            intent.putExtra("quantity",Integer.parseInt(quantity.getText().toString()));
                        // intent.putExtra("item", mRIDValue);
                        // LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
                        // holder.mRootViewLinear.performClick();
                        try {
                            ItemName = mRIDValue;
                            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                            editor = preferences.edit();
                            editor.apply();
                            editor.putString("FileItemName", ItemName);
                            editor.apply();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (Utilities.isNetworkAvailable(getActivity())) {
                            // mGetFilefromService(ItemName);
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            if (Utilities.isNetworkAvailable(getActivity())) {
                                String mFileItemName = preferences.getString("FileItemName", "default");
                                mGetFilefromService(mFileItemName);
                            }
                        }
                    }
                }, SERVER_TIME_OUT);
            }

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.mRootViewLinear.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            holder.mRootViewLinear.setEnabled(false);
                            String mRIDValue = msign.getId();//Integer.toString(msign.getRID());
                            preferences = Objects.requireNonNull(context).getSharedPreferences("plswait", MODE_PRIVATE);
                            editor = preferences.edit();
                            editor.apply();
                            editor.putString("mIDDigitalSignList", mRIDValue);
                            editor.putString(("mStatusIdDigitalSignList"), (Integer.toString(msign.getStatusId())));
                            editor.putString("mChallaengeIdDigitalSignList", msign.getChallenge());
                            editor.apply();
                            /*Intent intent = new Intent("custom-message-digitalSign");
                            intent.putExtra("item", mRIDValue);
                            LocalBroadcastManager.getInstance(context).sendBroadcast(intent);*/
                            try {
                                ItemName = mRIDValue;
                                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                                editor = preferences.edit();
                                editor.apply();
                                editor.putString("FileItemName", ItemName);
                                editor.apply();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (Utilities.isNetworkAvailable(getActivity())) {
                                // mGetFilefromService(ItemName);
                                mGetTokenAndGetFile();
                            }
                        }
                    });
                }
            }, SERVER_TIME_OUT);

        }

        @Override
        public int getItemCount() {
            // return mDsigns.size();
            if (null != mDsigns) {
                return mDsigns.size();
            } else {
                return 0;
            }
        }

        class CustomViewHolder extends RecyclerView.ViewHolder {
            CustomTextView mHeaderTitle, mHeaderSubTitle, mId, mRID, mPromptAr, mPromptEn, mPersonCivilNo, mChallenge, mStatusId, mSigningData, mFileName, mFileType, mServiceNameEN, mServiceNameAR;
            LinearLayout mRootViewLinear;

            CustomViewHolder(View view) {
                super(view);
                mRootViewLinear = view.findViewById(R.id.root_linear_view);
                mId = view.findViewById(R.id.sign_id);
                mHeaderTitle = view.findViewById(R.id.header_title);
                mHeaderSubTitle = view.findViewById(R.id.header_subtitle);
                mRID = view.findViewById(R.id.sign_rid);
                mPromptAr = view.findViewById(R.id.sign_prompt_ar);
                mPromptEn = view.findViewById(R.id.sign_prompt_en);
                mPersonCivilNo = view.findViewById(R.id.sign_personcivilno);
                mChallenge = view.findViewById(R.id.sign_challenge);
                mStatusId = view.findViewById(R.id.sign_statusid);
                mSigningData = view.findViewById(R.id.sign_data);
                mFileName = view.findViewById(R.id.sign_filename);
                mFileType = view.findViewById(R.id.sign_filetype);
                mServiceNameEN = view.findViewById(R.id.sign_service_name_en);
                mServiceNameAR = view.findViewById(R.id.sign_service_name_ar);
            }
        }
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    }).build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}