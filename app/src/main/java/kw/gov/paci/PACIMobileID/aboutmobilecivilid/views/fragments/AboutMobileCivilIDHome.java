package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.Objects;

public class AboutMobileCivilIDHome extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public ImageView header_Mobile_img;
    public CustomTextView header_Mobile_txt, body_Mobile_txt;

    private OnAboutMobileCivilIDInteractionListener mListener;

    public AboutMobileCivilIDHome() {
        // Required empty public constructor
    }

    public static AboutMobileCivilIDHome newInstance() {
        AboutMobileCivilIDHome fragment = new AboutMobileCivilIDHome();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        enForcedLocale();
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnProfileInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.about_mobile_civilid_home_header, container, false);
        header_Mobile_img = header.findViewById(R.id.about_civil_header_img);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.about_mobile_civilid_home_body, container, false);
        header_Mobile_txt = profileBody.findViewById(R.id.header_about_civil_id_header_txt);
        body_Mobile_txt = profileBody.findViewById(R.id.about_civil_id_body_txt);
        setImageResource();
        mBody.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.INVISIBLE);
        mNextButton.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.button_bg_rounded_corners));
        if (null != mNextButton) {
            mNextButton.setOnClickListener(this);
        }
        if (null != mPreviousButton) {
            mPreviousButton.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_btn:
                mListener.goToPreRequisitesScreen();
                break;
        }
    }

    public void setImageResource() {
        header_Mobile_img.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.mobilecivilid));
        header_Mobile_img.setPadding(200, 0, 0, 0);
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }
}

