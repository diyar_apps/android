
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.CivilIDPersonalDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GovData {

    @SerializedName("CivilIdExpiryDate")
    @Expose
    private String civilIdExpiryDate;
    @SerializedName("MOIReference")
    @Expose
    private String mOIReference;
    @SerializedName("SponsorName")
    @Expose
    private String sponsorName;

    public String getCivilIdExpiryDate() {
        return civilIdExpiryDate;
    }

    public void setCivilIdExpiryDate(String civilIdExpiryDate) {
        this.civilIdExpiryDate = civilIdExpiryDate;
    }

    public String getMOIReference() {
        return mOIReference;
    }

    public void setMOIReference(String mOIReference) {
        this.mOIReference = mOIReference;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

}
