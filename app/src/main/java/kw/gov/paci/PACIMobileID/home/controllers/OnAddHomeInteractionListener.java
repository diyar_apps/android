package kw.gov.paci.PACIMobileID.home.controllers;

import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.baseclasses.BaseInteractionListener;

/**
 * Created by SUNIL KUMAR V on 10/1/2018.
 */
public interface OnAddHomeInteractionListener {

    void goToAddAuthenticateRequestScreen();

    void goToAddPinRequestScreen();

    void goToAddPinSuccessfulScreen();

    void goToAddWebviewSignScreen(String s1,String s2);

    void goToAddAuthenSignScreen(String s1,String s2,String qrcodeId);

    void goToAddCancelMobileCivilIDScreen();

    void goToOldPinChangeScreen();

    void goToAddAuthSignDeclinePinScreen(String msg,String mSigingStatus);

    void goToAddDigitalSignDeclinePinScreen(String msg,String mFileNamePath);

    void goToAddNotifiAuthDigitalSignList();

    void goToActivityLogScreen();

    void goToAddCancelPinRequestScreen(String s1,String s2);
}
