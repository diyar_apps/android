package kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers;

import com.google.gson.JsonObject;

import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel.MyIDFinalEntrollModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.updatedTransModel.MyIDUpdateTransactionProgressModel;
import kw.gov.paci.PACIMobileID.home.models.GetAuthRequestDetailsModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDCompUnlockPinModel.MyIDCompleteUnlockModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.MyIDGetAllSigningRequests;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.MyIDGetAuthRequestModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel.MyIDGetFilePostModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDIntializeUnlockPinModel.MyIDInitialPinUnlockModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.MyIDJsonArrayListModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.MyIDpostAuthenticateRequestModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostGetAuthIdentityQR.MyIDpostGetAuthIdentityQRModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostUpdateSigningRequest.MyIDpostUpdateSigningRequestModel;
import kw.gov.paci.PACIMobileID.mobileidverifier.models.MyIDpostGetAuthIdentityData.MyIDpostGetAuthIdentityDataModel;
import kw.gov.paci.PACIMobileID.splash.models.postGtpvsn.MyIDPostGtpvsnModel;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by SUNIL KUMAR V on 9/21/2018.
 */
public interface RetroApiInterface {

    @POST("v1/Pre/uptrpgs")
    @Headers({"Content-Type: application/json;charset=UTF-8"})
    Call<JsonObject> scanUser(@Header("Authorization") String value,
                              @Body MyIDUpdateTransactionProgressModel myIDUpdateTransactionProgressModel);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Pre/gtdvst")
    Call<JsonObject> getdvst(@Header("Authorization") String value,
                             @Body MyIDGetRqdtsModel myIDGetRqdtsModel);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/rqdts")
    Call<JsonObject> getCivilIDResponse(@Header("Authorization") String value,
                                        @Body MyIDGetRqdtsModel myIDGetRqdtsModel);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/psdt")
    Call<JsonObject> getAllPersonalData(@Header("Authorization") String value,
                                        @Body MyIDGetRqdtsModel myIDGetRqdtsModel);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/fnrmt")
    Call<JsonObject> postFinalizeEntroll(@Header("Authorization") String value,
                                         @Body MyIDFinalEntrollModel body);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/utdts")
    Call<GetAuthRequestDetailsModel> getAuthRequestDetailsUser(@Header("Authorization") String value,
                                                               @Body MyIDGetAuthRequestModel body);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/utrq")
    Call<JsonObject> postAuthenticateRequest(@Header("Authorization") String value,
                                             @Body MyIDpostAuthenticateRequestModel body);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/sgrqs")
    Call<MyIDJsonArrayListModel> getDigitalSignList(@Header("Authorization") String value,
                                                    @Body MyIDGetAllSigningRequests body);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/gfl")
    Call<ResponseBody> GetFile(@Header("Authorization") String value,
                               @Body MyIDGetFilePostModel body);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/utrqspn")
    Call<MyIDJsonArrayListModel> getAllAuthList(@Header("Authorization") String value,
                                                @Body MyIDGetAllSigningRequests body);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/cnmbd")
    Call<JsonObject> getCancelMobileID(@Header("Authorization") String value,
                                       @Body MyIDGetRqdtsModel myIDGetRqdtsModel);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/rqplk")
    Call<JsonObject> mInitiatePINUnlockPost(@Header("Authorization") String value,
                                            @Body MyIDInitialPinUnlockModel myIDInitialPinUnlockModel);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/ctplk")
    Call<JsonObject> mCompletePINUnlockPost(@Header("Authorization") String value,
                                            @Body MyIDCompleteUnlockModel myIDCompleteUnlockModel);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/dtsgrq")
    Call<JsonObject> postUpdateSigningRequest(@Header("Authorization") String value,
                                              @Body MyIDpostUpdateSigningRequestModel myIDpostUpdateSigningRequestModel);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/gutdtqr")
    Call<JsonObject> postGetAuthIdentityQR(@Header("Authorization") String value,
                                           @Body MyIDpostGetAuthIdentityQRModel myIDpostGetAuthIdentityQR);


    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Post/gutidt")
    Call<JsonObject> postGetAuthIdentityData(@Header("Authorization") String value,
                                             @Body MyIDpostGetAuthIdentityDataModel myIDpostGetAuthIdentityDataModel);

    @Headers({"Content-Type: application/json;charset=UTF-8"})
    @POST("v1/Pre/gtpvsn")
    Call<JsonObject> postGtpvsn(@Header("Authorization") String value,
                                @Body MyIDPostGtpvsnModel myIDPostGtpvsnModel);

}
