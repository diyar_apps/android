package kw.gov.paci.PACIMobileID.home.views.fragments.Manage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.ScannerActivity;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.CustomDialog;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.fxn.stash.Stash;
import com.google.zxing.integration.android.IntentIntegrator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.TimeZone;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/11/2018.
 */
public class AddManageScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    // public static ArrayList<String> NameArr = new ArrayList<String>();
    //public static ArrayList<AddActivityLogModel> mGlobalAddactivitylogArraylist = new ArrayList<AddActivityLogModel>();
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public CustomTextView mShowDigitaltv;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mSerialNumber, mIssuerDN, mSubjectDN, mvaildFrom, mvaildTo, mKeyUsage;
    public LinearLayout mCancelMobileIDLL, mUnlockIDLL, mChangePINLL, mActivityLogLL;
    public String mNotificationNumberDisplay;
    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";
    public String isFirstTime = "false";
    public String isFirstTimeStatus;
    //public static ArrayList<AddActivityLogModel> mGlobalAddactivitylogArraylist;


    public AddManageScreen() {
        // Required empty public constructor
    }

    public static AddManageScreen newInstance() {
        AddManageScreen fragment = new AddManageScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_home_manage_screen, container, false);
        mShowDigitaltv = profileBody.findViewById(R.id.show_digital_id_txt);
        mChangePINLL = profileBody.findViewById(R.id.change_pin_root_view);
        mUnlockIDLL = profileBody.findViewById(R.id.unlock_root_view);
        mCancelMobileIDLL = profileBody.findViewById(R.id.cancel_mobile_id_root_view);
        mActivityLogLL = profileBody.findViewById(R.id.manage_activity_histroy_view_ll);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        getDataFromServer();
        mContentView.setVisibility(View.VISIBLE);
        mShowDigitaltv.setOnClickListener(this);
        mChangePINLL.setOnClickListener(this);
        mUnlockIDLL.setOnClickListener(this);
        mCancelMobileIDLL.setOnClickListener(this);
        mActivityLogLL.setOnClickListener(this);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.show_digital_id_txt:
                // custom dialog
                CustomDialog customDialog = new CustomDialog(getActivity(), mSerialNumber, mIssuerDN, mSubjectDN, mvaildFrom, mvaildTo, mKeyUsage);
                Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                customDialog.show();
                break;
            case R.id.change_pin_root_view:
                mListener.goToOldPinChangeScreen();
                break;
            case R.id.unlock_root_view:
                mScannerOpenActivity();
                break;
            case R.id.cancel_mobile_id_root_view:
                mListener.goToAddCancelMobileCivilIDScreen();
                break;
            case R.id.manage_activity_histroy_view_ll:
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                isFirstTimeStatus = preferences.getString("firstTimeStoreData", "false");
                if (isFirstTime.equals(isFirstTimeStatus)) {
                    editor.putString("firstTimeStoreData", "true");
                    editor.apply();
                 /*   Date today = new Date();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                    String dateToStr = format.format(today);
                    System.out.println(dateToStr);
                    MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel("Enrolment", "Enrolment - Success", "", "The enrolment process completed.", dateToStr));
                    saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
*/
                    Date today = new Date();
                    @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                    String dateToStr = format.format(today);
                    System.out.println(dateToStr);
                    MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel("Enrolment", "Enrolment - Success", "", "The enrolment process completed.", dateToStr));
                    saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                }
                mListener.goToActivityLogScreen();
                break;
        }
    }

    private void mScannerOpenActivity() {
        try {
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.setOrientationLocked(false);
            integrator.setPrompt("Scan a Qr code");
            integrator.addExtra("AddManageScreen", "AddManageScreenID");
            integrator.setCaptureActivity(ScannerActivity.class);
            integrator.initiateScan();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setImageResource() {
    }

    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
            if (mNotificationNumberDisplay.equals("0")) {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
            } else {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
            }
            mSerialNumber = preferences.getString("mMyIDSerialNumber", "default");
            mIssuerDN = preferences.getString(("mIssuerDN"), "default");
            mSubjectDN = preferences.getString(("mSubjectDN"), "default");
            mvaildFrom = preferences.getString(("mvaildFrom"), "default");
            mvaildTo = preferences.getString(("mvaildTo"), "default");
            mKeyUsage = preferences.getString(("mKeyUsage"), "default");

            String[] mIssuerDNSplit = mIssuerDN.split(",");
            String mpaci_demo = mIssuerDNSplit[0];
            String mPublicAuth = mIssuerDNSplit[1];
            String mkw = mIssuerDNSplit[2];


            String[] mSubjectDNSplit = mSubjectDN.split(",");
            String mname = mSubjectDNSplit[0];
            String mcivil = mSubjectDNSplit[1];
            String mthepublic = mSubjectDNSplit[2];
            String mkwSub = mSubjectDNSplit[3];

            mIssuerDN = "CN=" + mname + "," + " O=" + mPublicAuth + "," + " C=" + mkw;

            mSubjectDN = "CN=" + mpaci_demo + "," + " O=" + mthepublic + "," + " C=" + mkwSub;

            String inputText = mvaildFrom;
            String inputTextTo = mvaildTo;
            mGMTDateConvert(inputText);
            mGMTDateConvertTo(inputTextTo);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void mGMTDateConvert(String inputText) {
        Resources res = getResources();
        @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));

        @SuppressLint({"StringFormatInvalid", "LocalSuppress", "SimpleDateFormat"}) SimpleDateFormat outputFormat =
                new SimpleDateFormat("MM dd, yyyy h:mm a", Locale.ENGLISH);
        // Adjust locale and zone appropriately
        Date date = null;
        try {
            date = inputFormat.parse(inputText);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);
        System.out.println(outputText);
        String[] mVaildFromTvStrArray = outputText.split(",");
        String mMonth = mVaildFromTvStrArray[0];
        String mYear = mVaildFromTvStrArray[1];
        String[] monthNum = mMonth.split(" ");
        String mMonthNum = monthNum[0];
        String mMonthDay = monthNum[1];
        String[] yearNum = mYear.split(" ");
        String mYearNum = yearNum[1];
        //Log.d("year", mYearNum);
        mvaildFrom = mMonthDay + "/" + mMonthNum + "/" + mYearNum;
    }

    private void mGMTDateConvertTo(String inputText) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", Locale.ENGLISH);
        inputFormat.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));

        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat =
                new SimpleDateFormat("MM dd, yyyy h:mm a", Locale.ENGLISH);
        // Adjust locale and zone appropriately
        Date date = null;
        try {
            date = inputFormat.parse(inputText);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);
        System.out.println(outputText);
        String[] mVaildFromTvStrArray = outputText.split(",");
        String mMonth = mVaildFromTvStrArray[0];
        String mYear = mVaildFromTvStrArray[1];
        String[] monthNum = mMonth.split(" ");
        String mMonthNum = monthNum[0];
        String mMonthDay = monthNum[1];
        String[] yearNum = mYear.split(" ");
        String mYearNum = yearNum[1];
        //Log.d("year", mYearNum);
        mvaildTo = mMonthDay + "/" + mMonthNum + "/" + mYearNum;
    }

    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }
}
