package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import static android.content.Context.MODE_PRIVATE;

public class AddPinCollectedSuccessfully extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    private OnAboutMobileCivilIDInteractionListener mListener;
    public CustomTextView mheader_confirm_txt;
    private Intent intent;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;

    public AddPinCollectedSuccessfully() {
        // Required empty public constructor
    }

    public static AddPinCollectedSuccessfully newInstance() {
        AddPinCollectedSuccessfully fragment = new AddPinCollectedSuccessfully();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAboutMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.fragment_confirm_pin_header, container, false);
        mheader_confirm_txt = header.findViewById(R.id.header_confirm_txt);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_pin_collected_successfully, container, false);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mRootView.setVisibility(View.VISIBLE);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
        mCancelButton.setVisibility(View.VISIBLE);
        mCancelButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_btn:
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                editor.putString("ActivityLog_Finalize_entroll", "Finalize Enrollment");
                editor.apply();
                intent = new Intent(getActivity(), AddHomeActivity.class);
                startActivity(intent);
                Objects.requireNonNull(getActivity()).finish();
                break;
        }
    }

    public void setImageResource() {
        mCancelButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.collected_successfully_okbtn));
        mCancelButton.setText(R.string.Confirm);
    }
}
