
package kw.gov.paci.PACIMobileID.home.models.AuthenSignModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuthenSignModel {

    @SerializedName("Error")
    @Expose
    private Object error;
    @SerializedName("Data")
    @Expose
    private List<Datum> data = null;

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

}
