
package kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("RID")
    @Expose
    private Integer rID;
    @SerializedName("PromptAr")
    @Expose
    private String promptAr;
    @SerializedName("PromptEn")
    @Expose
    private String promptEn;
    @SerializedName("PersonCivilNo")
    @Expose
    private String personCivilNo;
    @SerializedName("Challenge")
    @Expose
    private String challenge;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;
    @SerializedName("SigningData")
    @Expose
    private Object signingData;
    @SerializedName("FileName")
    @Expose
    private String fileName;
    @SerializedName("FileType")
    @Expose
    private String fileType;
    @SerializedName("ServiceNameEN")
    @Expose
    private String serviceNameEN;
    @SerializedName("ServiceNameAR")
    @Expose
    private String serviceNameAR;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getRID() {
        return rID;
    }

    public void setRID(Integer rID) {
        this.rID = rID;
    }

    public String getPromptAr() {
        return promptAr;
    }

    public void setPromptAr(String promptAr) {
        this.promptAr = promptAr;
    }

    public String getPromptEn() {
        return promptEn;
    }

    public void setPromptEn(String promptEn) {
        this.promptEn = promptEn;
    }

    public String getPersonCivilNo() {
        return personCivilNo;
    }

    public void setPersonCivilNo(String personCivilNo) {
        this.personCivilNo = personCivilNo;
    }

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public Object getSigningData() {
        return signingData;
    }

    public void setSigningData(Object signingData) {
        this.signingData = signingData;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getServiceNameEN() {
        return serviceNameEN;
    }

    public void setServiceNameEN(String serviceNameEN) {
        this.serviceNameEN = serviceNameEN;
    }

    public String getServiceNameAR() {
        return serviceNameAR;
    }

    public void setServiceNameAR(String serviceNameAR) {
        this.serviceNameAR = serviceNameAR;
    }

}
