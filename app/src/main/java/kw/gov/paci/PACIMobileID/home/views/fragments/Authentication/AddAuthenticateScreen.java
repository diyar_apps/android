package kw.gov.paci.PACIMobileID.home.views.fragments.Authentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.ScannerActivity;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/11/2018.
 */
public class AddAuthenticateScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public LinearLayout mScanQrCodeLL, mAuthenticateRequestLL, mBottomPanelUnlockPinLL;
    public CustomTextView mFullNameEN, mCivilIDNum, mPinlocked_bold;
    public SharedPreferences preferences, sharedPreferences;
    public SharedPreferences.Editor editor, editors;
    public String mCivilIDStr, mFullNameStr, mFullNameAr;
    public ImageView mProfilePic;
    public String mProfilePhoto;
    public Bitmap decodedByte;
    private Resources mResources;
    String language;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    static HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;
    Boolean pinlockedBooleanType;
    String mNotificationNumberDisplay;

    public AddAuthenticateScreen() {
        // Required empty public constructor
    }

    public static AddAuthenticateScreen newInstance() {
        AddAuthenticateScreen fragment = new AddAuthenticateScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        if (pinlockedBooleanType) {
            /* mBottomPanelUnlockPinLL.setVisibility(View.GONE);*/
            mBottomPanelUnlockPinLL.setVisibility(View.VISIBLE);
            mPinlocked_bold.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mPinlocked_bold.startAnimation(anim);
        } else {
            mBottomPanelUnlockPinLL.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_authenciate_home_body_fragment, container, false);
        mProfilePic = profileBody.findViewById(R.id.profile_pic_imgv);
        mScanQrCodeLL = profileBody.findViewById(R.id.scan_qr_view_ll);
        mBottomPanelUnlockPinLL = profileBody.findViewById(R.id.bottom_panel_unlock);
        mPinlocked_bold = profileBody.findViewById(R.id.pinlocked_bold);
        mFullNameEN = profileBody.findViewById(R.id.full_en_name);
        mCivilIDNum = profileBody.findViewById(R.id.civiild_id_display);
        mAuthenticateRequestLL = profileBody.findViewById(R.id.authe_request_view_ll);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        mResources = getResources();
        mSetProfilePicImage();
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mScanQrCodeLL.setOnClickListener(this);
        mAuthenticateRequestLL.setOnClickListener(this);
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        if (pinlockedBooleanType) {
            mBottomPanelUnlockPinLL.setVisibility(View.GONE);
        } else {
            mBottomPanelUnlockPinLL.setVisibility(View.VISIBLE);
            mPinlocked_bold.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mPinlocked_bold.startAnimation(anim);
        }
        getDataFromServer();
        mFullFrame.addView(profileBody);
    }

    private void mSetProfilePicImage() {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mProfilePhoto = preferences.getString(("Photo"), "default");
        mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
        if (mNotificationNumberDisplay.equals("0")) {
            ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
        } else {
            ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
        }
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    mRoundedImageConvert();
                } else {
                    mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("CommitPrefEdits")
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mCivilIDStr = (mCivilIDStr);
            mFullNameStr = preferences.getString(("FullNameEn"), "default");
            mFullNameAr = preferences.getString(("FullNameAr"), "default");
            mFullNameAr = (mFullNameAr);
            mFullNameStr = (mFullNameStr);
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Civil_ID_No), mCivilIDStr);
            mCivilIDNum.setText(text);
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                mFullNameEN.setText(mFullNameAr);
            } else {
                mFullNameEN.setText(mFullNameStr);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scan_qr_view_ll:
                try {
                    mScannerOpenActivity();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case R.id.authe_request_view_ll:
                mListener.goToAddAuthenticateRequestScreen();
                break;
        }
    }

    private void mScannerOpenActivity() {
        try {
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.setOrientationLocked(false);
            integrator.setPrompt("Scan a Qr code");
            integrator.addExtra("AddAuthencateScreen", "AddAuthencateScreenID");
            //integrator.setCaptureActivity(ScannerActivity.class);
           // integrator.initiateScan();
            IntentIntegrator.forSupportFragment(this).initiateScan();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setImageResource() {

    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 0;
        int shadowWidth = 0;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mProfilePic.setImageDrawable(roundedBitmapDrawable);
    }
}