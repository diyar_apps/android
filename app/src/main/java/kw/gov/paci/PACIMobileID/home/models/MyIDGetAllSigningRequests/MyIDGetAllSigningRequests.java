
package kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyIDGetAllSigningRequests {

    @SerializedName("Identity")
    @Expose
    private Identity identity;
    @SerializedName("Data")
    @Expose
    private Data data = null;

    /**
     * No args constructor for use in serialization
     */
    public MyIDGetAllSigningRequests() {
    }

    /**
     * @param identity
     * @param data
     */
    public MyIDGetAllSigningRequests(Identity identity, Data data) {
        super();
        this.identity = identity;
        this.data = data;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
