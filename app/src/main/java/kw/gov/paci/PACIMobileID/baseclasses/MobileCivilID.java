package kw.gov.paci.PACIMobileID.baseclasses;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import java.util.Locale;
import java.util.Objects;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.AboutMobileCivilIDActivity;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.mobileidverifier.views.activitys.MobileIDVerifierScanActivity;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomMenuItemTypefaceSpan;

public abstract class MobileCivilID extends LocaleBaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    FrameLayout mFrameLayout;
    String languageToLoad;
    Locale locale;
    //Shared Preferences Variables
    private static final String Locale_Preference = "Locale Preference";
    private static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    /* public SharedPreferences preferencese_spalsh;
     public SharedPreferences.Editor editor_spalsh;*/
    public SharedPreferences preferences;
    // public SharedPreferences.Editor editor;
    public String mCivilNo, AddConfirmPinScreenSuccess;
    public NavigationView navigationView;
    public Menu m;
    public Configuration config;
    //static TextView notifCount;
    //static int mNotifCount = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root_view);

        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        preferences = Objects.requireNonNull(MobileCivilID.this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mCivilNo = preferences.getString(("CivilNo"), ("default"));
        AddConfirmPinScreenSuccess = preferences.getString("AddConfirmPinScreenSuccess", "default");
        mCivilNo = (mCivilNo);

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        config = new Configuration();
        sharedPreferences = getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        //changing statusbar color
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));
        }
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.getBackground().setAlpha(0);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            DrawerLayout drawer = findViewById(R.id.drawer_layout);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.addDrawerListener(toggle);

            toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    // Do whatever you want here
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    // Do whatever you want here
                    Utilities.hideKeyboard(MobileCivilID.this);
                }
            };
            // Set the drawer toggle as the DrawerListener
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView = findViewById(R.id.nav_view);
            if (AddConfirmPinScreenSuccess.equals("default")) {
                navigationView.getMenu().findItem(R.id.nav_home).setVisible(false);
                navigationView.getMenu().findItem(R.id.nav_verify_identity).setVisible(false);
                //navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
                //setNotifCount(0);
            } else {
                navigationView.getMenu().findItem(R.id.nav_home).setVisible(true);
                navigationView.getMenu().findItem(R.id.nav_verify_identity).setVisible(true);
                //navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
                // setNotifCount(2);
                //navigationView.setCheckedItem(R.id.nav_home);
                //navigationView.getMenu().getItem(0).setChecked(true);
                //onNavigationItemSelected(navigationView.getMenu().getItem(0));
            }
            m = navigationView.getMenu();
            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);
                //for aapplying a font to subMenu ...
                SubMenu subMenu = mi.getSubMenu();
                if (subMenu != null && subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        MenuItem subMenuItem = subMenu.getItem(j);
                        applyFontToMenuItem(subMenuItem);
                    }
                }
                //the method we have create in activity
                applyFontToMenuItem(mi);
                //setHome disable
               /* preferences = Objects.requireNonNull(MobileCivilID.this).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                mCivilNo = preferences.getString(("CivilNo"), ("default"));
                mCivilNo = (mCivilNo);
                if(mCivilNo.equals("default")) {
                    MenuItem nav_item1 = m.findItem(R.id.nav_home);
                    nav_item1.setEnabled(false);
                    nav_item1.setVisible(false);
                }else{
                    MenuItem nav_item1 = m.findItem(R.id.nav_home);
                    nav_item1.setEnabled(true);
                    nav_item1.setVisible(true);
                }*/
            }
           /* navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);*/
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFrameLayout = findViewById(R.id.root_content);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        //showAlert();
        goToPreviousScreen();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*preferences = Objects.requireNonNull(MobileCivilID.this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mCivilNo = preferences.getString(("CivilNo"), ("default"));
        mCivilNo = (mCivilNo);
        if(mCivilNo.equals("default")) {
            MenuItem nav_item1 = m.findItem(R.id.nav_home);
            nav_item1.setEnabled(false);
            nav_item1.setVisible(false);
        }else{
            MenuItem nav_item1 = m.findItem(R.id.nav_home);
            nav_item1.setEnabled(true);
            nav_item1.setVisible(true);
        }*/
    }

    private void showAlert() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        exitApplication();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.s_6_1_app_exit_alert).setPositiveButton(R.string.s_6_1_dialog_yes, dialogClickListener)
                .setNegativeButton(R.string.s_6_1_dialog_no, dialogClickListener).show();

    }

    private void exitApplication() {
        super.onBackPressed();
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        closeNavigationDrawer();
        int id = item.getItemId();
        Intent intent = null;
        if (id == R.id.nav_home) {
            intent = new Intent(getApplicationContext(), AddHomeActivity.class);
        } else if (id == R.id.nav_about_mobile_civil_id) {
            intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
        } else if (id == R.id.nav_verify_identity) {
            intent = new Intent(getApplicationContext(), MobileIDVerifierScanActivity.class);
        }
        /*else if (id == R.id.nav_feedback) {
            intent = new Intent(getApplicationContext(), FeedBackActivity.class);
        } else if (id == R.id.nav_app_help) {
            intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
        } else if (id == R.id.nav_new_update) {
            intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
        }*/
        else if (id == R.id.nav_arabic) {
            if (item.toString().equals("Arabic")) {
                languageToLoad = "ar";
            } else {
                languageToLoad = "en";
            }
            locale = new Locale(languageToLoad);
            editor.putString(Locale_KeyValue, languageToLoad);
            editor.commit();
            Locale.setDefault(locale);
            config.locale = locale;
            this.createConfigurationContext(config);
            this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());
            /*Added load intent locale when back button click*/
            Intent restart = getIntent();
            finish();
            startActivity(restart);
            if (AddConfirmPinScreenSuccess.equals("default")) {
                intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(getApplicationContext(), AddHomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        } else {
            intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
        }
        startActivity(intent);

        overridePendingTransition(R.anim.enter, R.anim.exit);
        MobileCivilID.this.finish();//check once
        return true;
    }

    protected void closeNavigationDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

  /*  protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();

        //fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.commit();
        //fragmentTransaction.commitAllowingStateLoss();
        if (fragment != null) {
            fragmentTransaction.remove(fragment);
            fragmentTransaction.add(fragmentContainer, fragment, id);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else {
            fragmentTransaction.add(fragmentContainer, fragment, id);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    }*/

    protected void replaceFragment(int fragmentContainer, Fragment fragment, String id, boolean isAddToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(fragmentContainer, fragment, id);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(id);
        }
//        fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    public void goToPreviousScreen() {
        FragmentManager fm = getSupportFragmentManager();
        if (isTaskRoot() && fm.getBackStackEntryCount() == 0) {
            //showAlert();
            finish();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/gotham_book.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomMenuItemTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    /*private static String (String input) {
        // This is base64 encoding, which is not an ion
        return Base64.encodeToString(input.getBytes(), Base64.DEFAULT);
    }

    private static String (String input) {
        return new String(Base64.decode(input, Base64.DEFAULT));
    }*/
   /* @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // MenuInflater inflater = getSupportMenuInflater();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_menu_drawer, menu);

        notifCount = (TextView) findViewById(R.id.badge_notification_2);
        notifCount.setText(String.valueOf(mNotifCount));
        return super.onCreateOptionsMenu(menu);
    }*/

   /* private void setNotifCount(int count){
        mNotifCount = count;
        invalidateOptionsMenu();
    }*/
}
