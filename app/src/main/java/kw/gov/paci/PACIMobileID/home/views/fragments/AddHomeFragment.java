package kw.gov.paci.PACIMobileID.home.views.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Objects;
import java.util.StringTokenizer;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/1/2018.
 */
public class AddHomeFragment extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    //private BottomNavigationView navigation;
    public CustomTextView mCivilID, mArabicName, mEnglishName, mCountryName, mGender, mDOB, mExperDate, mMoiRef, mMOItextDispaly;
    public String mCivilIDStr, mArabicNameStr, mEnglishNameStr, mCountryNameStr, mGenderStr, mDOBStr, mExperDateStr, mMoiRefStr, mCountryFlagStr, mNationalityAr;
    public ImageView mCountryFlag;
    public OnAddHomeInteractionListener mListener;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    private String commonNameed, mcommonNameed;
    public LinearLayout mRootBgImage;
    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editors;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    String language;
    public SharedPreferences preferences_splash;
    public SharedPreferences.Editor editor_splash;
    private String mdeviceID;
    private ProgressDialog pDialog;
    public JSONObject jObject;
    private RetroApiInterface apiService;
    private String data = null;
    public int Status_ID, Notifications_Count;
    public String mHomeFragmentScreenStatus = "mHomeFragmentScreenStatus";
    String authorzation_tokentype, mSponsorName;
    public LinearLayout mRootMoiRefKWT;
    public String mNotificationNumberDisplay;

    public AddHomeFragment() {
        // Required empty public constructor
    }

    public static AddHomeFragment newInstance() {
        AddHomeFragment fragment = new AddHomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_home_body_fragment, container, false);
        mRootMoiRefKWT = profileBody.findViewById(R.id.moi_ref_only_kwt_ll);
        mRootBgImage = profileBody.findViewById(R.id.root_ll_home);
        mCivilID = profileBody.findViewById(R.id.civilid_txt);
        mArabicName = profileBody.findViewById(R.id.arabic_name_txt);
        mEnglishName = profileBody.findViewById(R.id.english_name_txt);
        mCountryFlag = profileBody.findViewById(R.id.country_flag_imgvew);
        mCountryName = profileBody.findViewById(R.id.country_name_txt);
        mGender = profileBody.findViewById(R.id.gender);
        mDOB = profileBody.findViewById(R.id.dob_txt);
        mExperDate = profileBody.findViewById(R.id.exp_txt);
        mMoiRef = profileBody.findViewById(R.id.moi_ref_no_txt);
        mMOItextDispaly = profileBody.findViewById(R.id.moi_text_display);

        //((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setText("tt");
        getDataFromServer();

        preferences_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor_splash = preferences_splash.edit();
        editor_splash.apply();

        mdeviceID = preferences_splash.getString("mCarrierID", "default");

        if (Utilities.isNetworkAvailable(getActivity())) {
          /*  tokenIdentity = new TokenIdentity(getActivity());
            tokenIdentity.mGetAccessTokenRequest(mHomeFragmentScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);*/
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            if (Utilities.isNetworkAvailable(getActivity())) {
                getDeviceDetails(mdeviceID);
            }
        }

        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mFullFrame.addView(profileBody);
    }

    @SuppressLint({"SetTextI18n", "CommitPrefEdits"})
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
            if (mNotificationNumberDisplay.equals("0")) {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
            } else {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
            }
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mArabicNameStr = preferences.getString(("FullNameAr"), "default");
            mEnglishNameStr = preferences.getString(("FullNameEn"), "default");
            mCountryNameStr = preferences.getString(("NationalityEn"), "default");
            mNationalityAr = preferences.getString(("NationalityAr"), "default");
            mCountryFlagStr = preferences.getString(("NationalityFlag"), "default");
            mGenderStr = preferences.getString(("Gender"), "default");
            mDOBStr = preferences.getString(("BirthDate"), "default");
            mExperDateStr = preferences.getString(("CivilIdExpiryDate"), "default");
            mMoiRefStr = preferences.getString(("MOIReference"), "default");
            mSponsorName = preferences.getString(("SponsorName"), "default");

            //SetText
            mCivilID.setText(mCivilIDStr);
            mArabicName.setText(mArabicNameStr);
            mEnglishName.setText(mEnglishNameStr);
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");

            if (language.equals("ar")) {
                mCountryName.setText(mNationalityAr);
                switch (mGenderStr) {
                    case "F":
                        mGender.setText(R.string.Female_home);
                        break;
                    case "":
                        mGender.setText("-");
                        break;
                    default:
                        mGender.setText(R.string.Male);
                        break;
                }
            }
            if (language.equals("en")) {
                switch (mGenderStr) {
                    case "F":
                        mGender.setText(R.string.Female_home);
                        break;
                    case "":
                        mGender.setText("-");
                        break;
                    default:
                        mGender.setText(R.string.Male);
                        break;
                }
            } else {
                mCountryName.setText(mCountryNameStr);
                mGender.setText(mGenderStr);
            }
            mCountryName.setText(mCountryNameStr);
            if (language.equals("ar")) {
                if (mGender.getText().toString().equals("F")) {
                    mGender.setText("أنثى");
                    mCountryName.setText(mNationalityAr);
                } else {
                    mGender.setText("ذكر");
                    mCountryName.setText(mNationalityAr);
                }
            }
            StringTokenizer dobtoken = new StringTokenizer(mDOBStr, "T");
            String dob = dobtoken.nextToken();
            String[] mDob = dob.split("-");

            StringTokenizer tokens = new StringTokenizer(mExperDateStr, "T");
            String expDate = tokens.nextToken();
            String[] mexpDate = expDate.split("-");

            mDOB.setText(mDob[02] + "/" + mDob[01] + "/" + mDob[0]);
            mExperDate.setText(mexpDate[02] + "/" + mexpDate[01] + "/" + mexpDate[0]);
            mMoiRef.setText(mMoiRefStr);

            try {
                byte[] decodedString = Base64.decode(mCountryFlagStr, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                mCountryFlag.setImageBitmap(decodedByte);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (mCountryNameStr.equals("KWT") || mCountryNameStr.equals("Kuwaiti") || mCountryNameStr.equals("Kuwait")) {
            mRootBgImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.screenbg_blue));
        } else {
            mMOItextDispaly.setText(R.string.Sponsor);
            mMoiRef.setText(mSponsorName);
            //mMoiRef.setTextSize(getResources().getDimension(R.dimen.moi_ref_textsize));
            mRootBgImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.screenbg_liteorange));
        }
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_btn:
                break;
        }
    }

    public void setImageResource() {

    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.commit();
    }

    private void getDeviceDetails(String deviceID) {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        mdeviceID = preferences.getString(("mCarrierID"), ("default"));
        mCivilIDStr = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mCivilIDStr, mdeviceID);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data1 = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilIDStr, "", mdeviceID);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data1);
        Call<JsonObject> call = apiService.getdvst(BuildConfig.mBasicAuthorization, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    data = response.body().toString();
                    if (data != null) {
                        jObject = new JSONObject(data);
                        if (jObject.isNull("Data")) {
                            Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                jObject = new JSONObject(data);
                                JSONObject objectData = jObject.getJSONObject("Data");
                                Status_ID = objectData.getInt("Status");
                                if (Status_ID == 2) {
                                    Notifications_Count = objectData.getInt("NotificationsCount");
                                    editor.putString(("NotificationsCount"), (Integer.toString(Notifications_Count)));
                                    // editor.putString(("Challenge"), (myResponse.getData().getMyidProfileName()));
                                    editor.apply();
                                    ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setText(Integer.toString(Notifications_Count));
                                } else {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.e("splash", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
