package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnLoginInteractionListener;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AddConfirmPin;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilID;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;

public class LoginAccount extends MobileCivilID implements OnLoginInteractionListener {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_view);
        Bundle bundle = getIntent().getExtras();
        boolean isChangePassword = false;
        if (null != bundle) {
            isChangePassword = bundle.getBoolean(AddConfirmPin.IS_CHANGE_PASSWORD, false);
        }
        if (isChangePassword) {
        } else {
        }
        Activity currentActivity = ((MobileCivilIDApplication) getApplicationContext()).getCurrentActivity();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobileCivilIDApplication mobileCivilIDApplication = (MobileCivilIDApplication) this.getApplicationContext();
        mobileCivilIDApplication.setCurrentActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.root_content);
        if (!(currentFragment instanceof AddConfirmPin)) {
            goToPreviousScreen();
        }

    }

    @Override
    public void setAppPassCode() {
        finish();
    }

}
