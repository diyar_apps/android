
package kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("CivilNo")
    @Expose
    private String civilNo;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param civilNo
     */
    public Data(String civilNo) {
        super();
        this.civilNo = civilNo;
    }

    public String getCivilNo() {
        return civilNo;
    }

    public void setCivilNo(String civilNo) {
        this.civilNo = civilNo;
    }

}
