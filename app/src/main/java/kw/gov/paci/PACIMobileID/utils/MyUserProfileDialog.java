package kw.gov.paci.PACIMobileID.utils;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

/**
 * Created by SUNIL KUMAR V on 10/21/2018.
 */
public class MyUserProfileDialog extends Dialog implements
        android.view.View.OnClickListener {
    public Activity activity;
    public ImageView mUserProfilePic;
    public Bitmap bitmap;

    public MyUserProfileDialog(Activity activity, Bitmap bitmap) {
        super(activity);
        this.activity = activity;
        this.bitmap=bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.fragment_image_preview);
        mUserProfilePic=findViewById(R.id.img_preview);
        mUserProfilePic.setImageBitmap(bitmap);
        mUserProfilePic.setOnClickListener(this);
        setCanceledOnTouchOutside(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_preview:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}