
package kw.gov.paci.PACIMobileID.splash.models.postGtpvsn;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDPostGtpvsnModel {

    @SerializedName("Data")
    @Expose
    private Data data;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MyIDPostGtpvsnModel() {
    }

    /**
     * 
     * @param data
     */
    public MyIDPostGtpvsnModel(Data data) {
        super();
        this.data = data;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
