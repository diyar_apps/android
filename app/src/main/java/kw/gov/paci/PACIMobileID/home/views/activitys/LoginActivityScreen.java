package kw.gov.paci.PACIMobileID.home.views.activitys;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.AboutMobileCivilIDActivity;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddPinRequestScreen;
import kw.gov.paci.PACIMobileID.splash.models.DeviceStautsModel;
import kw.gov.paci.PACIMobileID.utils.CSRHelper;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomEditTextView;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.google.gson.JsonObject;
import com.intercede.Authentication;
import com.intercede.IdentityWallet;
import com.intercede.myIDSecurityLibrary.IPinCallback;
import com.intercede.myIDSecurityLibrary.IReturnUserPin;
import com.intercede.myIDSecurityLibrary.ISigningResult;
import com.intercede.myIDSecurityLibrary.InvalidContextException;
import com.intercede.myIDSecurityLibrary.MyIDSecurityLibrary;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationMechanisms;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationParameters;
import com.intercede.myIDSecurityLibrary.MyIdSignDataResponse;
import com.intercede.myIDSecurityLibrary.MyIdSigningParameters;
import com.intercede.myIDSecurityLibrary.SigningAlgorithm;
import com.intercede.myIDSecurityLibrary.UnlicencedException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by SUNIL KUMAR V on 11/10/2018.
 */
public class LoginActivityScreen extends AppCompatActivity implements View.OnClickListener {
    CustomTextView mHeaderNameAr, mHeaderEn;
    CustomEditTextView mOldPin;
    CustomButton mConfirmPin;
    public SharedPreferences preferences, preferences_splash;
    public SharedPreferences.Editor editor, editor_splash;
    public String mArabicNameStr, mEnglishNameStr, mConfirmPinStr;
    private RetroApiInterface apiService;
    private String mdeviceID;
    private String data = null;
    private DeviceStautsModel myResponse;
    private ProgressDialog pDialog;
    public JSONObject jObject;
    public String android_id, device_ID, NotificationDevice_UniqueId, Civil_No, Valid_From, Valid_To;
    public int Id, Identity_Type, Device_OS, Language_ID, Status_ID, Notifications_Count;
    public static int SPLASH_TIME_OUT = 1000;
    public LinearLayout mRootProgressBar;
    public String mLoginScreenStatus = "mLoginScreenStatus";
    String authorzation_tokentype;
    //FingerPrint
    private KeyStore keyStore;
    private static final String KEY_NAME = "MCivilID";
    private Cipher cipher;
    private TextView textView;
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editors;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    String language, mAfterHashUserEnterValue;
    Locale locale;
    String mCivilID;
    ImageView mFingerPrintImg;
    MyIDSecurityLibrary myIDSecurityLibrary;
    MyIdSigningParameters signingParameters;
    MyIdAuthenticationParameters authenticationParameters;
    byte[] sigBytes;
    public String sign;
    public Boolean pinlockedBooleanType;
    public LinearLayout mPinBlockedRoot;
    public CustomTextView mPinLockedtext;
    public Authentication authentication = null;

    @TargetApi(Build.VERSION_CODES.M)
    @SuppressLint({"HardwareIds", "CommitPrefEdits"})
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login_activity_screen_background);
        preferences = Objects.requireNonNull(LoginActivityScreen.this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();

        preferences_splash = Objects.requireNonNull(LoginActivityScreen.this).getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor_splash = preferences_splash.edit();
        editor_splash.apply();

        Window window = this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.colorBlack));

        sharedPreferences = LoginActivityScreen.this.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editors = sharedPreferences.edit();
        language = sharedPreferences.getString(Locale_KeyValue, "ar");

        locale = new Locale(language);
        editor.putString(Locale_KeyValue, language);
        editor.apply();
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        this.createConfigurationContext(config);
        this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());

        textView = findViewById(R.id.errorText);
        mFingerPrintImg = findViewById(R.id.finger_img_click);
        mHeaderNameAr = findViewById(R.id.header_name_ar);
        mHeaderEn = findViewById(R.id.header_name_en);
        mOldPin = findViewById(R.id.et_old_pin);
        mConfirmPin = findViewById(R.id.confirm_request_pin);
        mRootProgressBar = findViewById(R.id.progress_bar_dialog_root);
        mPinLockedtext = findViewById(R.id.pinlocked_bold);
        mPinBlockedRoot = findViewById(R.id.blocked_pin_ll);
        mArabicNameStr = preferences.getString("FullNameAr", "default");
        mEnglishNameStr = preferences.getString("FullNameEn", "default");
        mConfirmPinStr = preferences.getString("mConfirmPinED", "default");
        mdeviceID = preferences.getString("mCarrierID", "default");
        mHeaderNameAr.setText(mArabicNameStr);
        mHeaderEn.setText(mEnglishNameStr);
        if (language.equals("ar")) {
            mConfirmPin.setText(R.string.LoginAr);
            textView.setText(R.string.Biometric_Authentication_ar);
            mOldPin.setHint(R.string.Enter_PIN_ar);
        } else {
            mConfirmPin.setText(R.string.Login);
            textView.setText(R.string.Biometric_Authentication);
        }
        mConfirmPin.setOnClickListener(this);
        textView.setOnClickListener(this);
        mFingerPrintImg.setOnClickListener(this);

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_request_pin:
                initialise(LoginActivityScreen.this);
                pinlockedBooleanType = isPinLocked();
                if (!pinlockedBooleanType) {
                    if (mOldPin.getText().toString().length() <= 0) {
                        Toast.makeText(LoginActivityScreen.this, R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                    } else {
                        if (mOldPin.getText().toString().equals(mConfirmPinStr)) {
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            getDeviceStatus(LoginActivityScreen.this);
                        } else {
                            try {
                                byte[] randomBytes = new byte[20];
                                new Random().nextBytes(randomBytes);
                                sigBytes = randomBytes;
                                new SignDataAsyntask(sigBytes).execute();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
                break;
            case R.id.errorText:
                initialise(LoginActivityScreen.this);
                pinlockedBooleanType = isPinLocked();
                if (!pinlockedBooleanType) {
                    mFingerPrintActivation();
                }
                break;
            case R.id.finger_img_click:
                initialise(LoginActivityScreen.this);
                pinlockedBooleanType = isPinLocked();
                if (!pinlockedBooleanType) {
                    mFingerPrintActivation();
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void mFingerPrintActivation() {
        try {
            KeyguardManager keyguardManager = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
            FingerprintManager fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            assert fingerprintManager != null;
            if (!fingerprintManager.isHardwareDetected()) {
                textView.setText("Your Device does not have a Fingerprint Sensor");
            } else {
                // Checks whether fingerprint permission is set on manifest
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
                    textView.setText("Fingerprint authentication permission not enabled");
                } else {
                    // Check whether at least one fingerprint is registered
                    if (!fingerprintManager.hasEnrolledFingerprints()) {
                        textView.setText("Register at least one fingerprint in Settings");
                    } else {
                        // Checks whether lock screen security is enabled or not
                        if (!keyguardManager.isKeyguardSecure()) {
                            textView.setText("Lock screen security not enabled in Settings");
                        } else {
                            generateKey();
                            if (cipherInit()) {
                                FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
                                FingerprintHandler helper = new FingerprintHandler(this);
                                helper.startAuth(fingerprintManager, cryptoObject);
                                Toast.makeText(LoginActivityScreen.this, "Biometric Authentication Enabled", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getDeviceStatus(Activity activity) {
        if (Utilities.isNetworkAvailable(activity)) {
            mRootProgressBar.setVisibility(View.VISIBLE);
            getDeviceDetails(mdeviceID);
        }
    }

    @Override
    protected void onResume() {
        enForcedLocale();
        super.onResume();
        initialise(LoginActivityScreen.this);
        pinlockedBooleanType = isPinLocked();
        if (pinlockedBooleanType) {
            mPinBlockedRoot.setVisibility(View.VISIBLE);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mPinLockedtext.startAnimation(anim);
        } else {
            mPinBlockedRoot.setVisibility(View.GONE);
        }
    }

    private void getDeviceDetails(String deviceID) {
        mRootProgressBar.setVisibility(View.VISIBLE);
        mCivilID = preferences.getString("CivilNo", "default");
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Identity identity = new Identity(mCivilID, mdeviceID);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data1 = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilID, "", mdeviceID);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data1);
        Call<JsonObject> call = apiService.getdvst(BuildConfig.mBasicAuthorization, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    //pDialog.dismiss();
                    mRootProgressBar.setVisibility(View.GONE);
                    data = response.body().toString();
                    if (data != null) {
                        jObject = new JSONObject(data);
                        if (jObject.isNull("Data")) {
                            SharedPreferences preferences = LoginActivityScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            new mInitialScreenHandler().invoke();
                        } else {
                            try {
                                jObject = new JSONObject(data);
                                JSONObject objectData = jObject.getJSONObject("Data");
                                Status_ID = objectData.getInt("Status");
                                if (Status_ID == 2) {
                                    Id = objectData.getInt("Id");
                                    device_ID = objectData.get("DeviceID").toString();
                                    Device_OS = objectData.getInt("DeviceOS");
                                    Language_ID = objectData.getInt("LanguageID");
                                    NotificationDevice_UniqueId = objectData.get("NotificationDeviceUniqueId").toString();
                                    Civil_No = objectData.get("CivilNo").toString();
                                    Valid_From = objectData.get("ValidFrom").toString();
                                    Valid_To = objectData.get("ValidTo").toString();
                                    Identity_Type = objectData.getInt("IdentityType");
                                    Notifications_Count = objectData.getInt("NotificationsCount");
                                    editor.putString(("Id"), (Integer.toString(Id)));
                                    editor.putString(("mCarrierID"), (device_ID));
                                    editor.putString(("DeviceOS"), (Integer.toString(Device_OS)));
                                    editor.putString(("LanguageID"), (Integer.toString(Language_ID)));
                                    editor.putString(("NotificationDeviceUniqueId"), (NotificationDevice_UniqueId));
                                    editor.putString(("CivilNo"), (Civil_No));
                                    editor.putString(("ValidFrom"), (Valid_From));
                                    editor.putString(("ValidTo"), (Valid_To));
                                    editor.putString(("Status"), (Integer.toString(Status_ID)));
                                    editor.putString(("NotificationsCount"), (Integer.toString(Notifications_Count)));
                                    editor.apply();
                                    Intent intent = new Intent(getApplicationContext(), AddHomeActivity.class);
                                    startActivity(intent);
                                    LoginActivityScreen.this.finish();
                                } else {
                                    SharedPreferences preferences = LoginActivityScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    new mInitialScreenHandler().invoke();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        SharedPreferences preferences = LoginActivityScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        new mInitialScreenHandler().invoke();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(LoginActivityScreen.this, "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(LoginActivityScreen.this, "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class mInitialScreenHandler {
        public void invoke() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(LoginActivityScreen.this, AboutMobileCivilIDActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {
            e.printStackTrace();
        }

        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }

        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }

        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(KEY_NAME,
                    null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException | UnrecoverableKeyException | IOException | NoSuchAlgorithmException | InvalidKeyException e) {
            throw new RuntimeException("Failed to init Cipher", e);
        }
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class SignDataAsyntask extends AsyncTask<Void, Void, Void> implements ISigningResult, IPinCallback {
        byte[] mSignBytes;
        int count = 0;
        //public AlertDialog pinDialog;

        public SignDataAsyntask(byte[] sigBytes) {
            this.mSignBytes = sigBytes;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(LoginActivityScreen.this));
                myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
                myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
                myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
                signingParameters = new MyIdSigningParameters();
                signingParameters.algorithm = SigningAlgorithm.SHA256_PKCS15_PKCS7;
                authenticationParameters = new MyIdAuthenticationParameters();
                authenticationParameters.permittedAuthentication = MyIdAuthenticationMechanisms.PIN_ONLY;
                authenticationParameters.pinCallback = this;
                myIDSecurityLibrary.signData(mSignBytes, signingParameters, authenticationParameters, this);
            } catch (InvalidContextException | PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mRootProgressBar.setVisibility(View.GONE);

        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            cancel(true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        public void provideUserPin(final IReturnUserPin callback) {
            try {
                final Dialog alert = new Dialog(LoginActivityScreen.this);
                Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                alert.setContentView(R.layout.custom_dialog_wrong_pin);
                final CustomTextView mCountText = alert.findViewById(R.id.count_remaining);
                final CustomButton mOkBtn = alert.findViewById(R.id.ok_btn);

                if (!callback.isFirstAttempt()) {
                    mCountText.setText(String.valueOf(callback.attemptsRemaining()));
                    count = 1;
                } else if (callback.isFirstAttempt()) {
                    callback.setResult(mOldPin.getText().toString());
                }
                alert.setCancelable(false);
                if (count == 1) {
                    mOkBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callback.setResult("");
                            alert.dismiss();
                            alert.cancel();
                            mConfirmPin.setEnabled(true);
                        }
                    });
                }
                if (count == 1) {
                    alert.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void signingSuccess(MyIdSignDataResponse signDataResponse) {
            if (signDataResponse.signedData == null) {
                // Toast.makeText(LoginActivityScreen.this, "Data Was Not Signed", Toast.LENGTH_SHORT).show();
            } else {

            }
        }

        @Override
        public void signingError(Exception e) {
            Toast.makeText(LoginActivityScreen.this, "Error:" + e, Toast.LENGTH_SHORT).show();
        }
    }

    public void initialise(Context mainContext) {
        try {
            authentication = new Authentication(mainContext);
        } catch (UnlicencedException e) {
            e.printStackTrace();
        }
    }

    public boolean isPinLocked() {
        return authentication.isPinLocked();
    }

}