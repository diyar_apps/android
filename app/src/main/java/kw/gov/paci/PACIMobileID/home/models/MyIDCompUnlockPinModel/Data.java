
package kw.gov.paci.PACIMobileID.home.models.MyIDCompUnlockPinModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("TransactionID")
    @Expose
    private String transactionID;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param transactionID
     * @param deviceId
     */
    public Data(String deviceId, String transactionID) {
        super();
        this.deviceId = deviceId;
        this.transactionID = transactionID;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

}
