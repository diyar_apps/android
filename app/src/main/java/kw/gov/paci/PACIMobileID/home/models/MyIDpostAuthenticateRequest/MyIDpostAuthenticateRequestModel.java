
package kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDpostAuthenticateRequestModel {

    @SerializedName("Identity")
    @Expose
    private Identity identity;
    @SerializedName("Data")
    @Expose
    private Data data;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MyIDpostAuthenticateRequestModel() {
    }

    /**
     * 
     * @param identity
     * @param data
     */
    public MyIDpostAuthenticateRequestModel(Identity identity, Data data) {
        super();
        this.identity = identity;
        this.data = data;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
