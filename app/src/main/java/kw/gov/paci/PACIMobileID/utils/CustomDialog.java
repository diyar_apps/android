package kw.gov.paci.PACIMobileID.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

/**
 * Created by SUNIL KUMAR V on 10/18/2018.
 */
public class CustomDialog extends Dialog implements
        android.view.View.OnClickListener {
    public Activity activity;
    private ImageView mCloseImagvew;
    public String mSerialNumber, mIssuerDN, mSubjectDN, mvaildFrom, mvaildTo, mKeyUsage;
    public CustomTextView mSerialNumbertv, mIssuerDNtv, mSubjectDNtv, mvaildFromtv, mvaildTotv, mKeyUsagetv;

    public CustomDialog(Activity activity, String mSerialNumber, String mIssuerDN, String mSubjectDN, String mvaildFrom, String mvaildTo, String mKeyUsage) {
        super(activity);
        this.activity = activity;
        this.mSerialNumber = mSerialNumber;
        this.mIssuerDN = mIssuerDN;
        this.mSubjectDN = mSubjectDN;
        this.mvaildFrom = mvaildFrom;
        this.mvaildTo = mvaildTo;
        this.mKeyUsage = mKeyUsage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.custom_fragment_dialog_manage);
        setCanceledOnTouchOutside(false);
        mCloseImagvew = findViewById(R.id.close_btn_img);
        mSerialNumbertv = findViewById(R.id.common_name_txt);
        mIssuerDNtv = findViewById(R.id.cn_issuer_txt);
        mSubjectDNtv = findViewById(R.id.valid_from_txt);
        mvaildFromtv = findViewById(R.id.valid_to_txt);
        mvaildTotv = findViewById(R.id.serial_num_txt);
        mKeyUsagetv = findViewById(R.id.digital_sig_txt);
        //setText
        mSerialNumbertv.setText(mIssuerDN);
        mIssuerDNtv.setText(mSubjectDN);
        mSubjectDNtv.setText(mvaildFrom);
        mvaildFromtv.setText(mvaildTo);
        mvaildTotv.setText(mSerialNumber);
        mKeyUsagetv.setText("digitalSignature nonRepudiation");
        mCloseImagvew.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_btn_img:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}