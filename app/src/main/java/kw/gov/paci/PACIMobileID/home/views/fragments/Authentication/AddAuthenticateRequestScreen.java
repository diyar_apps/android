package kw.gov.paci.PACIMobileID.home.views.fragments.Authentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.AuthenSignAdapter;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.AuthenSignModel.AuthenSignModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.MyIDGetAllSigningRequests;
import kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.MyIDJsonArrayListModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/15/2018.
 */
public class AddAuthenticateRequestScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public LinearLayout mNoRequestView;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mCivilIDStr, mFullNameStr;
    public CustomTextView mFullNameEN, mCivilIDNum;
    private RecyclerView recyclerView;
    public RetroApiInterface apiService;
    private AuthenSignModel myResponse;
    private String data = null;
    private ArrayList<kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel.Datum> mAutheSignList;
    private AuthenSignAdapter eAdapter;
    public String ItemName;
    public String mPrompEn, mServiceEn, mQrcodeID;
    public LinearLayout mRootViewNoAthen;
    public ImageView mProfilePic;
    public String mProfilePhoto;
    public Bitmap decodedByte;
    private Resources mResources;
    String language;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editors;
    public String mFullNameAr;
    public SharedPreferences preferences_splash;
    public SharedPreferences.Editor editor_splash;
    private String mdeviceID;
    public JSONObject jObject;
    public int Status_ID, Notifications_Count;
    public LinearLayout mProgressBarLL;
    public String mAuthReqScreenStatus = "mAuthReqScreenStatus";
    public String mAutheGetAllListScreenStatus = "mAutheGetAllListScreenStatus";
    String authorzation_tokentype, mSerialNumber, CivilNo;

    public AddAuthenticateRequestScreen() {
        // Required empty public constructor
    }

    public static AddAuthenticateRequestScreen newInstance() {
        AddAuthenticateRequestScreen fragment = new AddAuthenticateRequestScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_authenticate_request_screen, container, false);
        recyclerView = profileBody.findViewById(R.id.recycler_view);
        mProfilePic = profileBody.findViewById(R.id.profile_pic_imgv);
        mFullNameEN = profileBody.findViewById(R.id.full_en_name);
        mCivilIDNum = profileBody.findViewById(R.id.civiild_id_display);
        mRootViewNoAthen = profileBody.findViewById(R.id.no_request_ll);
        mProgressBarLL = profileBody.findViewById(R.id.progress_bar_dialog_ll);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        preferences_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor_splash = preferences_splash.edit();
        editor_splash.apply();

        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();

        mdeviceID = preferences.getString(("mCarrierID"), ("default"));

        if (Utilities.isNetworkAvailable(getActivity())) {
            // getDeviceDetails(mdeviceID);
           /* tokenIdentity = new TokenIdentity(getActivity());
            tokenIdentity.mGetAccessTokenRequest(mAuthReqScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);*/
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            if (Utilities.isNetworkAvailable(getActivity())) {
                getDeviceDetails(mdeviceID);
            }
        }
        mResources = getResources();
        mSetProfilePicImage();
        setImageResource();
        getDataFromServer();
        mContentView.setVisibility(View.VISIBLE);
        mFullFrame.addView(profileBody);
    }

    private void mSetProfilePicImage() {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mProfilePhoto = preferences.getString(("Photo"), "default");
        mProfilePhoto = (mProfilePhoto);
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    //mSubHeaderImage.setImageBitmap(decodedByte);
                    mRoundedImageConvert();
                } else {
                    mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 25;
        int shadowWidth = 10;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mProfilePic.setImageDrawable(roundedBitmapDrawable);
    }

    @SuppressLint("CommitPrefEdits")
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mCivilIDStr = (mCivilIDStr);
            mFullNameStr = preferences.getString(("FullNameEn"), "default");
            mFullNameStr = (mFullNameStr);
            mFullNameAr = preferences.getString(("FullNameAr"), "default");
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Civil_ID_No), mCivilIDStr);
            mCivilIDNum.setText(text);
            mFullNameEN.setText(mFullNameStr);
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                mFullNameEN.setText(mFullNameAr);
            } else {
                mFullNameEN.setText(mFullNameStr);
            }
            getAllListDatafromServer(getActivity());

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getAllListDatafromServer(Activity activity) {

        if (Utilities.isNetworkAvailable(getActivity())) {
            //tokenIdentity = new TokenIdentity(getActivity());
            //tokenIdentity.mGetAccessTokenRequest(mAutheGetAllListScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            if (Utilities.isNetworkAvailable(getActivity())) {
                getCivilIDDataList(getActivity(), mCivilIDStr);
            }
        }
    }

    private void getCivilIDDataList(Activity activity, String mCivilIDStr) {
        mProgressBarLL.setVisibility(View.VISIBLE);
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.Identity identity = new kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.Identity(mSerialNumber, CivilNo);
        kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.Data data = new kw.gov.paci.PACIMobileID.home.models.MyIDGetAllSigningRequests.Data(mCivilIDStr);
        MyIDGetAllSigningRequests myIDGetAllSigningRequests = new MyIDGetAllSigningRequests(identity, data);
        Call<MyIDJsonArrayListModel> call = apiService.getAllAuthList(authorzation_tokentype, myIDGetAllSigningRequests);
        call.enqueue(new Callback<MyIDJsonArrayListModel>() {
            @Override
            public void onResponse(@NonNull Call<MyIDJsonArrayListModel> call, @NonNull Response<MyIDJsonArrayListModel> response) {
                try {
                    //Log.d("res:", "" + response);
                    mProgressBarLL.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            mAutheSignList = response.body().getData();
                            if (mAutheSignList != null) {
                                eAdapter = new AuthenSignAdapter(mAutheSignList, getActivity());
                                RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getActivity());
                                recyclerView.setLayoutManager(eLayoutManager);
                                recyclerView.setItemAnimator(new DefaultItemAnimator());
                                recyclerView.setAdapter(eAdapter);
                                LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity())).registerReceiver(mMessageReceiver,
                                        new IntentFilter("custom-message-authentication"));
                                if (mAutheSignList.size() == 0) {
                                    mRootViewNoAthen.setVisibility(View.VISIBLE);
                                } else {
                                    mRootViewNoAthen.setVisibility(View.GONE);
                                }
                            } else {
                                mRootViewNoAthen.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (mAutheSignList.size() == 0) {
                        mRootViewNoAthen.setVisibility(View.VISIBLE);
                    } else {
                        mRootViewNoAthen.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Call<MyIDJsonArrayListModel> call, Throwable t) {
                //Log.d("onFailure:", t.getLocalizedMessage());
                // Log.d("throw:", t.getMessage());
                // Log.d("throws:", t.toString());
            }
        });
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.scan_qr_view_ll:
                break;
            case R.id.authe_request_view_ll:
                //Toast.makeText(getActivity(),"No Authentication",Toast.LENGTH_SHORT).show();
                break;
        }
    }

    public void setImageResource() {

    }

    public BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            mPrompEn = intent.getStringExtra("mPrompEN");
            mServiceEn = intent.getStringExtra("mServiceEN");
            mQrcodeID = intent.getStringExtra("mQrcodeID");
            if (Utilities.isNetworkAvailable(getActivity())) {
                //mGetFilefromService(ItemName);
                mListener.goToAddAuthenSignScreen(mPrompEn, mServiceEn, mQrcodeID);
            }
        }
    };

    private void getDeviceDetails(String deviceID) {
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        mdeviceID = preferences.getString(("mCarrierID"), ("default"));
        mCivilIDStr = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mCivilIDStr, mdeviceID);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data1 = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilIDStr, "", mdeviceID);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data1);
        Call<JsonObject> call = apiService.getdvst(BuildConfig.mBasicAuthorization, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    //pDialog.dismiss();
                    data = response.body().toString();
                    if (data != null) {
                        jObject = new JSONObject(data);
                        if (jObject.isNull("Data")) {
                            Toast.makeText(getActivity(), Objects.requireNonNull(response.errorBody()).string(), Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                jObject = new JSONObject(data);
                                JSONObject objectData = jObject.getJSONObject("Data");
                                Status_ID = objectData.getInt("Status");
                                if (Status_ID == 2) {
                                    Notifications_Count = objectData.getInt("NotificationsCount");
                                    editor.putString(("NotificationsCount"), (Integer.toString(Notifications_Count)));
                                    // editor.putString(("Challenge"), (myResponse.getData().getMyidProfileName()));
                                    editor.apply();
                                    ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setText(Integer.toString(Notifications_Count));
                                } else {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(getActivity(), Objects.requireNonNull(response.errorBody()).string(), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
//                    Log.e("splash", e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}