
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.CivilIDPersonalDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessData {

    @SerializedName("OrganizationNameAr")
    @Expose
    private String organizationNameAr;
    @SerializedName("OrganizationNameEn")
    @Expose
    private String organizationNameEn;
    @SerializedName("OrganizationUnitNameAr")
    @Expose
    private String organizationUnitNameAr;
    @SerializedName("OrganizationUnitNameEn")
    @Expose
    private String organizationUnitNameEn;
    @SerializedName("JobTitleAr")
    @Expose
    private String jobTitleAr;
    @SerializedName("JobTitleEn")
    @Expose
    private String jobTitleEn;

    public String getOrganizationNameAr() {
        return organizationNameAr;
    }

    public void setOrganizationNameAr(String organizationNameAr) {
        this.organizationNameAr = organizationNameAr;
    }

    public String getOrganizationNameEn() {
        return organizationNameEn;
    }

    public void setOrganizationNameEn(String organizationNameEn) {
        this.organizationNameEn = organizationNameEn;
    }

    public String getOrganizationUnitNameAr() {
        return organizationUnitNameAr;
    }

    public void setOrganizationUnitNameAr(String organizationUnitNameAr) {
        this.organizationUnitNameAr = organizationUnitNameAr;
    }

    public String getOrganizationUnitNameEn() {
        return organizationUnitNameEn;
    }

    public void setOrganizationUnitNameEn(String organizationUnitNameEn) {
        this.organizationUnitNameEn = organizationUnitNameEn;
    }

    public String getJobTitleAr() {
        return jobTitleAr;
    }

    public void setJobTitleAr(String jobTitleAr) {
        this.jobTitleAr = jobTitleAr;
    }

    public String getJobTitleEn() {
        return jobTitleEn;
    }

    public void setJobTitleEn(String jobTitleEn) {
        this.jobTitleEn = jobTitleEn;
    }

}
