package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class AddPreRequisites extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    private OnAboutMobileCivilIDInteractionListener mListener;
    public ImageView header_Mobile_img;
    public CustomTextView header_Mobile_txt, body_Mobile_txt, body_Mobile_txt_two;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mStatus;

    public AddPreRequisites() {
        // Required empty public constructor
    }

    public static AddPreRequisites newInstance() {
        AddPreRequisites fragment = new AddPreRequisites();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAboutMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_new_prequisites_layout, container, false);
        header_Mobile_img = header.findViewById(R.id.about_civil_header_img);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.pre_requisites_home_body, container, false);
        header_Mobile_txt = profileBody.findViewById(R.id.header_about_civil_id_header_txt);
        body_Mobile_txt = profileBody.findViewById(R.id.about_civil_id_body_txt);
        body_Mobile_txt_two = profileBody.findViewById(R.id.about_civil_id_body_two);
        setImageResource();
        mBody.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.VISIBLE);
        mNextButton.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.button_bg_rounded_corners));
        mPreviousButton.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.button_bg_rounded_corners));
        if (null != mNextButton) {
            mNextButton.setOnClickListener(this);
        }
        if (null != mPreviousButton) {
            mPreviousButton.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_btn:
                preferences = getActivity().getSharedPreferences("splash_pref", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                mStatus = preferences.getString(("Status"), ("default"));
                mListener.goToProcedureScreen();
                break;
            case R.id.previous_btn:
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
                break;
        }
    }

    public void setImageResource() {
        header_Mobile_txt.setText(R.string.screenTwo_content_one);
        body_Mobile_txt.setText(R.string.screenTwo_content_two);
        body_Mobile_txt_two.setText(R.string.screenTwo_content_three);
    }
}



