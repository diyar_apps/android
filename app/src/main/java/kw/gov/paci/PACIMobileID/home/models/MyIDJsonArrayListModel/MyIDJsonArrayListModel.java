
package kw.gov.paci.PACIMobileID.home.models.MyIDJsonArrayListModel;

import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDJsonArrayListModel {

    @SerializedName("Error")
    @Expose
    private Object error;
    @SerializedName("Data")
    @Expose
    private ArrayList<Datum> data = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MyIDJsonArrayListModel() {
    }

    /**
     * 
     * @param error
     * @param data
     */
    public MyIDJsonArrayListModel(Object error, ArrayList<Datum> data) {
        super();
        this.error = error;
        this.data = data;
    }

    public Object getError() {
        return error;
    }

    public void setError(Object error) {
        this.error = error;
    }

    public ArrayList<Datum> getData() {
        return data;
    }

    public void setData(ArrayList<Datum> data) {
        this.data = data;
    }

}
