package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilID;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.feedback.views.activitys.FeedBackActivity;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.mobileidverifier.views.activitys.MobileIDVerifierScanActivity;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomMenuItemTypefaceSpan;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.Locale;
import java.util.Objects;
import java.util.Random;

public class ScannerActivity extends MobileCivilID implements NavigationView.OnNavigationItemSelectedListener {
    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;
    String languageToLoad;
    Locale locale;
    private static final String Locale_Preference = "Locale Preference";
    private static final String Locale_KeyValue = "Saved Locale";
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;
    public static final int YOUR_REQUEST_CODE = 11;
    Intent intent;
    int counter = 0;
    public CustomTextView mHeaderCenterText, mVerificationCode, mFullNameTxt, mCivilIDTxt;
    private CustomButton mCancelBtn;
    public SharedPreferences preferences_addgetmobileid;
    public String generatedRandomkey_string;
    public String mAuthIDScreen, mGetMobileIDScreen;
    public LinearLayout mRootHeaderLL, mRootHeaderLLAuthen;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editorses;
    public String mCivilIDStr, mFullNameStr;

    public SharedPreferences preferencese_spalsh;
    public SharedPreferences.Editor editor_spalsh;
    public String mStatus;
    public CustomTextView mHeaderTextDispaly;
    public RelativeLayout mRootBellIcon, mRootCancelButtonBg;
    public ImageView mProfilePic;
    public String mProfilePhoto;
    public Bitmap decodedByte;
    private Resources mResources;
    String language;
    public SharedPreferences.Editor editors;
    public String mFullNameAr, AddConfirmPinScreenSuccess;
    public CustomTextView mFullNameEN;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_scan_qr_code);
        mProfilePic = findViewById(R.id.profile_pic_imgv);
        mRootBellIcon = findViewById(R.id.badge_rootview);
        mRootCancelButtonBg = findViewById(R.id.bg_cancel_rl_root);
        mRootBellIcon.setVisibility(View.INVISIBLE);
        mHeaderTextDispaly = findViewById(R.id.authe_by_scanning_txt);
        mHeaderCenterText = findViewById(R.id.header_life_coach_txt);
        mRootHeaderLL = findViewById(R.id.add_authenticate_root_ll);
        mRootHeaderLLAuthen = findViewById(R.id.root_view_authe_txt);
        mFullNameTxt = findViewById(R.id.full_en_name);
        mCivilIDTxt = findViewById(R.id.civiild_id_display);
        mHeaderCenterText.setText(R.string.Scan_QR_Code);
        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        mVerificationCode = findViewById(R.id.zxing_status_view);
        mCancelBtn = findViewById(R.id.bottom_button_cancel);
        mFullNameEN = findViewById(R.id.full_en_name);
        mResources = getResources();
        mSetProfilePicImage();
        Intent intent = getIntent();
        Bundle mbundle = intent.getExtras();
        if (mbundle != null) {
            String mAddGetMobileIDScreenID = mbundle.getString("AddGetMobileIDScreen");
            String mAddManageIDScreenID = mbundle.getString("AddManageScreen");
            String mAddAuthencateScreenID = mbundle.getString("AddAuthencateScreen");
            String MIDHomeScannerID = mbundle.getString("MIDHomeScanner");
            if (mAddGetMobileIDScreenID != null && mAddGetMobileIDScreenID.equals("AddGetMobileIDScreenID")) {
                getMobileIDScreen();
            } else if (mAddManageIDScreenID != null && mAddManageIDScreenID.equals("AddManageScreenID")) {
                mRootHeaderLLAuthen.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(ScannerActivity.this), R.color.colorBlack_transparent_progress));
                mHeaderTextDispaly.setText(R.string.Unlock_Scan_content);
                mHeaderTextDispaly.setPadding(0, 350, 0, 100);
                barcodeScannerView.setVisibility(View.VISIBLE);
                mVerificationCode.setVisibility(View.GONE);
                mRootHeaderLL.setVisibility(View.GONE);
                mRootHeaderLLAuthen.setVisibility(View.VISIBLE);
                // mRootHeaderLLAuthen.setPadding(200,0,0,0);
            } else if (mAddAuthencateScreenID != null && mAddAuthencateScreenID.equals("AddAuthencateScreenID")) {
                getAuthenticateScreen();
            } else if (MIDHomeScannerID != null && MIDHomeScannerID.equals("MIDHomeScannerID")) {
                // getAuthenticateScreen();
                mRootHeaderLLAuthen.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(ScannerActivity.this), R.color.colorBlack_transparent_progress));
                mRootCancelButtonBg.setVisibility(View.GONE);
                mHeaderTextDispaly.setText(R.string.Scan_QR_Code_generated_from_another_Mobile_ID_to_verify_it);
                mHeaderTextDispaly.setPadding(0, 350, 0, 100);
                mVerificationCode.setVisibility(View.GONE);
                mRootHeaderLL.setVisibility(View.GONE);
                mRootHeaderLLAuthen.setVisibility(View.VISIBLE);
                mCancelBtn.setVisibility(View.GONE);
            }
        }
        mCancelBtn.setText(R.string.Cancel);
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScannerActivity.this.finish();
            }
        });
        //Initialize barcode scanner view
        Window window = this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.colorBlack));
        sharedPreferences = getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window windows = this.getWindow();
            windows.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            windows.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.getBackground().setAlpha(0);
        if (null != toolbar) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            DrawerLayout drawer = findViewById(R.id.drawer_layout);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);

            toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

                /** Called when a drawer has settled in a completely closed state. */
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    // Do whatever you want here
                }

                /** Called when a drawer has settled in a completely open state. */
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    // Do whatever you want here
                    Utilities.hideKeyboard(ScannerActivity.this);
                }
            };
            // Set the drawer toggle as the DrawerListener
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            NavigationView navigationView = findViewById(R.id.nav_view);
            Menu m = navigationView.getMenu();
            for (int i = 0; i < m.size(); i++) {
                MenuItem mi = m.getItem(i);
                //for aapplying a font to subMenu ...
                SubMenu subMenu = mi.getSubMenu();
                if (subMenu != null && subMenu.size() > 0) {
                    for (int j = 0; j < subMenu.size(); j++) {
                        MenuItem subMenuItem = subMenu.getItem(j);
                        applyFontToMenuItem(subMenuItem);
                    }
                }
                //the method we have create in activity
                applyFontToMenuItem(mi);
                //setHome disable
                preferences = Objects.requireNonNull(ScannerActivity.this).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                mCivilNo = preferences.getString(("CivilNo"), ("default"));
                AddConfirmPinScreenSuccess = preferences.getString("AddConfirmPinScreenSuccess", "default");

                if (AddConfirmPinScreenSuccess.equals("default")) {
                    MenuItem nav_item1 = m.findItem(R.id.nav_home);
                    nav_item1.setEnabled(false);
                    nav_item1.setVisible(false);

                    MenuItem nav_item2 = m.findItem(R.id.nav_verify_identity);
                    nav_item2.setEnabled(false);
                    nav_item2.setVisible(false);

                  /*  MenuItem nav_item3 = m.findItem(R.id.nav_logout);
                    nav_item3.setEnabled(false);
                    nav_item3.setVisible(false);*/
                } else {
                    MenuItem nav_item1 = m.findItem(R.id.nav_home);
                    nav_item1.setEnabled(true);
                    nav_item1.setVisible(true);

                    MenuItem nav_item2 = m.findItem(R.id.nav_verify_identity);
                    nav_item2.setEnabled(true);
                    nav_item2.setVisible(true);

                  /*  MenuItem nav_item3 = m.findItem(R.id.nav_logout);
                    nav_item3.setEnabled(true);
                    nav_item3.setVisible(true);*/
                }
            }
            navigationView.setNavigationItemSelectedListener(this);
            navigationView.setItemIconTintList(null);
        }
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //start capture
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(intent, savedInstanceState);
        capture.decode();
    }

    private void getAuthenticateScreen() {
        barcodeScannerView.setVisibility(View.VISIBLE);
        mVerificationCode.setVisibility(View.GONE);
        mRootHeaderLL.setVisibility(View.VISIBLE);
        mRootHeaderLLAuthen.setVisibility(View.VISIBLE);
        try {
            preferences = Objects.requireNonNull(ScannerActivity.this).getSharedPreferences("plswait", MODE_PRIVATE);
            editorses = preferences.edit();
            editorses.apply();
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mCivilIDStr = (mCivilIDStr);
            mFullNameStr = preferences.getString(("FullNameEn"), "default");
            mFullNameStr = (mFullNameStr);
            mFullNameAr = preferences.getString(("FullNameAr"), "default");
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Civil_ID_No), mCivilIDStr);
            mCivilIDTxt.setText(text);
            mFullNameTxt.setText(mFullNameStr);
            sharedPreferences = ScannerActivity.this.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                mFullNameEN.setText(mFullNameAr);
            } else {
                mFullNameEN.setText(mFullNameStr);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getMobileIDScreen() {
        try {
            preferences_addgetmobileid = Objects.requireNonNull(ScannerActivity.this).getSharedPreferences("randomkey", MODE_PRIVATE);
            generatedRandomkey_string = preferences_addgetmobileid.getString(("RkeyValue"), ("default"));
            generatedRandomkey_string = (generatedRandomkey_string);
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Scan_QRCode_Content), generatedRandomkey_string);
            mVerificationCode.setText(text);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        //showAlert();
        ScannerActivity.this.finish();
    }

    @Override
    protected void onResume() {
        enForcedLocale();
        super.onResume();
        capture.onResume();
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        closeNavigationDrawer();
        int id = item.getItemId();
        Intent intent = null;
        if (id == R.id.nav_home) {
            intent = new Intent(getApplicationContext(), AddHomeActivity.class);
        } else if (id == R.id.nav_about_mobile_civil_id) {
            intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
        } else if (id == R.id.nav_verify_identity) {
            intent = new Intent(getApplicationContext(), MobileIDVerifierScanActivity.class);
        } else if (id == R.id.nav_arabic) {
            if (item.toString().equals("Arabic")) {
                languageToLoad = "ar";
            } else {
                languageToLoad = "en";
            }
            locale = new Locale(languageToLoad);
            editor.putString(Locale_KeyValue, languageToLoad);
            editor.commit();
            Locale.setDefault(locale);
            config.locale = locale;
            this.createConfigurationContext(config);
            this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());
            /*Added load intent locale when back button click*/
            Intent restart = getIntent();
            finish();
            startActivity(restart);
            if (AddConfirmPinScreenSuccess.equals("default")) {
                intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            } else {
                intent = new Intent(getApplicationContext(), AddHomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            }
        } else {
            intent = new Intent(getApplicationContext(), AboutMobileCivilIDActivity.class);
        }
        startActivity(intent);

        overridePendingTransition(R.anim.enter, R.anim.exit);
        ScannerActivity.this.finish();//check once
        return true;
    }

    protected void closeNavigationDrawer() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.commit();
    }

    protected void replaceFragment(int fragmentContainer, Fragment fragment, String id, boolean isAddToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.replace(fragmentContainer, fragment, id);
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(id);
        }
        fragmentTransaction.commit();
    }

    public void goToPreviousScreen() {
        FragmentManager fm = getSupportFragmentManager();
        if (isTaskRoot() && fm.getBackStackEntryCount() == 0) {
            //showAlert();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/gotham_book.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomMenuItemTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void mSetProfilePicImage() {
        preferences = Objects.requireNonNull(ScannerActivity.this.getSharedPreferences("plswait", MODE_PRIVATE));
        editor = preferences.edit();
        editor.apply();
        mProfilePhoto = preferences.getString(("Photo"), "default");
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(ScannerActivity.this), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    mRoundedImageConvert();
                } else {
                    mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(ScannerActivity.this), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 25;
        int shadowWidth = 10;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mProfilePic.setImageDrawable(roundedBitmapDrawable);
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }
}
