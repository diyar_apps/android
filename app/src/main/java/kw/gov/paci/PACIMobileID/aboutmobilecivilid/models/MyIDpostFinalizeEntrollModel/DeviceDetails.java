
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeviceDetails {

    @SerializedName("DeviceUniqueId")
    @Expose
    private String deviceUniqueId;
    @SerializedName("NotificationDeviceUniqueId")
    @Expose
    private String notificationDeviceUniqueId;
    @SerializedName("DeviceFamily")
    @Expose
    private String deviceFamily;
    @SerializedName("DeviceOS")
    @Expose
    private Integer deviceOS;
    @SerializedName("DeviceModel")
    @Expose
    private String deviceModel;
    @SerializedName("IsJailbroken")
    @Expose
    private Boolean isJailbroken;
    @SerializedName("PlatformVersion")
    @Expose
    private String platformVersion;
    @SerializedName("IdentityType")
    @Expose
    private String identityType;
    @SerializedName("Certificate")
    @Expose
    private Certificate certificate;

    @SerializedName("SerialNumber")
    @Expose
    public String mSerialNumber;
    @SerializedName("KeyStoreType")
    @Expose
    public String KeyStoreType;

    /*public DeviceDetails(Certificate certificate) {
      this.certificate=certificate;
    }*/

    public DeviceDetails(String deviceUniqueId, String notificationDeviceUniqueId, String deviceFamily, int i, String deviceModel, boolean b, String platformVersion, String identityType, Certificate certificate, String mSerialNumber, String KeystoreType) {
        this.deviceUniqueId=deviceUniqueId;
        this.notificationDeviceUniqueId=notificationDeviceUniqueId;
        this.deviceFamily=deviceFamily;
        this.deviceOS=i;
        this.deviceModel=deviceModel;
        this.isJailbroken=b;
        this.platformVersion=platformVersion;
        this.identityType=identityType;
        this.certificate=certificate;
        this.mSerialNumber=mSerialNumber;
        this.KeyStoreType=KeystoreType;
    }

    public String getDeviceUniqueId() {
        return deviceUniqueId;
    }

    public void setDeviceUniqueId(String deviceUniqueId) {
        this.deviceUniqueId = deviceUniqueId;
    }

    public String getNotificationDeviceUniqueId() {
        return notificationDeviceUniqueId;
    }

    public void setNotificationDeviceUniqueId(String notificationDeviceUniqueId) {
        this.notificationDeviceUniqueId = notificationDeviceUniqueId;
    }

    public String getDeviceFamily() {
        return deviceFamily;
    }

    public void setDeviceFamily(String deviceFamily) {
        this.deviceFamily = deviceFamily;
    }

    public Integer getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(Integer deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public Boolean getIsJailbroken() {
        return isJailbroken;
    }

    public void setIsJailbroken(Boolean isJailbroken) {
        this.isJailbroken = isJailbroken;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public void setPlatformVersion(String platformVersion) {
        this.platformVersion = platformVersion;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

}
