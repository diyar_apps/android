package kw.gov.paci.PACIMobileID.home.views.activitys;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Objects;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.AboutMobileCivilIDActivity;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilID;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.home.controllers.AddActivityLogAdapter;
import kw.gov.paci.PACIMobileID.home.controllers.BottomNavigationViewHelper;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.fragments.AddAddressFragment;
import kw.gov.paci.PACIMobileID.home.views.fragments.AddHomeFragment;
import kw.gov.paci.PACIMobileID.home.views.fragments.AddNotifiAuthDigitalSignList;
import kw.gov.paci.PACIMobileID.home.views.fragments.AddVerifyFragment;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddAuthSignDeclinePinScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddAuthenSignFragment;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddAuthenticateRequestScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddAuthenticateScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddPinRequestScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddPinSuccessfulScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.DigitalSign.AddDigitalSignDeclinePinScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.DigitalSign.AddDigitalSignScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.DigitalSign.AddWebviewSignScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddActivityLogsScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddCancelMobileCivilIDFragment;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddCancelPinRequestScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddManageScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddOldPinRequestScreen;
import kw.gov.paci.PACIMobileID.utils.MyUserProfileDialog;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomMenuItemTypefaceSpan;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by SUNIL KUMAR V on 10/1/2018.
 */
public class AddHomeActivity extends MobileCivilID implements OnAddHomeInteractionListener, View.OnClickListener {
    private CustomTextView mHeaderCenterText;
    protected CustomButton mSubHeaderAdrees, mSubheaderverify;
    private ImageView mSubHeaderImage;
    public String mProfilePhoto;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public RelativeLayout mBottomNavigationBarRoot;
    private BottomNavigationView navigation;
    public String KeyQRcodeStr;
    public String message, mStatus;
    public Bitmap decodedByte;
    public String mCategoryID;
    public TextView mNotifiBadgeCount;
    public RelativeLayout mRootViewBadge;
    public static int SERVER_TIME_OUT = 100;
    private Resources mResources;
    public String mNotificationNumberDisplay, mNotifiBadgeCountNumber, mNotifiIDValue;
    public SharedPreferences preferences_splash;
    public SharedPreferences.Editor editor_splash;
    private String mdeviceID;
    public JSONObject jObject;
    private RetroApiInterface apiService;
    private String data = null;
    public int Status_ID, Notifications_Count;
    public LinearLayout mRootProgressBar;
    public static int SPLASH_TIME_OUT = 1000;
    public String mCivilID, mConfirmPinStr;
    public String mHomeScreenStatus = "mHomeScreenStatus";
    String authorzation_tokentype;
    Toolbar toolbar;
    public static AddActivityLogAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_root_view);
        super.onCreate(savedInstanceState);
        getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        mResources = getResources();
        mHeaderCenterText = findViewById(R.id.header_life_coach_txt);
        mRootViewBadge = findViewById(R.id.badge_rootview);
        mRootViewBadge.setVisibility(View.VISIBLE);
        mNotifiBadgeCount = findViewById(R.id.badge_notification_2);
        mBottomNavigationBarRoot = findViewById(R.id.nav_bar_bottom_root);
        mSubHeaderImage = findViewById(R.id.user_pic_img_view);
        mSubHeaderAdrees = findViewById(R.id.address_btn);
        mSubheaderverify = findViewById(R.id.verify_btn);
        navigation = findViewById(R.id.navigation);
        mRootProgressBar = findViewById(R.id.progress_bar_dialog_root);
        toolbar = findViewById(R.id.toolbar);

        //setVisibility
        mBottomNavigationBarRoot.setVisibility(View.VISIBLE);
        mSubHeaderImage.setVisibility(View.VISIBLE);
        mSubHeaderAdrees.setVisibility(View.VISIBLE);
        mSubheaderverify.setVisibility(View.VISIBLE);

        mHeaderCenterText.setText(R.string.title_header_my_civilid);
        mHeaderCenterText.setPadding(0, 0, 100, 0);

        preferences = Objects.requireNonNull(AddHomeActivity.this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();

        preferences_splash = Objects.requireNonNull(AddHomeActivity.this).getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor_splash = preferences_splash.edit();
        editor_splash.apply();

        mdeviceID = preferences.getString(("mCarrierID"), ("default"));

        mCivilID = preferences.getString("CivilNo", "default");
        mConfirmPinStr = preferences.getString("mConfirmPinED", "default");

        if (Utilities.isNetworkAvailable(AddHomeActivity.this)) {
            getDeviceDetails(mdeviceID);
           /* tokenIdentity = new TokenIdentity(AddHomeActivity.this);
            tokenIdentity.mGetAccessTokenRequest(mHomeScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);*/
        }

        mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
        mNotifiBadgeCount.setText(mNotificationNumberDisplay);

        mProfilePhoto = preferences.getString(("Photo"), "default");
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mSubHeaderImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(AddHomeActivity.this), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    mRoundedImageConvert();
                } else {
                    mSubHeaderImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(AddHomeActivity.this), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        mSubHeaderImage.setOnClickListener(this);
        mSubHeaderAdrees.setOnClickListener(this);
        mSubheaderverify.setOnClickListener(this);
        mRootViewBadge.setOnClickListener(this);

        customFontSetMenuItems();

        BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) navigation.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            try {
                final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
                final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
                final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
                layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
                layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 32, displayMetrics);
                iconView.setLayoutParams(layoutParams);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        navigation.setOnClickListener(this);
        try {
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                mCategoryID = bundle.getString("categoryId");
                mNotifiBadgeCountNumber = bundle.getString("mNotifiBadgeCountNumber");
                mNotifiIDValue = bundle.getString("mNotifiIDValue");
                editor.putString(("mNotifiIDValue"), (mNotifiIDValue));
                editor.apply();
                if (mCategoryID != null && mCategoryID.equals("AUTHENTICATION")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
                                mClearSharedPref();
                                new mInitialScreenHandler().invoke();
                            } else {
                                navigation.setSelectedItemId(R.id.ic_authu_bottombar);
                                View view = navigation.findViewById(R.id.ic_authu_bottombar);
                                view.performClick();
                                mHeaderImageProfileVisibility();
                                setFragment(R.id.root_content, AddAuthenticateRequestScreen.newInstance(),
                                        AddAuthenticateRequestScreen.class.getSimpleName());
                            }
                        }
                    }, SERVER_TIME_OUT);
                } else if (mCategoryID != null && mCategoryID.equals("SIGNING")) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            navigation.setSelectedItemId(R.id.ic_disgn_sign_bottombar);
                            View view = navigation.findViewById(R.id.ic_disgn_sign_bottombar);
                            view.performClick();
                            mHeaderImageProfileVisibility();
                            setFragment(R.id.root_content, AddDigitalSignScreen.newInstance(), AddDigitalSignScreen.class.getSimpleName());
                        }
                    }, SERVER_TIME_OUT);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mHeaderImageProfileVisibility();
                            setFragment(R.id.root_content, AddHomeFragment.newInstance(),
                                    AddHomeFragment.class.getSimpleName());
                        }
                    }, SERVER_TIME_OUT);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setFragment(R.id.root_content, AddHomeFragment.newInstance(), AddHomeFragment.class.getSimpleName());
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 12;
        int shadowWidth = 10;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mSubHeaderImage.setImageDrawable(roundedBitmapDrawable);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            goToPreviousScreen();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_btn:
                setFragment(R.id.root_content, AddAddressFragment.newInstance(), AddAddressFragment.class.getSimpleName());
                break;
            case R.id.verify_btn:
                setFragment(R.id.root_content, AddVerifyFragment.newInstance(), AddVerifyFragment.class.getSimpleName());
                break;
            case R.id.user_pic_img_view:
                //user selected image
                MyUserProfileDialog customDialog = new MyUserProfileDialog(AddHomeActivity.this, decodedByte);
                Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                customDialog.show();
                break;
            case R.id.badge_rootview:
                navigation.setVisibility(View.GONE);
                mRootViewBadge.setVisibility(View.INVISIBLE);
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                toolbar.setNavigationIcon(R.drawable.arrow_for);
                setSupportActionBar(toolbar);
                toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getApplicationContext(), AddHomeActivity.class);
                        startActivity(intent);
                    }
                });
                mHeaderCenterText.setText(R.string.Notifications);

                mHeaderImageProfileVisibility();
                setFragment(R.id.root_content, AddNotifiAuthDigitalSignList.newInstance(),
                        AddNotifiAuthDigitalSignList.class.getSimpleName());
                break;
        }

    }

    private void customFontSetMenuItems() {
        Menu m = navigation.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(AddHomeActivity.this.getAssets(), "fonts/gotham_book.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomMenuItemTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.ic_mb_cv_id_bottombar:
                    mHeaderCenterText.setText(R.string.title_header_my_civilid);
                    mBottomNavigationBarRoot.setVisibility(View.VISIBLE);
                    mSubHeaderImage.setVisibility(View.VISIBLE);
                    mSubHeaderAdrees.setVisibility(View.VISIBLE);
                    mSubheaderverify.setVisibility(View.VISIBLE);
                    setFragment(R.id.root_content, AddHomeFragment.newInstance(), AddHomeFragment.class.getSimpleName());
                    return true;
                case R.id.ic_authu_bottombar:
                    mHeaderCenterText.setText(R.string.title_header_name_authen);
                    mHeaderCenterText.setPadding(20, 20, 20, 20);
                    mHeaderImageProfileVisibility();
                    setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                    return true;
                case R.id.ic_disgn_sign_bottombar:
                    mHeaderCenterText.setText(R.string.title_header_name_digi_sign);
                    mHeaderImageProfileVisibility();
                    setFragment(R.id.root_content, AddDigitalSignScreen.newInstance(), AddDigitalSignScreen.class.getSimpleName());
                    return true;
                case R.id.ic_manage_bottombar:
                    mHeaderCenterText.setText(R.string.title_header_name_manage);
                    mHeaderImageProfileVisibility();
                    setFragment(R.id.root_content, AddManageScreen.newInstance(), AddManageScreen.class.getSimpleName());
                    return true;
            }
            return false;
        }
    };

    private void mHeaderImageProfileVisibility() {
        mSubHeaderImage.setVisibility(View.GONE);
        mSubHeaderAdrees.setVisibility(View.GONE);
        mSubheaderverify.setVisibility(View.GONE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanResult.getContents() != null) {
                String re = scanResult.getContents();
                KeyQRcodeStr = re;
                message = re;
                if (message.contains(",")) {
                    if (message.contains("PACMD-")) {
                        mStatus = "ManageScreen";
                        AddPinRequestScreen argumentFragment = new AddPinRequestScreen();//Get Fragment Instance
                        Bundle data = new Bundle();//Use bundle to pass data
                        data.putString("KeyQrCode", KeyQRcodeStr);//put string, int, etc in bundle with a key value
                        argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                        replaceFragment(R.id.root_content, AddPinRequestScreen.newInstance(message, mStatus),
                                AddPinRequestScreen.class.getSimpleName(), true);
                    } else {
                        Toast.makeText(AddHomeActivity.this, R.string.invalid_qr_code, Toast.LENGTH_SHORT).show();
                        intent = new Intent(AddHomeActivity.this, AddHomeActivity.class);
                        startActivity(intent);
                        // Objects.requireNonNull(AddHomeActivity.this).finish();
                    }
                } else {
                    if (message.endsWith("==")) {
                        mStatus = "test";
                        AddPinRequestScreen argumentFragment = new AddPinRequestScreen();//Get Fragment Instance
                        Bundle data = new Bundle();//Use bundle to pass data
                        data.putString("KeyQrCode", KeyQRcodeStr);//put string, int, etc in bundle with a key value
                        argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                        replaceFragment(R.id.root_content, AddPinRequestScreen.newInstance(message, mStatus),
                                AddPinRequestScreen.class.getSimpleName(), true);
                    } else {
                        Toast.makeText(AddHomeActivity.this, R.string.invalid_qr_code, Toast.LENGTH_SHORT).show();
                        intent = new Intent(AddHomeActivity.this, AddHomeActivity.class);
                        startActivity(intent);
                    }
                }
            }
        }
    }

    @Override
    public void goToAddAuthenticateRequestScreen() {
        setFragment(R.id.root_content, AddAuthenticateRequestScreen.newInstance(),
                AddAuthenticateRequestScreen.class.getSimpleName());
    }

    @Override
    public void goToAddPinRequestScreen() {
        setFragment(R.id.root_content, AddPinRequestScreen.newInstance("test", "test"),
                AddPinRequestScreen.class.getSimpleName());
    }

    @Override
    public void goToAddPinSuccessfulScreen() {
        setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance("default"),
                AddPinSuccessfulScreen.class.getSimpleName());
    }

    @Override
    public void goToAddWebviewSignScreen(String s1, String s2) {
        setFragment(R.id.root_content, AddWebviewSignScreen.newInstance(s1, s2),
                AddWebviewSignScreen.class.getSimpleName());
    }

    @Override
    public void goToAddAuthenSignScreen(String s1, String s2, String QrcodeId) {
        setFragment(R.id.root_content, AddAuthenSignFragment.newInstance(s1, s2, QrcodeId),
                AddAuthenSignFragment.class.getSimpleName());
    }

    @Override
    public void goToAddCancelMobileCivilIDScreen() {
        setFragment(R.id.root_content, AddCancelMobileCivilIDFragment.newInstance(),
                AddCancelMobileCivilIDFragment.class.getSimpleName());
    }

    @Override
    public void goToOldPinChangeScreen() {
        setFragment(R.id.root_content, AddOldPinRequestScreen.newInstance(),
                AddOldPinRequestScreen.class.getSimpleName());
    }

    @Override
    public void goToAddAuthSignDeclinePinScreen(String msg, String mSigingStatus) {
        setFragment(R.id.root_content, AddAuthSignDeclinePinScreen.newInstance(msg, mSigingStatus),
                AddAuthSignDeclinePinScreen.class.getSimpleName());
    }

    @Override
    public void goToAddDigitalSignDeclinePinScreen(String msg, String mFileNamePath) {
        setFragment(R.id.root_content, AddDigitalSignDeclinePinScreen.newInstance(msg, mFileNamePath),
                AddDigitalSignDeclinePinScreen.class.getSimpleName());
    }

    @Override
    public void goToAddNotifiAuthDigitalSignList() {
        setFragment(R.id.root_content, AddNotifiAuthDigitalSignList.newInstance(),
                AddNotifiAuthDigitalSignList.class.getSimpleName());
    }

    @Override
    public void goToActivityLogScreen() {
        setFragment(R.id.root_content, AddActivityLogsScreen.newInstance(),
                AddActivityLogsScreen.class.getSimpleName());
    }

    @Override
    public void goToAddCancelPinRequestScreen(String s1, String s2) {
        setFragment(R.id.root_content, AddCancelPinRequestScreen.newInstance(s1, s2),
                AddCancelPinRequestScreen.class.getSimpleName());
    }

    @Override
    protected void onResume() {
        enForcedLocale();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private void getDeviceDetails(String deviceID) {
        mRootProgressBar.setVisibility(View.VISIBLE);
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Identity identity = new Identity(mCivilID, mdeviceID);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data1 = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilID, "", mdeviceID);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data1);
        Call<JsonObject> call = apiService.getdvst(BuildConfig.mBasicAuthorization, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mRootProgressBar.setVisibility(View.GONE);
                    data = response.body().toString();
                    if (data != null) {
                        jObject = new JSONObject(data);
                        if (jObject.isNull("Data")) {
                            Toast.makeText(AddHomeActivity.this, "", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                jObject = new JSONObject(data);
                                JSONObject objectData = jObject.getJSONObject("Data");
                                Status_ID = objectData.getInt("Status");
                                if (Status_ID == 2) {
                                    Notifications_Count = objectData.getInt("NotificationsCount");
                                    editor.putString(("NotificationsCount"), (Integer.toString(Notifications_Count)));
                                    editor.apply();

                                    mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
                                    mNotifiBadgeCount.setText(mNotificationNumberDisplay);
                                } else {

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        //Toast.makeText(AddHomeActivity.this, "failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(AddHomeActivity.this, "", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(AddHomeActivity.this, "", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class mInitialScreenHandler {
        void invoke() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(AddHomeActivity.this, AboutMobileCivilIDActivity.class);
                    startActivity(intent);
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void mClearSharedPref() {
        preferences = this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }
}