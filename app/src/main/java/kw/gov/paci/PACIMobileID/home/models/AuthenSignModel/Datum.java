
package kw.gov.paci.PACIMobileID.home.models.AuthenSignModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("Challenge")
    @Expose
    private String challenge;
    @SerializedName("Id")
    @Expose
    private String id;
    @SerializedName("PersonCivilNo")
    @Expose
    private String personCivilNo;
    @SerializedName("PromptAr")
    @Expose
    private String promptAr;
    @SerializedName("PromptEn")
    @Expose
    private String promptEn;
    @SerializedName("ServiceNameEN")
    @Expose
    private String serviceNameEN;
    @SerializedName("ServiceNameAR")
    @Expose
    private String serviceNameAR;
    @SerializedName("RID")
    @Expose
    private Integer rID;
    @SerializedName("StatusId")
    @Expose
    private Integer statusId;

    public String getChallenge() {
        return challenge;
    }

    public void setChallenge(String challenge) {
        this.challenge = challenge;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPersonCivilNo() {
        return personCivilNo;
    }

    public void setPersonCivilNo(String personCivilNo) {
        this.personCivilNo = personCivilNo;
    }

    public String getPromptAr() {
        return promptAr;
    }

    public void setPromptAr(String promptAr) {
        this.promptAr = promptAr;
    }

    public String getPromptEn() {
        return promptEn;
    }

    public void setPromptEn(String promptEn) {
        this.promptEn = promptEn;
    }

    public String getServiceNameEN() {
        return serviceNameEN;
    }

    public void setServiceNameEN(String serviceNameEN) {
        this.serviceNameEN = serviceNameEN;
    }

    public String getServiceNameAR() {
        return serviceNameAR;
    }

    public void setServiceNameAR(String serviceNameAR) {
        this.serviceNameAR = serviceNameAR;
    }

    public Integer getRID() {
        return rID;
    }

    public void setRID(Integer rID) {
        this.rID = rID;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

}
