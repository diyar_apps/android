package kw.gov.paci.PACIMobileID.baseclasses;

import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.Objects;

public abstract class MobileCivilIDBaseFragment extends Fragment implements kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseInterface {

    protected FrameLayout mHeader;
    protected FrameLayout mBody;
    protected FrameLayout mFooter, mFullFrame;
    protected CustomButton mNextButton;
    protected CustomButton mPreviousButton;
    protected CustomButton mInfoButton, mCancelButton;
    //protected FloatingActionButton mFloatingActionButton;
    protected RelativeLayout mContentView;
    protected RelativeLayout mContentViewGradient;
    protected ImageView mQRCodeImg;
    protected CustomTextView mGetMobileIDText;
    protected RelativeLayout mRootView;
    // protected LinearLayout mRootProgressBar;

    public MobileCivilIDBaseFragment() {
        // default constructor.
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.base_view, container, false);
        mContentView = view.findViewById(R.id.content);
        mContentViewGradient = view.findViewById(R.id.bg_content);
        mHeader = mContentView.findViewById(R.id.header);
        mBody = mContentView.findViewById(R.id.body);
        mQRCodeImg = mContentView.findViewById(R.id.qr_code_civil_id);
        mGetMobileIDText = mContentView.findViewById(R.id.get_mobile_id_txt);
        mFooter = mContentView.findViewById(R.id.footer);
        mFullFrame = mContentView.findViewById(R.id.frame_scan_qr);
        // mRootProgressBar=mContentView.findViewById(R.id.progress_bar_dialog_root);
        //mFloatingActionButton = view.findViewById(R.id.fab);
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            Objects.requireNonNull(getActivity()).setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setBGThemeResource();
        setFooter(container);
        setHeader(container, inflater);
        setBody(container, inflater);
        // setImageResource();
        return view;
    }

    @Override
    public void setFooter(ViewGroup container) {
        mRootView = mFooter.findViewById(R.id.ll_footer);
        mPreviousButton = mFooter.findViewById(R.id.previous_btn);
        mNextButton = mFooter.findViewById(R.id.next_btn);
        mInfoButton = mFooter.findViewById(R.id.info_btn);
        mCancelButton = mFooter.findViewById(R.id.cancel_btn);
    }

    protected void removeFooterView() {

    }

    protected void setBGThemeResource() {
        mContentView.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.bg));
    }

    private void setFabColor(int colorCode) {

    }

    @Override
    public abstract void setHeader(ViewGroup container, LayoutInflater inflater);

    @Override
    public abstract void setBody(ViewGroup container, LayoutInflater inflater);
}
