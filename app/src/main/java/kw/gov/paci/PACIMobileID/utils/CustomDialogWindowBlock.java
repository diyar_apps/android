package kw.gov.paci.PACIMobileID.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;

import kw.gov.paci.PACIMobileID.R;

/**
 * Created by SUNIL KUMAR V on 1/10/2019.
 */
public class CustomDialogWindowBlock extends Dialog implements
        View.OnClickListener {
    public Activity activity;
    public static int SPLASH_TIME_OUT = 8000;

    public CustomDialogWindowBlock(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.custom_dialog_window_block);
        setCanceledOnTouchOutside(false);
        //mCloseImagvew.setOnClickListener(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_btn_img:
                dismiss();
                break;
            default:
                break;
        }
        dismiss();
    }
}