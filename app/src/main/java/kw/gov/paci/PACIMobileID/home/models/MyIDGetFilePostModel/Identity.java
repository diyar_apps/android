
package kw.gov.paci.PACIMobileID.home.models.MyIDGetFilePostModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Identity {

    @SerializedName("MobileUniqueId")
    @Expose
    private String mobileUniqueId;
    @SerializedName("CivilNo")
    @Expose
    private String civilNo;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Identity() {
    }

    /**
     * 
     * @param civilNo
     * @param mobileUniqueId
     */
    public Identity(String mobileUniqueId, String civilNo) {
        super();
        this.mobileUniqueId = mobileUniqueId;
        this.civilNo = civilNo;
    }

    public String getMobileUniqueId() {
        return mobileUniqueId;
    }

    public void setMobileUniqueId(String mobileUniqueId) {
        this.mobileUniqueId = mobileUniqueId;
    }

    public String getCivilNo() {
        return civilNo;
    }

    public void setCivilNo(String civilNo) {
        this.civilNo = civilNo;
    }

}
