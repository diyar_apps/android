package kw.gov.paci.PACIMobileID.home.views.fragments.Manage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.HeadlessUnlockPinCallbackHandler;
import kw.gov.paci.PACIMobileID.utils.Constants;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomEditTextView;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.fxn.stash.Stash;
import com.intercede.Authentication;
import com.intercede.IdentityDoesNotExistException;
import com.intercede.IncorrectPinException;
import com.intercede.NewPinMustBeDifferentException;
import com.intercede.PinLockedException;
import com.intercede.PinPolicyNotMetException;
import com.intercede.myIDSecurityLibrary.UnlicencedException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 11/10/2018.
 */
public class AddOldPinRequestScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public CustomTextView mPlsEnterHeadertxt, mPinMustBetxt, mItisHighlytxt, pinlocked;
    public LinearLayout mOldPinViewLL, mConfirmPinViewLL, mEnterPinViewLL;
    public CustomButton mConfirmRequestPinBtn;
    public SharedPreferences preferences, preferencese_splash;
    public SharedPreferences.Editor editor, editor_splash;
    public String mConfirmPinStr;
    public CustomEditTextView mEnterPinValue, mOldPinEd, mEnterConfirmNewPinValue;
    private boolean mIsLoginEnable = true;
    private int mCurrentLoginFailCount = 0;
    private Long mLastEnteredWrongPassword = 0L;
    public String mCurrentLoginFailCountShared_pref;
    boolean exceptionThrown = false;
    boolean locked;
    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";
    static public HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;

    public AddOldPinRequestScreen() {
        // Required empty public constructor
    }

    public static AddOldPinRequestScreen newInstance() {
        AddOldPinRequestScreen fragment = new AddOldPinRequestScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        locked = headlessUnlockPinCallbackHandler.isPinLocked();
        if (!locked) {
            pinlocked.setVisibility(View.GONE);
        } else {
            pinlocked.setVisibility(View.VISIBLE);
            pinlocked.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
            // pinlocked.setText(locked ? R.string.label_pin_locked : R.string.label_pin_not_locked);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            pinlocked.startAnimation(anim);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_home_enter_pin, container, false);
        mOldPinEd = profileBody.findViewById(R.id.et_old_pin);
        mOldPinEd.setHint(R.string.the_old_pin);
        pinlocked = profileBody.findViewById(R.id.pinlocked);
        mPlsEnterHeadertxt = profileBody.findViewById(R.id.header_enter_pin_edtxt);
        mEnterPinValue = profileBody.findViewById(R.id.et_new_pin);
        mEnterPinValue.setHint(R.string.the_new_pin);
        mEnterConfirmNewPinValue = profileBody.findViewById(R.id.et_confrm_pin);
        mEnterConfirmNewPinValue.setHint(R.string.the_confirm_new_pin);
        mConfirmRequestPinBtn = profileBody.findViewById(R.id.confirm_request_pin);
        mItisHighlytxt = profileBody.findViewById(R.id.it_is_highly_txt);
        mPinMustBetxt = profileBody.findViewById(R.id.pin_must_txt);
        mOldPinViewLL = profileBody.findViewById(R.id.pin_old_layout_root);
        mConfirmPinViewLL = profileBody.findViewById(R.id.pin_confrm_layout_root);
        mEnterPinViewLL = profileBody.findViewById(R.id.pin_new_layout_root);
        mPlsEnterHeadertxt.setText(R.string.Unlock_PIN_content);
        mItisHighlytxt.setVisibility(View.VISIBLE);
        mPinMustBetxt.setVisibility(View.VISIBLE);
        mConfirmPinViewLL.setVisibility(View.VISIBLE);
        mOldPinViewLL.setVisibility(View.VISIBLE);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mConfirmRequestPinBtn.setOnClickListener(this);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_request_pin:
                if (!locked) {
                    preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                    editor = preferences.edit();
                    editor.apply();
                    mConfirmPinStr = preferences.getString("mConfirmPinED", "default");
                    if (mOldPinEd.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_Old_PIN, Toast.LENGTH_SHORT).show();
                    } else if (mEnterPinValue.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                    } else if (mEnterConfirmNewPinValue.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_Confirm_your_PIN, Toast.LENGTH_SHORT).show();
                    } else if (mOldPinEd.getText().toString().length() <= 3 || mEnterPinValue.getText().toString().length() <= 3 || mEnterConfirmNewPinValue.getText().toString().length() <= 3) {
                        Toast.makeText(getContext(), R.string.PIN_Must_be_between_4_and_12_characters, Toast.LENGTH_SHORT).show();
                    }/* else if (mConfirmPinStr.equals(mEnterConfirmNewPinValue.getText().toString())) {
                        Toast.makeText(getContext(), "The new and old PIN must be different.", Toast.LENGTH_SHORT).show();
                    }*/ else if (Pattern.matches("^(0123|1234|2345|3456|4567|5678|6789|3210|4321|5432|6543|7654|8765|9876|0000|1111|2222|3333|4444|5555|6666|7777|8888|9999)$", mEnterPinValue.getText().toString())) {
                        Toast.makeText(getContext(), R.string.the_sequence_and_same_num_not_allowed, Toast.LENGTH_SHORT).show();
                    }
                   /* else if (mEnterPinValue.getText().toString().equals(mEnterConfirmNewPinValue)) {
                        Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                    }*/
                    else if (mEnterPinValue.getText().toString().equals(mEnterConfirmNewPinValue.getText().toString())) {
                        if (mOldPinEd.getText().toString().equals(mConfirmPinStr)) {
                            try {
                                Authentication auth = new Authentication(getActivity());
                                if (mEnterPinValue.getText().toString().equals(mEnterConfirmNewPinValue.getText().toString())) {
                                    auth.changeUserPin(mOldPinEd.getText().toString(), mEnterConfirmNewPinValue.getText().toString());
                                    editor.putString("mConfirmPinED", mEnterConfirmNewPinValue.getText().toString());
                                    editor.apply();
                                } else {
                                    boolean exceptionThrown = false;
                                    try {
                                        auth = new Authentication(getActivity());
                                        auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                                    } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                                        e.printStackTrace();
                                    } catch (IncorrectPinException e) {
                                        exceptionThrown = true;
                                        StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                        errorMsg.append("\n");
                                        errorMsg.append(e.numberOfAttemptsRemaining);
                                        errorMsg.append(" attempts remaining.");
                                        Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                                        e.printStackTrace();
                                    }
                                }

                            } catch (IncorrectPinException e) {
                                exceptionThrown = true;
                                StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                errorMsg.append("\n");
                                errorMsg.append(e.numberOfAttemptsRemaining);
                                errorMsg.append(" attempts remaining.");
                                //result.setText(errorMsg);
                                Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();
                                // mResetValues();
                            } catch (Exception e) {
                                exceptionThrown = true;
                                /*StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                errorMsg.append("\n");
                                errorMsg.append(e.getLocalizedMessage());
                                //result.setText(errorMsg);
                                Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_SHORT).show();*/
                                Toast.makeText(getContext(), R.string.the_new_and_old_pin_must_be_different, Toast.LENGTH_SHORT).show();
                                // mResetValues();
                            }

                            if (!exceptionThrown) {
                                //result.setText(getString(R.string.pin_successfully_changed));
                                Date today = new Date();
                                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.ENGLISH);
                                String dateToStr = format.format(today);
                                System.out.println(dateToStr);
                                MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_manage), getString(R.string.activity_log_title_changepin), "", getString(R.string.Your_Pin_Changed_Successfully), dateToStr));
                                saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                                Toast.makeText(getActivity(), R.string.Your_Pin_Changed_Successfully, Toast.LENGTH_SHORT).show();
                                setFragment(R.id.root_content, AddManageScreen.newInstance(), AddManageScreen.class.getSimpleName());
                            }
                        } else {
                            //Toast.makeText(getContext(), R.string.the_new_and_old_pin_must_be_different, Toast.LENGTH_SHORT).show();
                            boolean exceptionThrown = false;
                            try {
                                // Authentication auth = new Authentication(getActivity());
                                Authentication auth = new Authentication(getActivity());
                                auth.changeUserPin(mOldPinEd.getText().toString(), mEnterConfirmNewPinValue.getText().toString());
                            } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                                e.printStackTrace();
                            } catch (IncorrectPinException e) {
                                exceptionThrown = true;
                                StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                errorMsg.append("\n");
                                errorMsg.append(e.numberOfAttemptsRemaining);
                                errorMsg.append(" attempts remaining.");
                                Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                    }
                } else {
                   /* Animation animation1 = AnimationUtils.loadAnimation(getActivity(), R.anim.blink);
                    pinlocked.startAnimation(animation1);*/
                    Animation anim = new AlphaAnimation(0.0f, 1.0f);
                    anim.setDuration(500); //You can manage the time of the blink with this parameter
                    //anim.setStartOffset(20);
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    pinlocked.startAnimation(anim);
                }
                break;
        }
    }

    public void setImageResource() {

    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        try {
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(fragmentContainer, fragment, id);
            //fragmentTransaction.commit();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }
}