package kw.gov.paci.PACIMobileID.home.views.fragments.Authentication;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.baseclasses.BaseTokenServiceCallBack;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.MyIDCompUnlockPinModel.Data;
import kw.gov.paci.PACIMobileID.home.models.MyIDCompUnlockPinModel.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDCompUnlockPinModel.MyIDCompleteUnlockModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddManageScreen;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.fxn.stash.Stash;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/15/2018.
 */
public class AddPinSuccessfulScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public LinearLayout mNoRequestView;
    public CustomTextView mFullNameEN, mCivilIDNum;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mCivilIDStr, mFullNameStr;
    public CustomButton mOKButton;
    public static String signStatus;
    public CustomTextView mSignStatusDisplayTv;
    public RetroApiInterface apiService;
    public SharedPreferences preferencese_splash;
    public SharedPreferences.Editor editor_splash;
    public String mDeviceIdValue, mTransID;
    //private ProgressDialog pDialog;
    public ImageView mProfilePic;
    public String mProfilePhoto;
    public Bitmap decodedByte;
    private Resources mResources;
    String language;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editors;
    public String mFullNameAr;
    public String mAddPinSucsScreenStatus = "mAddPinSucsScreenStatus";
    String authorzation_tokentype, mSerialNumber, CivilNo;
    private ArrayList<AddActivityLogModel> addActivityLogModelsArrayList;
    public static final String ACTIVITY_LOGS_PIN_SUCCESS = "ACTIVITY_LOGS_PIN_SUCCESS";
    public static final String ACTIVITY_LOGS_PIN_FAIL = "ACTIVITY_LOGS_PIN_FAIL";
    public Boolean mNotifiStatusBoolean = false;
    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";

    public AddPinSuccessfulScreen() {
        // Required empty public constructor
    }

    public static AddPinSuccessfulScreen newInstance(String msg) {
        AddPinSuccessfulScreen fragment = new AddPinSuccessfulScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            signStatus = msg;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_home_authe_pin_successful, container, false);
        mSignStatusDisplayTv = profileBody.findViewById(R.id.header_enter_pin_edtxt);
        mProfilePic = profileBody.findViewById(R.id.profile_pic_imgv);
        if (signStatus != null && signStatus.equals("failStatus")) {
            mSignStatusDisplayTv.setText(R.string.You_have_Rejected_the_document_Successfully);
        }
        if (signStatus != null && signStatus.equals("successStatusManageScreen")) {
            mSignStatusDisplayTv.setText(R.string.Your_Pin_Unlocked_Successfully);
        }
        if (signStatus != null && signStatus.equals("successStatusDigitalSign")) {
            mSignStatusDisplayTv.setText(R.string.You_Have_Signed_the_document_Successfully);
        }
        if (signStatus != null && signStatus.equals("failStatusDigitalSign")) {
            mSignStatusDisplayTv.setText(R.string.You_have_Rejected_the_document_Successfully);
        }
        mOKButton = profileBody.findViewById(R.id.ok_successful_btn);
        mFullNameEN = profileBody.findViewById(R.id.full_en_name);
        mCivilIDNum = profileBody.findViewById(R.id.civiild_id_display);
        mNoRequestView = profileBody.findViewById(R.id.no_request_ll);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        mOKButton.setOnClickListener(this);
        mResources = getResources();
        mSetProfilePicImage();
        setImageResource();
        getDataFromServer();
        mContentView.setVisibility(View.VISIBLE);
        mFullFrame.addView(profileBody);
    }

    @SuppressLint("CommitPrefEdits")
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mCivilIDStr = (mCivilIDStr);
            mFullNameStr = preferences.getString(("FullNameEn"), "default");
            mFullNameStr = (mFullNameStr);
            mFullNameAr = preferences.getString(("FullNameAr"), "default");
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Civil_ID_No), mCivilIDStr);
            mCivilIDNum.setText(text);
            mFullNameEN.setText(mFullNameStr);
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                mFullNameEN.setText(mFullNameAr);
            } else {
                mFullNameEN.setText(mFullNameStr);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok_successful_btn:
                mNotifiStatusBoolean = preferences.getBoolean(("mNotifiStatusBoolean"), false);
                if (signStatus != null && signStatus.equals("successStatusManageScreen")) {
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                        preferencese_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
                        editor_splash = preferencese_splash.edit();
                        editor_splash.apply();

                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        mDeviceIdValue = preferences.getString(("mCarrierID"), ("default"));
                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        mTransID = preferences.getString(("mManageScreenTransID"), ("default"));
                        if (Utilities.isNetworkAvailable(getActivity())) {
                            // mCompletePINUnLock(mDeviceIdValue, mTransID);
                           // tokenIdentity = new TokenIdentity(getActivity());
                            //tokenIdentity.mGetAccessTokenRequest(mAddPinSucsScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            if (Utilities.isNetworkAvailable(getActivity())) {
                                mCompletePINUnLock(mDeviceIdValue, mTransID);
                            }
                        }
                    }
                }
                if (mNotifiStatusBoolean) {
                    Intent intent = new Intent(getActivity(), AddHomeActivity.class);
                    startActivity(intent);
                    Objects.requireNonNull(getActivity()).finish();
                } else {
                    // setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                    Intent intent = new Intent(getActivity(), AddHomeActivity.class);
                    startActivity(intent);
                    Objects.requireNonNull(getActivity()).finish();
                }
                break;
        }
    }

    public void setImageResource() {

    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void mCompletePINUnLock(String mDeviceIdValue, String mTransID) {
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mSerialNumber, CivilNo);
        Data data_object = new Data(mDeviceIdValue, mTransID);
        MyIDCompleteUnlockModel myIDCompleteUnlockModel = new MyIDCompleteUnlockModel(identity, data_object);
        Call<JsonObject> call = apiService.mCompletePINUnlockPost(authorzation_tokentype, myIDCompleteUnlockModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    if (response.code() == 200) {
                        //unlock pin security
                        if (response.isSuccessful()) {
                            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                            editor = preferences.edit();
                            editor.apply();
                            editor.putString("mCurrentLoginFailCount", "default");
                            editor.apply();
                            //
                          /*  Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.pin_unlock_txt), getString(R.string.complete_pin_unlock), getString(R.string.pin_unlock_txt), getString(R.string.successfully_txt), dateToStr));
                            saveToSharedPreference(addActivityLogModelsArrayList);*/
                           /* setFragment(R.id.root_content, AddManageScreen.newInstance(),
                                    AddManageScreen.class.getSimpleName());*/
                            Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_manage), getString(R.string.activity_log_title_unlock), "", getString(R.string.Your_Pin_Unlocked_Successfully_activity_history), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                            /*Intent intent = new Intent(getActivity(), AddHomeActivity.class);
                            startActivity(intent);
                            getActivity().finish();*/
                        } else {
                            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                            editor = preferences.edit();
                            editor.apply();
                            editor.putString("mCurrentLoginFailCount", "default");
                            editor.apply();
                           /* Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.pin_unlock_txt), getString(R.string.complete_pin_unlock), getString(R.string.pin_unlock_txt), getString(R.string.failed_txt), dateToStr));
                            saveToSharedPreferenceFail(addActivityLogModelsArrayList);*/
                           /* setFragment(R.id.root_content, AddManageScreen.newInstance(),
                                    AddManageScreen.class.getSimpleName());*/
                            Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_manage), getString(R.string.activity_log_title_unlock), "", getString(R.string.Your_Pin_Unlocked_Successfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                           /* Intent intent = new Intent(getActivity(), AddHomeActivity.class);
                            startActivity(intent);
                            getActivity().finish();*/
                            Toast.makeText(getActivity(), String.valueOf(response.errorBody()), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), String.valueOf(response.errorBody()), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                    setFragment(R.id.root_content, AddManageScreen.newInstance(), AddManageScreen.class.getSimpleName());
                }
            }
        });
    }

    private void mSetProfilePicImage() {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mProfilePhoto = preferences.getString(("Photo"), "default");
        mProfilePhoto = (mProfilePhoto);
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    //mSubHeaderImage.setImageBitmap(decodedByte);
                    mRoundedImageConvert();
                } else {
                    mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 25;
        int shadowWidth = 10;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mProfilePic.setImageDrawable(roundedBitmapDrawable);
    }

    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }

   /* private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS_PIN_SUCCESS, addActivityLogModelsArrayList);
    }

    private void saveToSharedPreferenceFail(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS_PIN_FAIL, addActivityLogModelsArrayList);
    }*/
}