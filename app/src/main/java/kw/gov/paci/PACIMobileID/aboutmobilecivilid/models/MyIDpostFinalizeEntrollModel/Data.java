
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("TransactionId")
    @Expose
    private String transactionId;
    @SerializedName("MobileCode")
    @Expose
    private String mobileCode;
    @SerializedName("DeviceDetails")
    @Expose
    private DeviceDetails deviceDetails;

    public Data(String mTransactionId, String mobileCode, DeviceDetails deviceDetails) {
        this.transactionId = mTransactionId;
        this.mobileCode = mobileCode;
        this.deviceDetails = deviceDetails;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    public DeviceDetails getDeviceDetails() {
        return deviceDetails;
    }

    public void setDeviceDetails(DeviceDetails deviceDetails) {
        this.deviceDetails = deviceDetails;
    }


}
