package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.JsonObject;
import com.intercede.IProvisionCallback;
import com.intercede.IReturnUserPin;
import com.intercede.IdentityWallet;
import com.intercede.PinPolicy;
import com.intercede.ProvisionLink;
import com.intercede.ProvisionLinkException;
import com.intercede.myIDSecurityLibrary.InvalidContextException;
import com.intercede.myIDSecurityLibrary.MyIDSecurityLibrary;
import com.intercede.myIDSecurityLibrary.NotRunningOnBackgroundThreadException;
import com.intercede.myIDSecurityLibrary.UnlicencedException;
import com.scottyab.rootbeer.RootBeer;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel.Certificate;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel.Data;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel.DeviceDetails;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel.MyIDFinalEntrollModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.utils.CSRHelper;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class AddConfirmPin extends MobileCivilIDBaseFragment implements View.OnClickListener, IProvisionCallback {
    public static final String IS_EDIT = "type";
    public static String IS_CHANGE_PASSWORD = "isChangePassword";
    private OnAboutMobileCivilIDInteractionListener mListener;
    public CustomTextView mheader_confirm_txt;
    public EditText mEnterPinED, mConfirmPinED;
    public RetroApiInterface apiService;
    public SharedPreferences preferences, preferences_splash;
    public SharedPreferences.Editor editor;
    private String mEnterpin, mConfirmpin;
    public String mTransactionId;
    public String MobileCode, CivilNo, DeviceUniqueId, NotificationDeviceUniqueId, DeviceFamily, DeviceOS, DeviceModel, PlatformVersion, IdentityType;
    private String data = null;
    SharedPreferences.Editor editors, editor_spalsh;
    public static String mQrData;
    public CustomTextView mItIsHighly, mPinMustText;
    public LinearLayout mEnterPinRootView;
    public String mSerialNumber, mKeystoreType = "Android PKCS";
    public String CollectionUrl;
    LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver receiver;
    public IdentityWallet mWallet;
    MyIDSecurityLibrary myIDSecurityLibrary;
    public LinearLayout mProgressbarll;
    static public String PROVISION_SUCCESS = "provision_success";
    static public String PROVISION_FAILED = "provision_failed";
    public static int SERVER_TIME_OUT = 100;
    String carrier, mConfirmPinEDString;
    byte[] mMyIDCertificate, mMyIDSerialNumber, mMyIDThumprint;
    String mMyIDCertBase64, mMyIDDisplayName, mMyIDIssuer, mMyIDSubject, mMyIDGetCertificate;
    Date mMyIDValidFrom, mMyIDValidTo;
    public Boolean mIsJailBreak = false;
    String authorzation_tokentype_str;

    public AddConfirmPin() {
        // Required empty public constructor
    }

    public static AddConfirmPin newInstance(String msg) {
        AddConfirmPin fragment = new AddConfirmPin();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @SuppressLint("CommitPrefEdits")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        editors = Objects.requireNonNull(getActivity()).getSharedPreferences("SHARED_PREF", Context.MODE_PRIVATE).edit();
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        preferences_splash = getActivity().getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor_spalsh = preferences_splash.edit();
        editor = preferences.edit();
        editor.apply();
        editor_spalsh.apply();
        //getDetails
        mTransactionId = preferences.getString("TransactionId", "default");
        NotificationDeviceUniqueId = preferences.getString("TokenID", "default");
        if (NotificationDeviceUniqueId.equals("default")) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
                @Override
                public void onSuccess(InstanceIdResult instanceIdResult) {
                    String newToken = instanceIdResult.getToken();
                    editor.putString("TokenID", newToken);
                    editor.apply();
                    //Log.e("newToken", newToken);
                }
            });
        }
        MobileCode = preferences.getString("MobileCode", "default");
        CivilNo = preferences.getString("CivilNo", "default");
        DeviceFamily = "android";
        DeviceOS = "2";
        DeviceModel = android.os.Build.MODEL;
        try {
            RootBeer rootBeer = new RootBeer(getActivity());
            if (rootBeer.isRooted()) {
                //we found indication of root
                mIsJailBreak = rootBeer.isRooted();
                Toast.makeText(getActivity(), "The app cannot run on this device", Toast.LENGTH_SHORT).show();
                mListener.goToGetMobileIDScreen();
            } else {
                mIsJailBreak = rootBeer.isRooted();
                //we didn't find indication of root
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        PlatformVersion = Build.VERSION.RELEASE;
        IdentityType = "1";
        localBroadcastManager = LocalBroadcastManager.getInstance(getActivity());
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAboutMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.fragment_confirm_pin_header, container, false);
        mheader_confirm_txt = header.findViewById(R.id.header_confirm_txt);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_confirm_pin_body, container, false);
        mEnterPinED = profileBody.findViewById(R.id.et_new_pin);
        mConfirmPinED = profileBody.findViewById(R.id.et_confrm_pin);
        mItIsHighly = profileBody.findViewById(R.id.it_is_highly_txt);
        mPinMustText = profileBody.findViewById(R.id.pin_must_txt);
        mEnterPinRootView = profileBody.findViewById(R.id.pin_new_layout_root);
        mProgressbarll = profileBody.findViewById(R.id.progress_bar_ll);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mEnterpin = mEnterPinED.getText().toString().trim();
        mConfirmpin = mConfirmPinED.getText().toString().trim();
        mFullFrame.addView(profileBody);
    }

    private void mRestEditTextValues() {
        mEnterPinED.setText("");
        mConfirmPinED.setText("");
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mRootView.setVisibility(View.VISIBLE);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
        mCancelButton.setVisibility(View.VISIBLE);
        mCancelButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.conformbtn));
        mCancelButton.setText(R.string.Confirm);
        mCancelButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cancel_btn:
                mEditTextConfirmPinValidation();
                break;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void getCarrierID() {
        try {
            mWallet = new IdentityWallet(getActivity());
            carrier = mWallet.getCarrierId();
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            editor.putString("mCarrierID", carrier);
            editor.apply();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Error:" + e, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onStop() {
        localBroadcastManager.unregisterReceiver(receiver);
        super.onStop();
        if (myIDSecurityLibrary != null) {
            myIDSecurityLibrary.onStop();
            myIDSecurityLibrary = null;
        }
    }

    private void mEditTextConfirmPinValidation() {
        try {
            mEnterpin = mEnterPinED.getText().toString().trim();
            mConfirmpin = mConfirmPinED.getText().toString().trim();
            if (mEnterpin.isEmpty()) {
                Toast.makeText(getActivity(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                mRestEditTextValues();
                mEnterPinED.requestFocus();
            } else if (mConfirmpin.isEmpty()) {
                Toast.makeText(getActivity(), R.string.Please_Confirm_your_PIN, Toast.LENGTH_SHORT).show();
                mRestEditTextValues();
                mEnterPinED.requestFocus();
            } else if (mConfirmPinED.getText().toString().length() <= 3 || mEnterPinED.getText().toString().length() <= 3) {
                Toast.makeText(getActivity(), R.string.PIN_Must_be_between_4_and_12_characters, Toast.LENGTH_SHORT).show();
                mRestEditTextValues();
                mEnterPinED.requestFocus();
            } else if (Pattern.matches("^(0123|1234|2345|3456|4567|5678|6789|3210|4321|5432|6543|7654|8765|9876|0000|1111|2222|3333|4444|5555|6666|7777|8888|9999)$", mConfirmPinED.getText().toString().trim())) {
                Toast.makeText(getContext(), R.string.s_1_11_consecutive_passcode_alert_txt, Toast.LENGTH_SHORT).show();
            } else {
                if (mEnterPinED.getText().toString().length() >= 4 && mConfirmPinED.getText().toString().length() >= 4 && mEnterPinED.getText().toString().equals(mConfirmPinED.getText().toString())) {
                    //Toast.makeText(getActivity(), "Equal", Toast.LENGTH_SHORT).show();
                    if (mEnterpin.equals(mConfirmPinED.getText().toString())) {
                        editor.putString("mConfirmPinED", mConfirmPinED.getText().toString());
                        editor.apply(); // Or commit if targeting old devices
                        if (Utilities.isNetworkAvailable(getActivity())) {
                            mProgressbarll.setVisibility(View.VISIBLE);
                            mEnterPinED.setEnabled(false);
                            mConfirmPinED.setEnabled(false);
                            mMyIDCertificateStart();
                        }
                    } else {
                        mRestEditTextValues();
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                    mEnterPinED.setText("");
                    mConfirmPinED.setText("");
                    mEnterPinED.requestFocus();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mMyIDCertificateStart() {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    CollectionUrl = preferences.getString("CollectionUrl", "default");
                    ProvisionLink provLink = new ProvisionLink(CollectionUrl);
                    try {
                        initialise(getActivity());
                        startProvision(provLink);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (ProvisionLinkException e) {
                    e.getLocalizedMessage();
                    //Log.d("provLink", e.getLocalizedMessage());
                }
            }
        }, SERVER_TIME_OUT);

    }

    private void mAddFinalEntroll(FragmentActivity activity) {
        try {
            authorzation_tokentype_str = preferences.getString("authorzation_tokentype", "default");
            mMyIDGetCertificate = preferences.getString("mMyIDCertBase64", "default");
            Certificate certificate = new Certificate(mMyIDGetCertificate);
            mSerialNumber = preferences.getString(("mCarrierID"), "default");
            DeviceUniqueId = preferences.getString("mCarrierID", "default");
            NotificationDeviceUniqueId = preferences.getString("TokenID", "default");
            CivilNo = preferences.getString("CivilNo", "default");
            mTransactionId = preferences.getString("TransactionId", "default");
            MobileCode = preferences.getString("MobileCode", "default");
            DeviceDetails deviceDetails = new DeviceDetails(DeviceUniqueId, NotificationDeviceUniqueId, DeviceFamily, 2, DeviceModel, mIsJailBreak, PlatformVersion, IdentityType, certificate, mSerialNumber, mKeystoreType);
            Identity identity = new Identity(mSerialNumber, CivilNo);
            Data data = new Data(mTransactionId, MobileCode, deviceDetails);
            MyIDFinalEntrollModel postFinalizedModel = new MyIDFinalEntrollModel(identity, data);
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            Call<JsonObject> call = apiService.postFinalizeEntroll(authorzation_tokentype_str, postFinalizedModel);
            //Log.d("postFinalizedModel", "" + postFinalizedModel);
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                    try {
                        //Log.d("resn", "" + response);
                        mProgressbarll.setVisibility(View.GONE);
                        if (response.isSuccessful()) {
                            editor.putString("AddConfirmPinScreenSuccess", "AddConfirmPinScreenSuccess");
                            editor.apply();
                            mListener.goToAddPinCollectedSuccessfully();
                        } else {
                            editor.clear();
                            editor.apply();
                            mListener.goToGetMobileIDScreen();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    editor.clear();
                    editor.apply();
                    mListener.goToGetMobileIDScreen();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            editor.putString("CivilNo", "");
            editor.apply();
        }
    }

    public void setImageResource() {
    }

    @Override
    public void onProvisionIdentitySuccess() {
        localBroadcastManager.sendBroadcast(new Intent(PROVISION_SUCCESS));
        getCeriticate();
    }

    @Override
    public void onProvisionIdentityFail(Exception e) {
        Intent failedIntent = new Intent(PROVISION_FAILED);
        failedIntent.putExtra("description", HeadlessErrorHandler.extractErrorResult(e));
        localBroadcastManager.sendBroadcast(failedIntent);
        Toast.makeText(getActivity(), "" + HeadlessErrorHandler.extractErrorResult(e), Toast.LENGTH_SHORT).show();
        editor.clear();
        editor.apply();
        mListener.goToGetMobileIDScreen();
    }

    @Override
    public void onProvisionIdentityRequireSetUserPin(PinPolicy pinPolicy, IReturnUserPin callback) {
        preferences = Objects.requireNonNull(Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE));
        editor = preferences.edit();
        editor.apply();
        mConfirmPinEDString = preferences.getString("mConfirmPinED", "default");
        callback.setResult(mConfirmPinEDString);
    }

    @Override
    public void onProvisionIdentityPromptForUserPin(int i, IReturnUserPin iReturnUserPin) {
        //Log.d("onPrPromptUserPin", "callback" + iReturnUserPin);
    }

    private void getCeriticate() {
        try {
            new MyAsyncTask().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void initialise(Context mainContext) {
        try {
            mWallet = new IdentityWallet(mainContext);
        } catch (UnlicencedException e) {
            e.printStackTrace();
        }
    }

    public void startProvision(ProvisionLink provLink) {

        try {
            mWallet.provisionIdentity(provLink, this);
        } catch (Exception e) {
            onProvisionIdentityFail(e);
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class MyAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(getActivity()));
                myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
                myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
                myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
                int num_cer = myIDSecurityLibrary.numberOfCertificates();
                myIDSecurityLibrary.enumerateCertificates();
                for (int i = 0; i < Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()).length; i++) {
                    mMyIDSubject = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].subject;
                    mMyIDDisplayName = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].displayName;
                    mMyIDIssuer = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].issuer;
                    mMyIDSerialNumber = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].serialNumber;
                    mMyIDCertificate = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].certificate;
                    mMyIDThumprint = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].thumbprint;
                    mMyIDValidFrom = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].validFrom;
                    mMyIDValidTo = (Objects.requireNonNull(myIDSecurityLibrary.enumerateCertificates()))[0].validTo;
                }
                try {
                    // byte[] bytes = mMyIDSerialNumber;
                    //mMyIDSerialNumberString = new String(bytes, "UTF-8"); // for UTF-8 encoding
                    getCertificateDetails(mMyIDCertificate);
                    mMyIDCertBase64 = CSRHelper.encryptBASE64(mMyIDCertificate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                editor.putString("mMyIDCertBase64", mMyIDCertBase64);
                editor.putString("mMyIDDisplayName", mMyIDDisplayName);
                editor.putString("mIssuerDN", mMyIDIssuer);
                editor.putString("mSubjectDN", mMyIDSubject);
                editor.putString("mvaildFrom", mMyIDValidFrom.toString());
                editor.putString("mvaildTo", mMyIDValidTo.toString());
                //  editor.putString("mMyIDSerialNumber", mMyIDSerialNumberString);
                editor.apply();
                getCarrierID();
                mAddFinalEntroll(getActivity());
            } catch (InvalidContextException | PackageManager.NameNotFoundException | NotRunningOnBackgroundThreadException e) {
                e.printStackTrace();
                editor.putString("CivilNo", "");
                editor.apply();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }

    private void getCertificateDetails(byte[] mMyIDCertificate) {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(mMyIDCertificate);
        try {
            java.security.cert.Certificate cert = null;
            try {
                cert = CertificateFactory.getInstance("X.509", "BC").generateCertificate(inputStream);
                X509Certificate c = (X509Certificate) cert;
                //getCeritificate Details
                mSerialNumber = c.getSerialNumber().toString();
                editor.putString("mMyIDSerialNumber", mSerialNumber);
                editor.apply();
                /*Start*/
            } catch (NoSuchProviderException e) {
                e.printStackTrace();
                editor.putString("CivilNo", "");
                editor.apply();
            }
        } catch (CertificateException e) {
            e.printStackTrace();
            editor.putString("CivilNo", "");
            editor.apply();
        }
    }
}




