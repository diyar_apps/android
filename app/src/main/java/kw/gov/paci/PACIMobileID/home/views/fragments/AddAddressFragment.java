package kw.gov.paci.PACIMobileID.home.views.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.BottomNavigationViewHelper;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomMenuItemTypefaceSpan;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/8/2018.
 */
public class AddAddressFragment extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    private BottomNavigationView navigation;

    public OnAddHomeInteractionListener mListener;
    protected CustomButton mOkBtn;
    public CustomTextView mAreaTv, mBlockTv, mStreetTv, mUnitTypeTv, mUnitNoTv, mFloorTv, mPaciNumTv;
    public String mAreaStr, mBlockStr, mStreetStr, mUnitTypeStr, mUnitNoStr, mFloorStr, mPaciNumStr;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mCountryNameStr;
    public LinearLayout mRootBgImage;
    public String mNotificationNumberDisplay;

    public AddAddressFragment() {
        // Required empty public constructor
    }

    public static AddAddressFragment newInstance() {
        AddAddressFragment fragment = new AddAddressFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_address_home_body_fragment, container, false);
        mRootBgImage = profileBody.findViewById(R.id.add_address_home_bg_ll);
        mAreaTv = profileBody.findViewById(R.id.area_txt);
        mBlockTv = profileBody.findViewById(R.id.block_no);
        mStreetTv = profileBody.findViewById(R.id.street_txt);
        mUnitTypeTv = profileBody.findViewById(R.id.unit_type_txt);
        mUnitNoTv = profileBody.findViewById(R.id.unit_no_txt);
        mFloorTv = profileBody.findViewById(R.id.floor_txt);
        mPaciNumTv = profileBody.findViewById(R.id.paci_num_txt);
        getDataFromServer();
        navigation = profileBody.findViewById(R.id.navigation);
        mOkBtn = profileBody.findViewById(R.id.ok_btn);
        mOkBtn.setOnClickListener(this);
        //customFontSetMenuItems();
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        /*BottomNavigationViewHelper.removeShiftMode(navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        */
        mFullFrame.addView(profileBody);
    }

    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
            if (mNotificationNumberDisplay.equals("0")) {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
            } else {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
            }
            mAreaStr = preferences.getString(("Area"), "default");
            mBlockStr = preferences.getString(("BlockNumber"), "default");
            mFloorStr = preferences.getString(("FloorNumber"), "default");
            mStreetStr = preferences.getString(("StreetName"), "default");
            mUnitTypeStr = preferences.getString(("UnitType"), "default");
            mUnitNoStr = preferences.getString(("UnitNumber"), "default");
            mPaciNumStr = preferences.getString(("PaciBuildingNumber"), "default");
            mCountryNameStr = preferences.getString(("NationalityEn"), "default");
            if (mCountryNameStr.equals("KWT") || mCountryNameStr.equals("Kuwaiti") || mCountryNameStr.equals("Kuwait")) {
                mRootBgImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.screenbg_blue));
            } else {
                mRootBgImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.screenbg_liteorange));
            }
            if (mFloorStr.matches("\\s*") || mFloorStr.equals("null")) {
                mFloorStr = "-";
            }
            if (mStreetStr.matches("\\s*") || mStreetStr.equals("null")) {
                mStreetStr = "-";
            }
            if (mUnitNoStr.matches("\\s*") || mUnitNoStr.equals("null")) {
                mUnitNoStr = "-";
            }
            if (mAreaStr.matches("\\s*") || mAreaStr.equals("null")) {
                mAreaStr = "-";
            }
            if (mBlockStr.matches("\\s*") || mBlockStr.equals("null")) {
                mBlockStr = "-";
            }
            if (mUnitTypeStr.matches("\\s*") || mUnitTypeStr.equals("null")) {
                mUnitTypeStr = "-";
            }
            //SetText
            mAreaTv.setText(mAreaStr);
            mBlockTv.setText(mBlockStr);
            mFloorTv.setText(mFloorStr);
            mStreetTv.setText(mStreetStr);
            mUnitTypeTv.setText(mUnitTypeStr);
            mUnitNoTv.setText(mUnitNoStr);
            mPaciNumTv.setText(mPaciNumStr);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

 /*   private void customFontSetMenuItems() {
        Menu m = navigation.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }*/

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok_btn:
                //getActivity().finish();
                setFragment(R.id.root_content, AddHomeFragment.newInstance(), AddHomeFragment.class.getSimpleName());
                break;
        }
    }

    public void setImageResource() {

    }

   /* private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.ic_mb_cv_id_bottombar:
                    return true;
                case R.id.ic_authu_bottombar:
                    return true;
                case R.id.ic_disgn_sign_bottombar:
                    return true;
                case R.id.ic_manage_bottombar:
                    return true;

            }
            return false;
        }
    };*/

   /* private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/gotham_book.otf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomMenuItemTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }*/

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.commit();
    }
}