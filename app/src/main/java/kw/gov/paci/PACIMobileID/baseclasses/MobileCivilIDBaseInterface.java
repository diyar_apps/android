package kw.gov.paci.PACIMobileID.baseclasses;

import android.view.LayoutInflater;
import android.view.ViewGroup;

public interface MobileCivilIDBaseInterface {

    void setHeader(ViewGroup container, LayoutInflater inflater);

    void setBody(ViewGroup container, LayoutInflater inflater);

    void setFooter(ViewGroup container);
}
