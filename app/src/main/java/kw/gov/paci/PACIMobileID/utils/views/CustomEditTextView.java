package kw.gov.paci.PACIMobileID.utils.views;

/**
 * Created by VAMANPALLI SUNIL KUMAR on 09/08/2018.
 */
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.style.StyleSpan;
import android.util.AttributeSet;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.utils.Utilities;

/**
 * Base class for EditTextViews that would use custom fonts.
 */
public class CustomEditTextView extends android.support.v7.widget.AppCompatEditText {

    private static Typeface tfLatoSemiBold = null;

    public CustomEditTextView(final Context context) {
        super(context);
        init(context);
    }

    public CustomEditTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
        //init(context);
        setCustomFont(context, attrs);
    }

/*	public CustomEditTextView(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
		init(context);
		setCustomFont(context, attrs);
	}*/

    private final void init(final Context context) {
        if (isInEditMode()) {
            return;
        }

        final AssetManager assets = context.getAssets();
        tfLatoSemiBold = Typeface.createFromAsset(assets, getContext().getString(R.string.change_the_font));
        setTypeface(tfLatoSemiBold);
        setSpannableFactory(new TTFFactory());
    }

    private void setCustomFont(Context ctx, AttributeSet attrs) {

        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.DIYARTextView);
        String font = a.getString(R.styleable.DIYARTextView_default_font);

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), font);
        } catch (Exception e) {
            Utilities.sendException(e);
        }

        setTypeface(tf);

        a.recycle();
    }

    public void setCustomFont(int fontNameId) {
        String firstTypeFace = getContext().getResources().getString(fontNameId);
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(getContext().getAssets(), firstTypeFace);
            this.setTypeface(tf);
        } catch (Exception e) {
            Utilities.sendException(e);
        }
    }

    /**
     * @author roman.kliotzkin
     *         <p>
     *         Custom StyleSpan class that replaces the standard font with our variations of the Rockwell font
     */
    private static class RockwellHTMLSpan extends StyleSpan {
        public RockwellHTMLSpan(int style) {
            super(style);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            applyCustomTypeFace(ds);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            applyCustomTypeFace(paint);
        }

        private void applyCustomTypeFace(Paint paint) {
            paint.setTypeface(tfLatoSemiBold);
        }
    }

    /**
     * Sets the HTML text in this view by parsing the spannable object returned by Html.fromHtml
     * and replacing StyleSpan objects with our own RockwellHTMLSpan
     * <p>
     * Note the special case of bold-italic : currently, the way this is indicated is by placing
     * 2 spans in one place, and we have to check for that and place a BOLD_ITALIC style instead
     */
    private static class TTFFactory extends Spannable.Factory {
        @Override
        public Spannable newSpannable(final CharSequence source) {
            SpannableStringBuilder span = (SpannableStringBuilder) Html.fromHtml(String.valueOf(source));

            for (StyleSpan currentStyleSpan : span.getSpans(0, span.length(), StyleSpan.class)) {
                StyleSpan newSpan = null;
                boolean isBoldItalic = false;

                // special hacky case for bold_italic
                for (StyleSpan internalSpan : span.getSpans(span.getSpanStart(currentStyleSpan), span.getSpanEnd(currentStyleSpan), StyleSpan.class)) {
                    if (internalSpan.getStyle() != currentStyleSpan.getStyle()) {
                        newSpan = new RockwellHTMLSpan(Typeface.BOLD_ITALIC);

                        isBoldItalic = true;
                        break;
                    }
                }

                if (!isBoldItalic && ((currentStyleSpan.getStyle() & Typeface.BOLD) | (currentStyleSpan.getStyle() & Typeface.ITALIC)) != 0) {
                    newSpan = new RockwellHTMLSpan(currentStyleSpan.getStyle());
                }

                span.setSpan(newSpan, span.getSpanStart(currentStyleSpan), span.getSpanEnd(currentStyleSpan), span.getSpanFlags(currentStyleSpan));
                span.removeSpan(currentStyleSpan);
            }

            return span;
        }
    }
}
