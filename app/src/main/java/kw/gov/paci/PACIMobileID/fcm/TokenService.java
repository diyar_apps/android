package kw.gov.paci.PACIMobileID.fcm;

import android.app.Service;
import android.content.SharedPreferences;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 1/5/2019.
 */
public class TokenService extends FirebaseInstanceIdService {
    public SharedPreferences preferences_pls_wait;
    public SharedPreferences.Editor editor_pls_wait;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        //Log.w("notification", refreshedToken);
        preferences_pls_wait = Objects.requireNonNull(this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor_pls_wait = preferences_pls_wait.edit();
        editor_pls_wait.apply();
        editor_pls_wait.putString("TokenID", refreshedToken);
        editor_pls_wait.apply();
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
    }
}
