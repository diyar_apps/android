package kw.gov.paci.PACIMobileID.baseclasses;

import retrofit2.Response;

/**
 * Created by SUNIL KUMAR V on 1/3/2019.
 */
public interface BaseTokenServiceCallBack {

    void successful(Response response,String statusScreenMethod);

    void fail(Throwable t);

    void tokenexpiry();
}
