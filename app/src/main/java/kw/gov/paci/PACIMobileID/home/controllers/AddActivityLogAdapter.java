package kw.gov.paci.PACIMobileID.home.controllers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.ArrayList;

/**
 * Created by SUNIL KUMAR V on 1/10/2019.
 */
public class AddActivityLogAdapter extends RecyclerView.Adapter<AddActivityLogAdapter.CustomViewHolder> {
    private ArrayList<AddActivityLogModel> mDsigns;
    public Context context;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    String language;
    public SharedPreferences preferences_lang;
    public SharedPreferences.Editor editor_lang;
    public static final String Locale_KeyValue = "Saved Locale";
    private static final String Locale_Preference = "Locale Preference";
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editors;

    public AddActivityLogAdapter(ArrayList<AddActivityLogModel> mDsigns, Context context) {
        this.mDsigns = mDsigns;
        this.context = context;
    }

    @Override
    public AddActivityLogAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_activity_logs_display_items, parent, false);

        return new AddActivityLogAdapter.CustomViewHolder(itemView);
    }

    @SuppressLint({"SetTextI18n", "CommitPrefEdits"})
    @Override
    public void onBindViewHolder(final AddActivityLogAdapter.CustomViewHolder holder, final int position) {
      /*  final AddActivityLogModel msign = mDsigns.get(position);
        sharedPreferences = this.context.getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
        editors = sharedPreferences.edit();
        language = sharedPreferences.getString(Locale_KeyValue, "ar");
        if (language.equals("ar")) {
            if (msign.getMTitle().equals("Authenticate")) {
                holder.mTitle.setText("مصادقة");
            }
            if (msign.getMTitltModule().equals("Authentication - Success")) {
                holder.mTitltModule.setText(" طلبات المصادقة - Success");
            }
            if (msign.getMTitleModuleStatus().equals("Authentication - Success")) {
                holder.mTitltModule.setText(" طلبات المصادقة - Success");
            }
            if (msign.getMPinlockStatus().equals("You have Authenticated Successfully")) {
                holder.mTitltModule.setText("تم المصادقة بنجاح");
            }
            holder.mDateTime.setText(msign.getMStatusBg());
        } else {
            holder.mTitle.setText(msign.getMTitle());
            holder.mTitltModule.setText(msign.getMTitltModule());
            holder.mTitleModuleStatus.setText(msign.getMTitleModuleStatus());
            holder.mPinlockStatus.setText(msign.getMPinlockStatus());
            holder.mDateTime.setText(msign.getMStatusBg());
        }

        if (msign.getMTitltModule().equals("Enrolment - Success") || msign.getMTitltModule().equals("Authentication - Success") || msign.getMTitltModule().equals("Sign - Success")) {
            holder.mStatusBg.setBackgroundColor(Color.parseColor("#0A9813"));
        } else {
            // holder.mStatusBg.setBackgroundColor(Color.parseColor("#DC4444"));
            holder.mStatusBg.setBackgroundColor(Color.parseColor("#0A9813"));
        }
        if (msign.getMTitleModuleStatus().matches("\\s*")) {
            holder.mTitleModuleStatus.setVisibility(View.GONE);
        }
*/
        final AddActivityLogModel msign = mDsigns.get(position);
        holder.mTitle.setText(msign.getMTitle());
        holder.mTitltModule.setText(msign.getMTitltModule());
        holder.mTitleModuleStatus.setText(msign.getMTitleModuleStatus());
        holder.mPinlockStatus.setText(msign.getMPinlockStatus());
        holder.mDateTime.setText(msign.getMStatusBg());

        if (msign.getMTitltModule().equals("Enrolment - Success") || msign.getMTitltModule().equals("Authentication - Success") || msign.getMTitltModule().equals("Sign - Success")) {
            holder.mStatusBg.setBackgroundColor(Color.parseColor("#0A9813"));
        } else {
            // holder.mStatusBg.setBackgroundColor(Color.parseColor("#DC4444"));
            holder.mStatusBg.setBackgroundColor(Color.parseColor("#0A9813"));
        }
        if (msign.getMTitleModuleStatus().matches("\\s*")) {
            holder.mTitleModuleStatus.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        if (null != mDsigns) {
            return mDsigns.size();
        } else {
            return 0;
        }
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        CustomTextView mTitle, mTitltModule, mTitleModuleStatus, mPinlockStatus, mDateTime;
        ImageView mStatusBg;

        CustomViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.title_screen);
            mTitltModule = view.findViewById(R.id.title_module);
            mTitleModuleStatus = view.findViewById(R.id.title_module_status);
            mPinlockStatus = view.findViewById(R.id.pin_status);
            mStatusBg = view.findViewById(R.id.status_imageview_bg_color);
            mDateTime = view.findViewById(R.id.date_and_time);
        }
    }
}