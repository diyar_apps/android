package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import com.intercede.NetworkFailureException;
import com.intercede.PinLockedException;
import com.intercede.UnrecoverableErrorException;

/**
 * Created by V.Sunil Kumar on 25/12/2018.
 */

public class HeadlessErrorHandler {

    static public String extractErrorResult(final Exception exception) {
        String errMsg = exception.getLocalizedMessage();
        StringBuilder messageBuilder = new StringBuilder();
        messageBuilder.append(errMsg);
        if(exception instanceof NetworkFailureException) {
            NetworkFailureException ex = (NetworkFailureException)exception;
            if(!ex.mHttpStatus.isEmpty()) {
                messageBuilder.append("\nHTTP status: ");
                messageBuilder.append(ex.mHttpStatus);
            }
            if(!ex.mErrorCode.isEmpty()) {
                messageBuilder.append("\nDiagnostic code: ");
                messageBuilder.append(ex.mErrorCode);
            }
        } else if(exception instanceof PinLockedException) {
            PinLockedException ex = (PinLockedException)exception;
            if(!ex.mErrorCode.isEmpty()) {
                messageBuilder.append("\nDiagnostic code: ");
                messageBuilder.append(ex.mErrorCode);
            }
        } else if(exception instanceof UnrecoverableErrorException) {
            UnrecoverableErrorException ex = (UnrecoverableErrorException)exception;
            if(!ex.mErrorCode.isEmpty()) {
                messageBuilder.append("\nDiagnostic code: ");
                messageBuilder.append(ex.mErrorCode);
            }
        }
        return messageBuilder.toString();
    }
}
