
package kw.gov.paci.PACIMobileID.splash.models;

/**
 * Created by VAMANPALLI SUNIL KUMAR on 09/08/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("DeviceID")
    @Expose
    private String deviceID;
    @SerializedName("DeviceOS")
    @Expose
    private Integer deviceOS;
    @SerializedName("NotificationDeviceUniqueId")
    @Expose
    private String notificationDeviceUniqueId;
    @SerializedName("CivilNo")
    @Expose
    private String civilNo;
    @SerializedName("ValidFrom")
    @Expose
    private String validFrom;
    @SerializedName("ValidTo")
    @Expose
    private String validTo;
    @SerializedName("IdentityType")
    @Expose
    private Integer identityType;
    @SerializedName("Status")
    @Expose
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public Integer getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(Integer deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getNotificationDeviceUniqueId() {
        return notificationDeviceUniqueId;
    }

    public void setNotificationDeviceUniqueId(String notificationDeviceUniqueId) {
        this.notificationDeviceUniqueId = notificationDeviceUniqueId;
    }

    public String getCivilNo() {
        return civilNo;
    }

    public void setCivilNo(String civilNo) {
        this.civilNo = civilNo;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public void setValidTo(String validTo) {
        this.validTo = validTo;
    }

    public Integer getIdentityType() {
        return identityType;
    }

    public void setIdentityType(Integer identityType) {
        this.identityType = identityType;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
