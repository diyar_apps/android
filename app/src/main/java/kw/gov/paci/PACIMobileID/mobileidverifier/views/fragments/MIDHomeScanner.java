package kw.gov.paci.PACIMobileID.mobileidverifier.views.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.ScannerActivity;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.mobileidverifier.controllers.OnMobileIDVerifyInteractionListener;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Objects;

/**
 * Created by SUNIL KUMAR V on 1/28/2019.
 */
public class MIDHomeScanner extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public ImageView header_Mobile_img;
    public CustomTextView header_Mobile_txt, body_Mobile_txt;

    private OnMobileIDVerifyInteractionListener mListener;

    public MIDHomeScanner() {
        // Required empty public constructor
    }

    public static MIDHomeScanner newInstance() {
        MIDHomeScanner fragment = new MIDHomeScanner();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        enForcedLocale();
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMobileIDVerifyInteractionListener) {
            mListener = (OnMobileIDVerifyInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMobileIDVerifyInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.about_mobile_civilid_home_header, container, false);
        header_Mobile_img = header.findViewById(R.id.about_civil_header_img);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.about_mobile_civilid_home_body, container, false);
        header_Mobile_txt = profileBody.findViewById(R.id.header_about_civil_id_header_txt);
        body_Mobile_txt = profileBody.findViewById(R.id.about_civil_id_body_txt);
        setImageResource();
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        mContentView.setVisibility(View.GONE);
        try {
            mScannerOpenActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_btn:
                //mListener.goToPreRequisitesScreen();
                break;
        }
    }

    public void setImageResource() {
        header_Mobile_img.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.mobilecivilid));
        header_Mobile_img.setPadding(200, 0, 0, 0);
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }

    private void mScannerOpenActivity() {
        try {
            IntentIntegrator integrator = new IntentIntegrator(getActivity());
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setCameraId(0);  // Use a specific camera of the device
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(true);
            integrator.setOrientationLocked(false);
            integrator.setPrompt("Scan a Qr code");
            integrator.addExtra("MIDHomeScanner", "MIDHomeScannerID");
            integrator.setCaptureActivity(ScannerActivity.class);
            integrator.initiateScan();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}