package kw.gov.paci.PACIMobileID.mobileidverifier.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.mobileidverifier.controllers.OnMobileIDVerifyInteractionListener;
import kw.gov.paci.PACIMobileID.mobileidverifier.models.MyIDpostGetAuthIdentityData.Data;
import kw.gov.paci.PACIMobileID.mobileidverifier.models.MyIDpostGetAuthIdentityData.Identity;
import kw.gov.paci.PACIMobileID.mobileidverifier.models.MyIDpostGetAuthIdentityData.MyIDpostGetAuthIdentityDataModel;
import kw.gov.paci.PACIMobileID.utils.CustomDialogWindow;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.google.gson.JsonObject;

import java.util.Objects;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 1/28/2019.
 */
public class MobileIDVerifyDataDisplay extends MobileCivilIDBaseFragment implements View.OnClickListener {

    private OnMobileIDVerifyInteractionListener mListener;
    public static String mQrData;
    public RetroApiInterface apiService;
    String mPostGetAuthIdentityDataStatus = "mPostGetAuthIdentityDataStatus";
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    String authorzation_tokentype;
    public String mDeviceIdValue, mCivilNo;
    public ImageView mProfilePicImgview;
    public Bitmap decodedByte, decodedByteFlag;
    public ImageView mFlagImgv;
    public CustomTextView mNameEn, mCountryName, mNameAr, mDob, mExpDate, mArea, mBlock, mStreet, mUnitType, mUnitNo, mFloorNo, mPaciNo;
    public String mCivilID, mFullNameAr, mFullNameEn, mBirthDate, mGender, mEmailAddress, mMobileNumber, mNationalityEn, mNationalityAr, mNationalityFlag, mBloodGroup, mCardExpiryDate, mCivilIDExpiry, mMoiRef, mSponser, mGovernerate, mAreaStr, mPaciBuildingNumber, mBuildingType, mFloorNumber, mBuildingNumber, mBlockNumber, mUnitNumber, mStreetName, mUnitTypeStr;
    public static int SPLASH_TIME_OUT = 5000;
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editors;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    String language;

    public MobileIDVerifyDataDisplay() {
        // Required empty public constructor
    }

    public static MobileIDVerifyDataDisplay newInstance(String msg) {
        MobileIDVerifyDataDisplay fragment = new MobileIDVerifyDataDisplay();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        enForcedLocale();
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnMobileIDVerifyInteractionListener) {
            mListener = (OnMobileIDVerifyInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnMobileIDVerifyInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.about_mobile_civilid_home_header, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_mobile_id_verify_data_display, container, false);
        mProfilePicImgview = profileBody.findViewById(R.id.image_load_profile);
        mNameEn = profileBody.findViewById(R.id.person_name_en);
        mNameAr = profileBody.findViewById(R.id.person_name_ar);
        mFlagImgv = profileBody.findViewById(R.id.flag_country_imgv);
        mCountryName = profileBody.findViewById(R.id.country_name);
        mDob = profileBody.findViewById(R.id.dob_date_txt);
        mExpDate = profileBody.findViewById(R.id.exp_date_txt);
        mArea = profileBody.findViewById(R.id.area_dynamic_txt);
        mBlock = profileBody.findViewById(R.id.block_dynamic_txt);
        mStreet = profileBody.findViewById(R.id.street_dynamic_txt);
        mUnitType = profileBody.findViewById(R.id.unit_type_dynamic_txt);
        mUnitNo = profileBody.findViewById(R.id.unit_no_dynamic_txt);
        mFloorNo = profileBody.findViewById(R.id.floor_dynamic_txt);
        mPaciNo = profileBody.findViewById(R.id.paci_no_dynamic_txt);
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CustomDialogWindow customDialog = new CustomDialogWindow(getActivity());
                Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                customDialog.show();
            }
        }, SPLASH_TIME_OUT);*/
        CustomDialogWindow customDialog = new CustomDialogWindow(getActivity());
        Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        customDialog.show();
        setImageResource();
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        mContentView.setVisibility(View.VISIBLE);
        if (Utilities.isNetworkAvailable(getActivity())) {
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            if (Utilities.isNetworkAvailable(getActivity())) {
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                mDeviceIdValue = preferences.getString("mCarrierID", "default");
                mCivilNo = preferences.getString("CivilNo", "default");
                authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
                mPostGetAuthIdentityDataStatusMethod(authorzation_tokentype, mDeviceIdValue, mCivilNo);
            }
        }
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_btn:
                //mListener.goToPreRequisitesScreen();
                break;
        }
    }

    public void setImageResource() {
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }

    private void mPostGetAuthIdentityDataStatusMethod(String authorzationTokentype, String mDeviceIdValue, String mCivilNo) {
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Identity identity = new Identity(mDeviceIdValue, mCivilNo);
        Data data = new Data(mQrData);
        MyIDpostGetAuthIdentityDataModel myIDpostGetAuthIdentityDataModel = new MyIDpostGetAuthIdentityDataModel(identity, data);
        Call<JsonObject> call = apiService.postGetAuthIdentityData(authorzationTokentype, myIDpostGetAuthIdentityDataModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                // Log.d("res", "" + response);
                if (response.body().get("Data").toString().equals("null")) {
                    Toast.makeText(getActivity(), response.body().get("Error").getAsJsonObject().get("Message").toString().replace("\"", ""), Toast.LENGTH_SHORT).show();
                    setFragment(R.id.root_content, MIDHomeScanner.newInstance(), MIDHomeScanner.class.getSimpleName());
                } else {
                    try {
                        String mProfilePic = response.body().get("Data").getAsJsonObject().get("Photo").toString().replace("\"", "");
                        byte[] decodedString = Base64.decode(mProfilePic, Base64.DEFAULT);
                        decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        if (decodedByte != null) {
                            //mProfilePicImgview.setImageDrawable(decodedByte);
                            mProfilePicImgview.setImageBitmap(decodedByte);
                        } else {
                            mProfilePicImgview.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
                        }
                        mCivilID = response.body().get("Data").getAsJsonObject().get("CivilID").toString().replace("\"", "");
                        mFullNameAr = response.body().get("Data").getAsJsonObject().get("FullNameAr").toString().replace("\"", "");
                        mFullNameEn = response.body().get("Data").getAsJsonObject().get("FullNameEn").toString().replace("\"", "");
                        mBirthDate = response.body().get("Data").getAsJsonObject().get("BirthDate").toString().replace("\"", "");
                        mGender = response.body().get("Data").getAsJsonObject().get("Gender").toString().replace("\"", "");
                        mEmailAddress = response.body().get("Data").getAsJsonObject().get("EmailAddress").toString().replace("\"", "");
                        mMobileNumber = response.body().get("Data").getAsJsonObject().get("MobileNumber").toString().replace("\"", "");
                        mNationalityEn = response.body().get("Data").getAsJsonObject().get("NationalityEn").toString().replace("\"", "");
                        mNationalityAr = response.body().get("Data").getAsJsonObject().get("NationalityAr").toString().replace("\"", "");
                        mNationalityFlag = response.body().get("Data").getAsJsonObject().get("NationalityFlag").toString().replace("\"", "");
                        mBloodGroup = response.body().get("Data").getAsJsonObject().get("BloodGroup").toString().replace("\"", "");
                        mCardExpiryDate = response.body().get("Data").getAsJsonObject().get("CardExpiryDate").toString().replace("\"", "");

                        mCivilIDExpiry = response.body().get("Data").getAsJsonObject().get("GovData").getAsJsonObject().get("CivilIdExpiryDate").toString().replace("\"", "");
                        mMoiRef = response.body().get("Data").getAsJsonObject().get("GovData").getAsJsonObject().get("MOIReference").toString().replace("\"", "");
                        mSponser = response.body().get("Data").getAsJsonObject().get("GovData").getAsJsonObject().get("SponsorName").toString().replace("\"", "");

                        mGovernerate = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("Governerate").toString().replace("\"", "");
                        mAreaStr = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("Area").toString().replace("\"", "");
                        mPaciBuildingNumber = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("PaciBuildingNumber").toString().replace("\"", "");
                        mBuildingType = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("BuildingType").toString().replace("\"", "");
                        mFloorNumber = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("FloorNumber").toString().replace("\"", "");
                        mBuildingNumber = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("BuildingNumber").toString().replace("\"", "");
                        mBlockNumber = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("BlockNumber").toString().replace("\"", "");
                        mUnitNumber = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("UnitNumber").toString().replace("\"", "");
                        mStreetName = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("StreetName").toString().replace("\"", "");
                        mUnitTypeStr = response.body().get("Data").getAsJsonObject().get("Address").getAsJsonObject().get("UnitType").toString().replace("\"", "");

                        mNameEn.setText(mFullNameEn);
                        mNameAr.setText(mFullNameAr);
                        //mFlagImgv
                        sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
                        editors = sharedPreferences.edit();
                        language = sharedPreferences.getString(Locale_KeyValue, "ar");
                        if (language.equals("ar")) {
                            mCountryName.setText(mNationalityAr);
                        } else {
                            mCountryName.setText(mNationalityEn);
                        }

                        // mDob.setText(mBirthDate);

                        //mExpDate.setText(mCivilIDExpiry);


                        StringTokenizer dobtoken = new StringTokenizer(mBirthDate, "T");
                        String dob = dobtoken.nextToken();
                        String[] mDobsplit = dob.split("-");

                        StringTokenizer tokens = new StringTokenizer(mCivilIDExpiry, "T");
                        String expDate = tokens.nextToken();
                        String[] mexpDate = expDate.split("-");

                        mDob.setText(mDobsplit[02] + "/" + mDobsplit[01] + "/" + mDobsplit[0]);
                        mExpDate.setText(mexpDate[02] + "/" + mexpDate[01] + "/" + mexpDate[0]);

                        if (mFloorNumber.matches("\\s*") || mFloorNumber.equals("null")) {
                            mFloorNumber = "-";
                        }
                        if (mStreetName.matches("\\s*") || mStreetName.equals("null")) {
                            mStreetName = "-";
                        }
                        if (mUnitNumber.matches("\\s*") || mUnitNumber.equals("null")) {
                            mUnitNumber = "-";
                        }
                        if (mAreaStr.matches("\\s*") || mAreaStr.equals("null")) {
                            mAreaStr = "-";
                        }
                        if (mBlockNumber.matches("\\s*") || mBlockNumber.equals("null")) {
                            mBlockNumber = "-";
                        }
                        if (mUnitTypeStr.matches("\\s*") || mUnitTypeStr.equals("null")) {
                            mUnitTypeStr = "-";
                        }
                        mArea.setText(mAreaStr);
                        mBlock.setText(mBlockNumber);
                        mStreet.setText(mStreetName);
                        mUnitType.setText(mUnitTypeStr);
                        mUnitNo.setText(mUnitNumber);
                        mFloorNo.setText(mFloorNumber);
                        mPaciNo.setText(mPaciBuildingNumber);

                        //String mFlagPic = response.body().get("Data").getAsJsonObject().get("Photo").toString().replace("\"", "");
                        byte[] decodedStringFlag = Base64.decode(mNationalityFlag, Base64.DEFAULT);
                        decodedByteFlag = BitmapFactory.decodeByteArray(decodedStringFlag, 0, decodedStringFlag.length);
                        if (decodedByteFlag != null) {
                            //mProfilePicImgview.setImageDrawable(decodedByte);
                            mFlagImgv.setImageBitmap(decodedByteFlag);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }
}