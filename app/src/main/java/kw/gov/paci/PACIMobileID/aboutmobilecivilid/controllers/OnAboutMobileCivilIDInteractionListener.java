package kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers;

import kw.gov.paci.PACIMobileID.baseclasses.BaseInteractionListener;

public interface OnAboutMobileCivilIDInteractionListener extends BaseInteractionListener {

    void onBackButtonClicked();

    void goToNextScreen();

    void goToPreRequisitesScreen();

    void goToProcedureScreen();

    void goToGetMobileIDScreen();

    void goToPleaseWaitCivilIDScreen();

    void goToConfirmPinScreen();

    void goToAddPinCollectedSuccessfully();
}
