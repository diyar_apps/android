package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models;

/**
 * Created by SUNIL KUMAR V on 1/10/2019.
 */
public class AddActivityLogModel {
    private String mTitle;
    private String mTitltModule;
    private String mTitleModuleStatus;
    private String mPinlockStatus;
    private String mStatusBg;

    /**
     * No args constructor for use in serialization
     */
    public AddActivityLogModel() {
    }

    /**
     * @param mPinlockStatus
     * @param mStatusBg
     * @param mTitleModuleStatus
     * @param mTitltModule
     * @param mTitle
     */
    public AddActivityLogModel(String mTitle, String mTitltModule, String mTitleModuleStatus, String mPinlockStatus, String mStatusBg) {
        super();
        this.mTitle = mTitle;
        this.mTitltModule = mTitltModule;
        this.mTitleModuleStatus = mTitleModuleStatus;
        this.mPinlockStatus = mPinlockStatus;
        this.mStatusBg = mStatusBg;
    }

    public String getMTitle() {
        return mTitle;
    }

    public void setMTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getMTitltModule() {
        return mTitltModule;
    }

    public void setMTitltModule(String mTitltModule) {
        this.mTitltModule = mTitltModule;
    }

    public String getMTitleModuleStatus() {
        return mTitleModuleStatus;
    }

    public void setMTitleModuleStatus(String mTitleModuleStatus) {
        this.mTitleModuleStatus = mTitleModuleStatus;
    }

    public String getMPinlockStatus() {
        return mPinlockStatus;
    }

    public void setMPinlockStatus(String mPinlockStatus) {
        this.mPinlockStatus = mPinlockStatus;
    }

    public String getMStatusBg() {
        return mStatusBg;
    }

    public void setMStatusBg(String mStatusBg) {
        this.mStatusBg = mStatusBg;
    }
}
