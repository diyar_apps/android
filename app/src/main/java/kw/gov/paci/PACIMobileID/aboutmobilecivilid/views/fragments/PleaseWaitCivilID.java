package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.wang.avi.AVLoadingIndicatorView;

import java.net.SocketTimeoutException;
import java.util.Objects;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.updatedTransModel.Data;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.updatedTransModel.MyIDUpdateTransactionProgressModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.utils.Constants;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class PleaseWaitCivilID extends MobileCivilIDBaseFragment {
    public static final String IS_EDIT = "type";
    private OnAboutMobileCivilIDInteractionListener mListener;
    public CustomTextView header_Mobile_txt, body_Mobile_txt;
    public static Bundle args;
    public static String mQrData;
    public String generatedRandomkey_encrypt, generatedRandomkey_decrypt;
    public RetroApiInterface apiService;
    public SharedPreferences preferences, preferences_addgetmobileid;
    public SharedPreferences.Editor editor;
    private String passEncryptedCivilID;
    public AVLoadingIndicatorView mAVLoadingIndicatorView;
    public String CollectionUrl, mCivilIDNum;
    String authorzation_tokentype_str;

    public PleaseWaitCivilID() {
        // Required empty public constructor
    }

    public static PleaseWaitCivilID newInstance(String msg) {
        PleaseWaitCivilID fragment = new PleaseWaitCivilID();
        args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        preferences_addgetmobileid = Objects.requireNonNull(getActivity()).getSharedPreferences("randomkey", MODE_PRIVATE);
        generatedRandomkey_encrypt = preferences_addgetmobileid.getString("RkeyValue", "default");
        generatedRandomkey_decrypt = generatedRandomkey_encrypt;

        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        if (Utilities.isNetworkAvailable(getActivity())) {
            mQrData =mQrData.replace("PACMD-", "");
            Data data_trans_mobile_id = new Data(mQrData, generatedRandomkey_decrypt);
            MyIDUpdateTransactionProgressModel myIDUpdateTransactionProgressModel = new MyIDUpdateTransactionProgressModel(data_trans_mobile_id);
            mScanCodeRandomKey(BuildConfig.mBasicAuthorization, myIDUpdateTransactionProgressModel);
        }
        return root;
    }

    @Override
    public void onResume() {
        enForcedLocale();
        super.onResume();
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAboutMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.fragment_please_wait_header, container, false);
        mAVLoadingIndicatorView = header.findViewById(R.id.material_design_ball_scale_ripple_loader);
        mAVLoadingIndicatorView.setVisibility(View.VISIBLE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_please_wait_body, container, false);
        header_Mobile_txt = profileBody.findViewById(R.id.please_txt);
        body_Mobile_txt = profileBody.findViewById(R.id.collecting_you);
        setImageResource();
        mBody.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mRootView.setVisibility(View.VISIBLE);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.VISIBLE);
        mInfoButton.setVisibility(View.GONE);
        mCancelButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
    }

    public void setImageResource() {
        header_Mobile_txt.setText(R.string.Please_Wait);
        body_Mobile_txt.setText(R.string.Collecting_you_Mobile_Civil_ID);
    }

    public void mScanCodeRandomKey(final String mAuthrozationType, final MyIDUpdateTransactionProgressModel myIDUpdateTransactionProgressModel) {
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Call<JsonObject> call = apiService.scanUser(BuildConfig.mBasicAuthorization, myIDUpdateTransactionProgressModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    if (response.isSuccessful()) {
                        if (!response.body().get("Data").isJsonNull()) {
                            editor.putString("TransactionId", mQrData);
                            editor.putString("MobileCode", generatedRandomkey_decrypt);
                            String mCivilID = response.body().get("Data").getAsJsonObject().get("CivilNo").toString().replace("\"", "");
                            String mAuthToken = response.body().get("Data").getAsJsonObject().get("Token").toString().replace("\"", "");
                            editor.putString("CivilNo", mCivilID);
                            mAuthToken = Constants.mBearer + mAuthToken;
                            editor.putString("authorzation_tokentype", mAuthToken);
                            editor.apply();
                            getCivilIDDetailsApiCall(getActivity());
                        } else if (response.body().get("Error").getAsJsonObject().get("Code").getAsInt() == 400 || response.body().get("Data").isJsonNull()) {
                            Toast.makeText(getActivity(), R.string.Unable_to_get_the_data_from_server, Toast.LENGTH_SHORT).show();
                            mListener.goToGetMobileIDScreen();
                        } else {
                            Toast.makeText(getActivity(), R.string.Unable_to_get_the_data_from_server, Toast.LENGTH_SHORT).show();
                            mListener.goToGetMobileIDScreen();
                        }
                    } else {
                        Toast.makeText(getActivity(), "" + response.errorBody(), Toast.LENGTH_SHORT).show();
                        mListener.goToGetMobileIDScreen();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                    mListener.goToGetMobileIDScreen();
                }
            }
        });
    }

    private void getCivilIDDetailsApiCall(Activity activity) {
        if (Utilities.isNetworkAvailable(activity)) {
            getCivilIDData(mQrData, getActivity());
        }
    }

    private void getCivilIDData(String qrcode, final Activity activity) {
        mCivilIDNum = preferences.getString("CivilNo", "default");
        authorzation_tokentype_str = preferences.getString("authorzation_tokentype", "default");
        Identity identity = new Identity(mCivilIDNum, "");
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilIDNum, mQrData, "");
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data);
        Call<JsonObject> call = apiService.getCivilIDResponse(authorzation_tokentype_str, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    if (response.isSuccessful()) {
                        if (!response.body().get("Data").isJsonNull()) {
                            String mID = response.body().get("Data").getAsJsonObject().get("Id").toString().replace("\"", "");
                            String mCivilID = response.body().get("Data").getAsJsonObject().get("CivilNo").toString().replace("\"", "");
                            String mStatus = response.body().get("Data").getAsJsonObject().get("Status").toString().replace("\"", "");
                            String mMyidProfileName = response.body().get("Data").getAsJsonObject().get("MyidProfileName").toString().replace("\"", "");
                            String mMyidJobId = response.body().get("Data").getAsJsonObject().get("MyidJobId").toString().replace("\"", "");
                            String mChallenge = response.body().get("Data").getAsJsonObject().get("Challenge").toString().replace("\"", "");
                            String mCollectionUrl = response.body().get("Data").getAsJsonObject().get("CollectionUrl").toString().replace("\"", "");

                            editor.putString("cvilId", mID);
                            editor.putString("CivilNo", mCivilID);
                            editor.putString("Status", mStatus);
                            editor.putString("MyidProfileName", mMyidProfileName);
                            editor.putString("MyidJobId", mMyidJobId);
                            editor.putString("Challenge", mChallenge);
                            editor.putString("CollectionUrl", mCollectionUrl);
                            editor.apply();
                            passEncryptedCivilID = preferences.getString("CivilNo", mCivilID);
                            mCivilID = passEncryptedCivilID;
                            CollectionUrl = preferences.getString("CollectionUrl", mCollectionUrl);
                            getPersonalAllData(activity, mCivilID);
                        } else {
                            Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_SHORT).show();
                            mListener.goToGetMobileIDScreen();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_SHORT).show();
                        mListener.goToGetMobileIDScreen();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
            }
        });
    }

    private void getPersonalAllData(Activity activity, String civilid) {
        if (Utilities.isNetworkAvailable(activity)) {
            mCivilIDNum = preferences.getString("CivilNo", "default");
            getPersonalDataDetails(mCivilIDNum);
        }
    }

    private void getPersonalDataDetails(String civilid) {
        authorzation_tokentype_str = preferences.getString("authorzation_tokentype", "default");
        Identity identity = new Identity(civilid, "");
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(civilid, mQrData, "");
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data);
        Call<JsonObject> call = apiService.getAllPersonalData(authorzation_tokentype_str, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                //Log.d("resp", "" + response);
                try {
                    if (response.isSuccessful()) {
                        if (!response.body().get("Data").isJsonNull()) {
                            String mFullNameAr = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("FullNameAr").toString().replace("\"", "");
                            String mFullNameEn = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("FullNameEn").toString().replace("\"", "");
                            String mBirthDate = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BirthDate").toString().replace("\"", "");
                            String mGender = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Gender").toString().replace("\"", "");
                            String mEmailAddress = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("EmailAddress").toString().replace("\"", "");
                            String mMobileNumber = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("MobileNumber").toString().replace("\"", "");
                            String mNationalityEn = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("NationalityEn").toString().replace("\"", "");
                            String mNationalityAr = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("NationalityAr").toString().replace("\"", "");
                            String mNationalityFlag = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("NationalityFlag").toString().replace("\"", "");
                            String mBloodGroup = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BloodGroup").toString().replace("\"", "");
                            String mPhoto = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Photo").toString().replace("\"", "");
                            String mCivilIdExpiryDate = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("GovData").getAsJsonObject().get("CivilIdExpiryDate").toString().replace("\"", "");
                            String mMOIReference = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("GovData").getAsJsonObject().get("MOIReference").toString().replace("\"", "");
                            String mSponsorName = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("GovData").getAsJsonObject().get("SponsorName").toString().replace("\"", "");
                       /* String mOrganizationNameAr = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BusinessData").getAsJsonObject().get("OrganizationNameAr").toString().replace("\"", "");
                        String mOrganizationNameEn = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BusinessData").getAsJsonObject().get("OrganizationNameEn").toString().replace("\"", "");
                        String mOrganizationUnitNameAr = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BusinessData").getAsJsonObject().get("OrganizationUnitNameAr").toString().replace("\"", "");
                        String mOrganizationUnitNameEn = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BusinessData").getAsJsonObject().get("OrganizationUnitNameEn").toString().replace("\"", "");
                        String mJobTitleAr = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BusinessData").getAsJsonObject().get("JobTitleAr").toString().replace("\"", "");
                        String mJobTitleEn = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("BusinessData").getAsJsonObject().get("JobTitleEn").toString().replace("\"", "");
                       */
                            String mGovernerate = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("Governerate").toString().replace("\"", "");
                            String mArea = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("Area").toString().replace("\"", "");
                            String mPaciBuildingNumber = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("PaciBuildingNumber").toString().replace("\"", "");
                            String mBuildingType = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("BuildingType").toString().replace("\"", "");
                            String mFloorNumber = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("FloorNumber").toString().replace("\"", "");
                            String mBuildingNumber = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("BuildingNumber").toString().replace("\"", "");
                            String mBlockNumber = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("BlockNumber").toString().replace("\"", "");
                            String mUnitNumber = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("UnitNumber").toString().replace("\"", "");
                            String mStreetName = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("StreetName").toString().replace("\"", "");
                            String mUnitType = response.body().get("Data").getAsJsonObject().get("MobileCivilIdData").getAsJsonObject().get("Address").getAsJsonObject().get("UnitType").toString().replace("\"", "");
                            String mCredentialRenewalPeriod = response.body().get("Data").getAsJsonObject().get("MobileCredentialSettings").getAsJsonObject().get("CredentialRenewalPeriod").toString().replace("\"", "");

                            // Write
                            editor.putString("FullNameAr", mFullNameAr);
                            editor.putString("FullNameEn", mFullNameEn);
                            editor.putString("BirthDate", mBirthDate);
                            editor.putString("Gender", mGender);
                            editor.putString("EmailAddress", mEmailAddress);
                            editor.putString("MobileNumber", mMobileNumber);
                            editor.putString("NationalityEn", mNationalityEn);
                            editor.putString("NationalityAr", mNationalityAr);
                            editor.putString("NationalityFlag", mNationalityFlag);
                            editor.putString("BloodGroup", mBloodGroup);
                            mPhoto = mPhoto.replace("\\r\\n", "");
                            editor.putString("Photo", mPhoto);
                            //GovData
                            editor.putString("CivilIdExpiryDate", mCivilIdExpiryDate);
                            editor.putString("MOIReference", mMOIReference);
                            editor.putString("SponsorName", mSponsorName);
                            //BusinessData
                       /* editor.putString("OrganizationNameAr", mOrganizationNameAr);
                        editor.putString("OrganizationNameEn", mOrganizationNameEn);
                        editor.putString("OrganizationUnitNameAr", mOrganizationUnitNameAr);
                        editor.putString("OrganizationUnitNameEn", mOrganizationUnitNameEn);
                        editor.putString("JobTitleAr", mJobTitleAr);
                        editor.putString("JobTitleEn", mJobTitleEn);*/
                            //Address
                            editor.putString("Governerate", mGovernerate);
                            editor.putString("Area", mArea);
                            editor.putString("PaciBuildingNumber", mPaciBuildingNumber);
                            editor.putString("BuildingType", mBuildingType);
                            editor.putString("FloorNumber", mFloorNumber);
                            editor.putString("BuildingNumber", mBuildingNumber);
                            editor.putString("BlockNumber", mBlockNumber);
                            editor.putString("UnitNumber", mUnitNumber);
                            editor.putString("StreetName", mStreetName);
                            editor.putString("UnitType", mUnitType);
                            //MobileCredentialSettings
                            editor.putString("CredentialRenewalPeriod", mCredentialRenewalPeriod);
                            editor.apply();
                            mListener.goToConfirmPinScreen();
                        }
                    } else {
                        Toast.makeText(getActivity(), response.errorBody().string(), Toast.LENGTH_SHORT).show();
                        mListener.goToGetMobileIDScreen();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {

            }
        });
    }
}



