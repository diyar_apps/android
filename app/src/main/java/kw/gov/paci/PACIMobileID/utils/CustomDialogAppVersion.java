package kw.gov.paci.PACIMobileID.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.AddPinRequestScreen;
import kw.gov.paci.PACIMobileID.splash.SplashScreen;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.Objects;

/**
 * Created by SUNIL KUMAR V on 1/15/2019.
 */
public class CustomDialogAppVersion extends Dialog implements
        android.view.View.OnClickListener {
    public Activity activity;
    public CustomButton mUpdateRoot, mCloseBtnRoot;
    public CustomTextView mAppVersionText;
    public SharedPreferences preferences_pls_wait;
    public SharedPreferences.Editor editor_pls_wait;
    public String mAppVersion;

    public CustomDialogAppVersion(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.custom_dialog_app_version);
        mUpdateRoot = findViewById(R.id.update_txt);
        mCloseBtnRoot = findViewById(R.id.close_txt);
        mAppVersionText = findViewById(R.id.appversion_text);

        preferences_pls_wait = Objects.requireNonNull(activity).getSharedPreferences("plswait", Context.MODE_PRIVATE);
        editor_pls_wait = preferences_pls_wait.edit();
        editor_pls_wait.apply();
        mAppVersion = preferences_pls_wait.getString("AppVersion", "default");
        Resources res = activity.getResources();
        @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(res.getString(R.string.s_1_app_version_alert_txt), mAppVersion);
        mAppVersionText.setText(text);

        mUpdateRoot.setOnClickListener(this);
        mCloseBtnRoot.setOnClickListener(this);
        setCanceledOnTouchOutside(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update_txt:
                try {
                    Intent viewIntent =
                            new Intent("android.intent.action.VIEW",
                                    Uri.parse("https://play.google.com/store/apps/details?id=kw.gov.paci.PACIMobileID"));
                    this.activity.startActivity(viewIntent);
                } catch (Exception e) {
                    Toast.makeText(activity, "Unable to Connect Try Again...",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                //dismiss();
                break;
            case R.id.close_txt:
                dismiss();
                activity.finish();
                break;
            default:
                break;
        }
        //dismiss();
    }
}