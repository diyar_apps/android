package kw.gov.paci.PACIMobileID.home.views.fragments.DigitalSign;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.CustomDialog;
import kw.gov.paci.PACIMobileID.utils.CustomDialogWindow;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.util.FitPolicy;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 11/10/2018.
 */
public class AddWebviewSignScreen extends MobileCivilIDBaseFragment implements View.OnClickListener, OnPageChangeListener, OnLoadCompleteListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public LinearLayout mScanQrCodeLL, mAuthenticateRequestLL;
    public CustomTextView mFullNameEN, mCivilIDNum;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mCivilIDStr, mFullNameStr;
    public static String mFileNamePath, mFileName;
    private static final String TAG = AddWebviewSignScreen.class.getSimpleName();
    PDFView pdfView;
    Integer pageNumber = 0;
    String pdfFileName;
    public CustomButton mDigitalSignBtn, mDigitalCancelBtn;
    public RetroApiInterface apiService;
    public SharedPreferences.Editor editor_splash;
    public String sign;
    public Boolean mCancelDigitalSignStatus;
    public ImageView mProfilePic;
    public String mProfilePhoto;
    public Bitmap decodedByte;
    private Resources mResources;
    String language;
    private static final String Locale_Preference = "Locale Preference";
    public static final String Locale_KeyValue = "Saved Locale";
    public SharedPreferences sharedPreferences;
    public SharedPreferences.Editor editors;
    public String mFullNameAr;
    public static int SPLASH_TIME_OUT = 500;
    public LinearLayout mRootProgressLayout;
    public static int RENDER_TIME_OUT = 500;

    public AddWebviewSignScreen() {
        // Required empty public constructor
    }

    public static AddWebviewSignScreen newInstance(String s1, String s2) {
        AddWebviewSignScreen fragment = new AddWebviewSignScreen();
        Bundle args = new Bundle();
        if (s1 != null) {
            mFileNamePath = s1;
            mFileName = s2;
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        //((AddHomeActivity) Objects.requireNonNull(getActivity())).mBottomNavigationBarRoot.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_add_webview_sign_screen, container, false);
        mFullNameEN = profileBody.findViewById(R.id.full_en_name);
        mProfilePic = profileBody.findViewById(R.id.profile_pic_imgv);
        pdfView = profileBody.findViewById(R.id.pdfView);
        mRootProgressLayout = profileBody.findViewById(R.id.root_progress_view);
        mCivilIDNum = profileBody.findViewById(R.id.civiild_id_display);
        mDigitalSignBtn = profileBody.findViewById(R.id.sign_digital);
        mDigitalCancelBtn = profileBody.findViewById(R.id.cancel_digital);
        mBody.setVisibility(View.GONE);
       /* pdfView.setVisibility(View.GONE);
        mRootProgressLayout.setVisibility(View.VISIBLE);*/

 /*       new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                CustomDialogWindow customDialog = new CustomDialogWindow(getActivity());
                Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                customDialog.show();
            }
        }, SPLASH_TIME_OUT);*/
        CustomDialogWindow customDialog = new CustomDialogWindow(getActivity());
        Objects.requireNonNull(customDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        customDialog.show();

        mDigitalSignBtn.setOnClickListener(this);
        mDigitalCancelBtn.setOnClickListener(this);
        mFullFrame.setVisibility(View.VISIBLE);
        mResources = getResources();
        mSetProfilePicImage();
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getDataFromServer();
                displayFromAsset(mFileName);
            }
        }, SPLASH_TIME_OUT);

        /*try {
            new MyAsyncTask().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        mFullFrame.addView(profileBody);
    }

    @SuppressLint({"SetJavaScriptEnabled", "CommitPrefEdits"})
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mCivilIDStr = preferences.getString(("CivilNo"), "default");
            mCivilIDStr = (mCivilIDStr);
            mFullNameStr = preferences.getString(("FullNameEn"), "default");
            mFullNameStr = (mFullNameStr);
            mFullNameAr = preferences.getString(("FullNameAr"), "default");
            Resources res = getResources();
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Civil_ID_No), mCivilIDStr);
            mCivilIDNum.setText(text);
            mFullNameEN.setText(mFullNameStr);
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                mFullNameEN.setText(mFullNameAr);
            } else {
                mFullNameEN.setText(mFullNameStr);
            }

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_digital:
                mCancelDigitalSignStatus = false;
                mListener.goToAddDigitalSignDeclinePinScreen("ClickedDigiSiginButton", mFileNamePath);
                break;
            case R.id.cancel_digital:
                mCancelDigitalSignStatus = true;
                mListener.goToAddDigitalSignDeclinePinScreen("ClickedDigiDeclineButton", mFileNamePath);
                break;
        }
    }

    public void setImageResource() {

    }

    private void displayFromAsset(String assetFileName) {
        try {
            pdfFileName = assetFileName;
            File file1 = new File(mFileNamePath);
            pdfView.fromFile(file1)
                    .pages(0) // all pages are displayed by default
                    .defaultPage(1)
                    // .onPageChange(this)
                    //.pageSnap(true)
                    // .pageFling(true)
                    .swipeHorizontal(false)
                    .pageFitPolicy(FitPolicy.WIDTH)
                    .onLoad(this)
                    // .showMinimap(false)
                    .load();
           /* pdfView.fromFile(file1)
                    .defaultPage(1)
                    .enableSwipe(true)
                    .swipeHorizontal(false)
                    .onPageChange(this)
                    .enableAnnotationRendering(true)
                    .onLoad(this)
                    .enableAntialiasing(true)
                    .scrollHandle(new DefaultScrollHandle(getActivity()))
                    .load();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
    }

    @Override
    public void loadComplete(int nbPages) {
        // PdfDocument.Meta meta = pdfView.getDocumentMeta();
        // printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {

            //Log.e(TAG, String.format("%s %s, p %d", sep, b.getTitle(), b.getPageIdx()));

            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class MyAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            try {
                displayFromAsset(mFileName);
                // new mRenderPostDelay().invoke();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           /* pdfView.setVisibility(View.GONE);
            mRootProgressLayout.setVisibility(View.VISIBLE);*/
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
           /* mRootProgressLayout.setVisibility(View.GONE);
            pdfView.setVisibility(View.VISIBLE);*/
        }
    }

    private void mSetProfilePicImage() {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mProfilePhoto = preferences.getString(("Photo"), "default");
        mProfilePhoto = (mProfilePhoto);
        if (mProfilePhoto.equals("default") || mProfilePhoto.equals("null")) {
            mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
        } else {
            try {
                byte[] decodedString = Base64.decode(mProfilePhoto, Base64.DEFAULT);
                decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (decodedByte != null) {
                    mRoundedImageConvert();
                } else {
                    mProfilePic.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.user_icon_small));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void mRoundedImageConvert() {
        Paint paint = new Paint();
        int srcBitmapWidth = decodedByte.getWidth();
        int srcBitmapHeight = decodedByte.getHeight();
        int borderWidth = 0;
        int shadowWidth = 0;
        int dstBitmapWidth = Math.min(srcBitmapWidth, srcBitmapHeight) + borderWidth * 2;
        Bitmap dstBitmap = Bitmap.createBitmap(dstBitmapWidth, dstBitmapWidth, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(dstBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(decodedByte, (dstBitmapWidth - srcBitmapWidth) / 2, (dstBitmapWidth - srcBitmapHeight) / 2, null);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(borderWidth * 2);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(shadowWidth);
        canvas.drawCircle(canvas.getWidth() / 2, canvas.getHeight() / 2, canvas.getWidth() / 2, paint);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mResources, dstBitmap);
        roundedBitmapDrawable.setCircular(true);
        roundedBitmapDrawable.setAntiAlias(true);
        mProfilePic.setImageDrawable(roundedBitmapDrawable);
    }

}