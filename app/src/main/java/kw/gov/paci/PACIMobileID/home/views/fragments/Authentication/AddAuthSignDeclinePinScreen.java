package kw.gov.paci.PACIMobileID.home.views.fragments.Authentication;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.GetAuthRequestDetailsModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.Data;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.MyIDGetAuthRequestModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.MyIDpostAuthenticateRequestModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.LoginActivityScreen;
import kw.gov.paci.PACIMobileID.utils.CSRHelper;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomEditTextView;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.fxn.stash.Stash;
import com.google.gson.JsonObject;
import com.intercede.myIDSecurityLibrary.IPinCallback;
import com.intercede.myIDSecurityLibrary.IReturnUserPin;
import com.intercede.myIDSecurityLibrary.ISigningResult;
import com.intercede.myIDSecurityLibrary.InvalidContextException;
import com.intercede.myIDSecurityLibrary.MyIDSecurityLibrary;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationMechanisms;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationParameters;
import com.intercede.myIDSecurityLibrary.MyIdSignDataResponse;
import com.intercede.myIDSecurityLibrary.MyIdSigningParameters;
import com.intercede.myIDSecurityLibrary.SigningAlgorithm;

import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 11/13/2018.
 */
public class AddAuthSignDeclinePinScreen extends MobileCivilIDBaseFragment implements View.OnClickListener, ISigningResult, IPinCallback {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public static String mQrData, mStatusScreen;
    public CustomTextView mPlsEnterHeadertxt, mPinMustBetxt, mItisHighlytxt, pinlocked;
    public LinearLayout mOldPinViewLL, mConfirmPinViewLL, mEnterPinViewLL;
    public CustomButton mConfirmRequestPinBtn;
    public SharedPreferences preferences, preferencese_splash;
    public SharedPreferences.Editor editor, editor_splash;
    public String mConfirmPinStr;
    public CustomEditTextView mEnterPinValue;
    public RetroApiInterface apiService;
    private String data = null;
    public GetAuthRequestDetailsModel myResponse;
    public String mDeviceIdValue, mRequestIdvalue, mPKCS7Value, mStatusIdValue;
    public String sign;
    public CustomEditTextView mConfirmPInED;
    byte[] sigBytes;
    public Boolean mCancelAuthenStatus = false;
    public String failStatus;
    private boolean mIsLoginEnable = true;
    private int mCurrentLoginFailCount = 0;
    private Long mLastEnteredWrongPassword = 0L;
    public String mCurrentLoginFailCountShared_pref;
    public LinearLayout mProgressbarll;
    MyIDSecurityLibrary myIDSecurityLibrary;
    MyIdSigningParameters signingParameters;
    MyIdAuthenticationParameters authenticationParameters;
    public String mAddAuthSignDecScreenStatus = "mAddAuthSignDecScreenStatus";
    String authorzation_tokentype, mSerialNumber, CivilNo;
    public String mAddAuthSignDecSignPostScreenStatus = "mAddAuthSignDecSignPostScreenStatus";
    public String mAddAuthSignDecCancelSignPostScreenStatus = "mAddAuthSignDecCancelSignPostScreenStatus";
    static HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;
    Boolean pinlockedBooleanType;
    private ArrayList<AddActivityLogModel> addActivityLogModelsArrayList;
    public static final String ACTIVITY_LOGS_AUTHEN = "ACTIVITY_LOGS_AUTHE";
    public static final String ACTIVITY_LOGS_AUTHEN_FAILD = "ACTIVITY_LOGS_AUTHE_FAILD";
    private AlertDialog pinDialog;
    int count;
    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";

    public AddAuthSignDeclinePinScreen() {
        // Required empty public constructor
    }

    public static AddAuthSignDeclinePinScreen newInstance(String msg, String mSigingStatus) {
        AddAuthSignDeclinePinScreen fragment = new AddAuthSignDeclinePinScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
            mStatusScreen = mSigingStatus;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        if (pinlockedBooleanType) {
            //pinlocked.setVisibility(View.GONE);
            pinlocked.setVisibility(View.VISIBLE);
            pinlocked.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            pinlocked.startAnimation(anim);
        } else {
            pinlocked.setVisibility(View.GONE);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_home_enter_pin, container, false);
        mPlsEnterHeadertxt = profileBody.findViewById(R.id.header_enter_pin_edtxt);
        pinlocked = profileBody.findViewById(R.id.pinlocked);
        mEnterPinValue = profileBody.findViewById(R.id.et_new_pin);
        mConfirmPInED = profileBody.findViewById(R.id.et_confrm_pin);
        mConfirmRequestPinBtn = profileBody.findViewById(R.id.confirm_request_pin);
        mItisHighlytxt = profileBody.findViewById(R.id.it_is_highly_txt);
        mPinMustBetxt = profileBody.findViewById(R.id.pin_must_txt);
        mOldPinViewLL = profileBody.findViewById(R.id.pin_old_layout_root);
        mConfirmPinViewLL = profileBody.findViewById(R.id.pin_confrm_layout_root);
        mEnterPinViewLL = profileBody.findViewById(R.id.pin_new_layout_root);
        mProgressbarll = profileBody.findViewById(R.id.progress_bar_ll);
        mPlsEnterHeadertxt.setText(R.string.AuthoPIN_Content_one);
        mItisHighlytxt.setVisibility(View.INVISIBLE);
        mPinMustBetxt.setVisibility(View.INVISIBLE);
        mConfirmPinViewLL.setVisibility(View.INVISIBLE);
        mOldPinViewLL.setVisibility(View.INVISIBLE);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mConfirmRequestPinBtn.setOnClickListener(this);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_request_pin:
                mConfirmRequestPinBtn.setEnabled(false);
                headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
                headlessUnlockPinCallbackHandler.initialise(getActivity());
                pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
                if (pinlockedBooleanType) {
                    pinlocked.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
                } else {
                    try {
                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        mConfirmPinStr = preferences.getString(("mConfirmPinED"), "default");
                        if (mStatusScreen != null && mStatusScreen.equals("ClickedAuthenButton")) {
                            mCancelAuthenStatus = false;
                            if (mEnterPinValue.getText().toString().length() <= 0) {
                                Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                                mConfirmRequestPinBtn.setEnabled(true);
                            } else if (mEnterPinValue.getText().toString().length() <= 3) {
                                Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                                mConfirmRequestPinBtn.setEnabled(true);
                            } else if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                                getDataFromServer();
                            } else {
                                getDataFromServer();
                               /* boolean exceptionThrown = false;
                                try {
                                    Authentication auth = new Authentication(getActivity());
                                    auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                                } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                                    e.printStackTrace();
                                } catch (IncorrectPinException e) {
                                    exceptionThrown = true;
                                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                    errorMsg.append("\n");
                                    errorMsg.append(e.numberOfAttemptsRemaining);
                                    errorMsg.append(" attempts remaining.");
                                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                                if (!exceptionThrown) {
                                    //result.setText(getString(R.string.pin_successfully_changed));
                                    if (Utilities.isNetworkAvailable(getActivity())) {
                                        mProgressbarll.setVisibility(View.VISIBLE);
                                        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                                        // mGetAuthRequestDetails(mQrData);
                                        tokenIdentity = new TokenIdentity(getActivity());
                                        tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecScreenStatus, mGrant_type, mUserName, mPassword, this);
                                    }
                                }*/
                                if (Utilities.isNetworkAvailable(getActivity())) {
                                    mProgressbarll.setVisibility(View.VISIBLE);
                                    // mGetAuthRequestDetails(mQrData);
                                    //tokenIdentity = new TokenIdentity(getActivity());
                                    //tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
                                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                                    if (Utilities.isNetworkAvailable(getActivity())) {
                                        mGetAuthRequestDetails(mQrData);
                                    }
                                }
                            }
                        } else {
                            mCancelAuthenStatus = true;
                            if (mEnterPinValue.getText().toString().length() <= 0) {
                                Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                            } else if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                                getDataFromServer();
                            } else {
                                getDataFromServer();
                               /* boolean exceptionThrown = false;
                                try {
                                    Authentication auth = new Authentication(getActivity());
                                    auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                                } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                                    e.printStackTrace();
                                } catch (IncorrectPinException e) {
                                    exceptionThrown = true;
                                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                    errorMsg.append("\n");
                                    errorMsg.append(e.numberOfAttemptsRemaining);
                                    errorMsg.append(" attempts remaining.");
                                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                                    e.printStackTrace();
                                }
                                if (!exceptionThrown) {
                                    //result.setText(getString(R.string.pin_successfully_changed));
                                    if (Utilities.isNetworkAvailable(getActivity())) {
                                        mProgressbarll.setVisibility(View.VISIBLE);
                                        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                                        // mGetAuthRequestDetails(mQrData);
                                        tokenIdentity = new TokenIdentity(getActivity());
                                        tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecScreenStatus, mGrant_type, mUserName, mPassword, this);
                                    }
                                }*/
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
        }
    }

    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mConfirmPinStr = preferences.getString("mConfirmPinED", "default");
            if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                if (Utilities.isNetworkAvailable(getActivity())) {
                    mProgressbarll.setVisibility(View.VISIBLE);
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    mGetAuthRequestDetails(mQrData);
                }
            } else {
                /*boolean exceptionThrown = false;
                try {
                    Authentication auth = new Authentication(getActivity());
                    auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                    e.printStackTrace();
                } catch (IncorrectPinException e) {
                    exceptionThrown = true;
                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                    errorMsg.append("\n");
                    errorMsg.append(e.numberOfAttemptsRemaining);
                    errorMsg.append(" attempts remaining.");
                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                if (!exceptionThrown) {
                    //result.setText(getString(R.string.pin_successfully_changed));
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        mProgressbarll.setVisibility(View.VISIBLE);
                        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                        // mGetAuthRequestDetails(mQrData);
                        tokenIdentity = new TokenIdentity(getActivity());
                        tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecScreenStatus, mGrant_type, mUserName, mPassword, this);
                    }
                }*/
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public void mGetAuthRequestDetails(final String mQrData) {
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mSerialNumber, CivilNo);
        Data data_object = new Data(mQrData);
        MyIDGetAuthRequestModel myIDGetAuthRequestModel = new MyIDGetAuthRequestModel(identity, data_object);
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Call<GetAuthRequestDetailsModel> call = apiService.getAuthRequestDetailsUser(authorzation_tokentype, myIDGetAuthRequestModel);
        call.enqueue(new Callback<GetAuthRequestDetailsModel>() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onResponse(@NonNull Call<GetAuthRequestDetailsModel> call, @NonNull Response<GetAuthRequestDetailsModel> response) {
                try {
                    myResponse = response.body();
                    data = String.valueOf(response.body().getData());
                    if (data != null && data.length() > 0) {
                        editor.putString("Id", (Integer.toString(myResponse.getData().getId())));
                        editor.putString("ServiceProviderId", (myResponse.getData().getServiceProviderId()));
                        editor.putString("PromptAr", (myResponse.getData().getPromptAr()));
                        editor.putString("PromptEn", (myResponse.getData().getPromptEn()));
                        editor.putString("RequestChannelId", (Integer.toString(myResponse.getData().getRequestChannelId())));
                        // editor.putString(("PersonCivilNo"), (myResponse.getData().getPersonCivilNo()));
                        //  editor.putString(("AdditionalSPData"), (myResponse.getData().getAdditionalSPData()));
                        editor.putString("Challenge", (myResponse.getData().getChallenge()));
                        editor.putString("CallbackURL", (myResponse.getData().getCallbackURL()));
                        //editor.putString(("ResponseData"), (myResponse.getData().getResponseData()));
                        editor.putString("StatusId", (Integer.toString(myResponse.getData().getStatusId())));
                        editor.putString("CreatedAt", (myResponse.getData().getCreatedAt()));
                        editor.apply();
                        //mListener.goToAddPinSuccessfulScreen();
                        preferencese_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
                        editor_splash = preferencese_splash.edit();
                        editor.apply();

                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();

                        mDeviceIdValue = preferences.getString("mCarrierID", "default");

                        // mRequestIdvalue = preferences.getString(("RequestId"), (Integer.toString(myResponse.getData().getRequestChannelId())));
                        mRequestIdvalue = preferences.getString(("RequestId"), (Integer.toString(myResponse.getData().getId())));
                        mRequestIdvalue = Integer.toString(myResponse.getData().getId());
                        editor.putString("RequestId", Integer.toString(myResponse.getData().getId()));
                        editor.apply();
                        mPKCS7Value = preferences.getString("PKCS7", myResponse.getData().getChallenge());
                        mStatusIdValue = preferences.getString(("StatusId"), (Integer.toString(myResponse.getData().getStatusId())));
                        if (Utilities.isNetworkAvailable(getActivity())) {
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
                            headlessUnlockPinCallbackHandler.initialise(getActivity());
                            pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
                            if (pinlockedBooleanType) {
                                /*mAuthenticateRequestSign(mDeviceIdValue, mRequestIdvalue, mPKCS7Value, mStatusIdValue);*/
                                Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();
                            } else {
                                mAuthenticateRequestSign(mDeviceIdValue, mRequestIdvalue, mPKCS7Value, mStatusIdValue);
                                /*Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();*/
                            }
                        }
                    } else if (data == null) {
                        Toast.makeText(getActivity(), String.valueOf(Objects.requireNonNull(response.body()).getError().getMessage()), Toast.LENGTH_SHORT).show();
                        setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                    } else if (data != null && data.equals("false")) {
                        Toast.makeText(getActivity(), String.valueOf(Objects.requireNonNull(response.body()).getError().getMessage()), Toast.LENGTH_SHORT).show();
                        setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                    } else {
                        Toast.makeText(getActivity(), String.valueOf(Objects.requireNonNull(response.body()).getError().getMessage()), Toast.LENGTH_SHORT).show();
                        setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetAuthRequestDetailsModel> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                    setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                }
            }
        });
    }

    private void mAuthenticateRequestSign(String mDeviceIdValue, String mRequestIdvalue, String mPKCS7Value, String mStatusIdValue) {
        try {
            try {
                sigBytes = mPKCS7Value.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            mProgressbarll.setVisibility(View.GONE);
            mMyIDSignData(sigBytes);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mAuthenticateRequestSignPost(String mDeviceIdValue, String mRequestIdvalue, String sign, String mStatusIdValue) {
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Identity identity_oj = new kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Identity(mSerialNumber, CivilNo);
        kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Data data_obj = new kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Data(mDeviceIdValue, mRequestIdvalue, sign, mStatusIdValue);
        MyIDpostAuthenticateRequestModel myIDpostAuthenticateRequestModel = new MyIDpostAuthenticateRequestModel(identity_oj, data_obj);
        Call<JsonObject> call = apiService.postAuthenticateRequest(authorzation_tokentype, myIDpostAuthenticateRequestModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mProgressbarll.setVisibility(View.GONE);
                    if (response.isSuccessful() && response.body().getAsJsonObject().get("Data").toString().equals("true")) {
                        if (!mCancelAuthenStatus) {
                            /*Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.authentication_qr), getString(R.string.authenticate_txt), getString(R.string.authentication_txt), getString(R.string.successfully_txt), dateToStr));
                            saveToSharedPreference(addActivityLogModelsArrayList);*/
                            Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.activity_log_document_authe_signed_succesfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                            failStatus = "successStatus";
                        } else {
                            /*Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.authentication_qr), getString(R.string.authenticate_txt), getString(R.string.authentication_txt), getString(R.string.failed_txt), dateToStr));
                            saveToSharedPreferenceFail(addActivityLogModelsArrayList);*/
                            Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.activity_log_document_authe_cancel_signed_succesfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                            failStatus = "failStatus";
                        }
                    } else {
                        Date today = new Date();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm", Locale.ENGLISH);
                        String dateToStr = format.format(today);
                        System.out.println(dateToStr);
                        MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.activity_log_document_authe_cancel_signed_succesfully), dateToStr));
                        saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                        failStatus = "failStatus";
                    }
                    AddPinSuccessfulScreen argumentFragment = new AddPinSuccessfulScreen();//Get Fragment Instance
                    Bundle data = new Bundle();//Use bundle to pass data
                    data.putString("failStatus", failStatus);//put string, int, etc in bundle with a key value
                    argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                    setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance(failStatus),
                            AddPinSuccessfulScreen.class.getSimpleName());


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    //message = "Socket Time out. Please try again.";
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }

    public void setImageResource() {
    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        try {
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(fragmentContainer, fragment, id);
            //fragmentTransaction.commit();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mMyIDSignData(byte[] sigBytes) {
        try {
            count = 0;
            myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(getActivity()));
            myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
            myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
            myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
            signingParameters = new MyIdSigningParameters();
            signingParameters.algorithm = SigningAlgorithm.SHA256_PKCS15_PKCS7;
            authenticationParameters = new MyIdAuthenticationParameters();
            authenticationParameters.permittedAuthentication = MyIdAuthenticationMechanisms.PIN_ONLY;
            authenticationParameters.pinCallback = this;
            //authenticationParameters.pinCallback.provideUserPin(this);
            myIDSecurityLibrary.signData(sigBytes, signingParameters, authenticationParameters, this);
        } catch (InvalidContextException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void signingSuccess(MyIdSignDataResponse signDataResponse) {
        //Log.d("signdataResp", "" + signDataResponse);

        if (signDataResponse.signedData == null) {
            Toast.makeText(getActivity(), "Data Was Not Signed", Toast.LENGTH_SHORT).show();
        } else {
            byte[] signedBytes = signDataResponse.signedData;
            try {
                sign = CSRHelper.encryptBASE64(signedBytes);
            } catch (Exception e) {
                e.printStackTrace();
            }
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            editor.putString("DigitalSignAuthenList", sign);
            editor.apply();
            if (signDataResponse.certificateData != null) {
                // Log.d("==sign1==", ""+signDataResponse.certificateData);
            } else {
                Toast.makeText(getActivity(), "No certificate data returned", Toast.LENGTH_SHORT).show();
            }
            if (!mCancelAuthenStatus) {
                if (Utilities.isNetworkAvailable(getActivity())) {
                    // mAuthenticateRequestSignPost(mDeviceIdValue, mRequestIdvalue, sign, mStatusIdValue);
                    // tokenIdentity = new TokenIdentity(getActivity());
                    //tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecSignPostScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        mAuthenticateRequestSignPost(mDeviceIdValue, mRequestIdvalue, sign, mStatusIdValue);
                    }
                }
            } else {
                if (Utilities.isNetworkAvailable(getActivity())) {
                    // mAuthenticateRequestSignPost(mDeviceIdValue, mRequestIdvalue, sign, "3");
                    //tokenIdentity = new TokenIdentity(getActivity());
                    //tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecCancelSignPostScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        mAuthenticateRequestSignPost(mDeviceIdValue, mRequestIdvalue, sign, "3");
                    }
                }
            }
        }
    }

    @Override
    public void signingError(Exception e) {
        Toast.makeText(getActivity(), "Error:" + e, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (myIDSecurityLibrary != null) {
            myIDSecurityLibrary.onStop();
            myIDSecurityLibrary = null;
        }

    }

   /* @Override
    public void setResult(String s) {
        //String a = "";
    }


    @Override
    public boolean isFirstAttempt() {
        return false;
    }

    @Override
    public Integer attemptsRemaining() {
        return 5;
    }*/

    @Override
    public void provideUserPin(final IReturnUserPin callback) {

       /* AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        if (!callback.isFirstAttempt()) {
            dialog.setTitle(R.string.Invalid_PIN);
            dialog.setMessage("\n" + callback.attemptsRemaining());
            count = 1;
        } else if (callback.isFirstAttempt()) {
            callback.setResult(mEnterPinValue.getText().toString());
        }
        dialog.setCancelable(false);
        if (count == 1) {
            dialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // pinDialog = null;
                    dialog.cancel();
                    callback.setResult("");
                    dialog.dismiss();
                    dialog.cancel();
                    mConfirmRequestPinBtn.setEnabled(true);
                }
            });
        }
        if (count == 1) {
            dialog.show();
        }
    }*/
        final Dialog alert = new Dialog(Objects.requireNonNull(getActivity()));
        Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alert.setContentView(R.layout.custom_dialog_wrong_pin);
        final CustomTextView mCountText = alert.findViewById(R.id.count_remaining);
        final CustomButton mOkBtn = alert.findViewById(R.id.ok_btn);

        if (!callback.isFirstAttempt()) {
            mCountText.setText(String.valueOf(callback.attemptsRemaining()));
            count = 1;
        } else if (callback.isFirstAttempt()) {
            callback.setResult(mEnterPinValue.getText().toString());
        }
        alert.setCancelable(false);
        if (count == 1) {
            mOkBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alert.cancel();
                    callback.setResult("");
                    alert.dismiss();
                    alert.cancel();
                    mConfirmRequestPinBtn.setEnabled(true);
                }
            });
        }
        if (count == 1) {
            alert.show();
        }
    }
}