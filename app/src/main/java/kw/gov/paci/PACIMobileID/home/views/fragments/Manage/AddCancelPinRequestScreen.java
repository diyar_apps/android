package kw.gov.paci.PACIMobileID.home.views.fragments.Manage;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.MyIDCancelIDModel.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDCancelIDModel.MyIDCancelID;
import kw.gov.paci.PACIMobileID.home.views.activitys.LoginActivityScreen;
import kw.gov.paci.PACIMobileID.splash.SplashScreen;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomEditTextView;

import com.google.gson.JsonObject;
import com.intercede.Authentication;
import com.intercede.IdentityWallet;
import com.intercede.myIDSecurityLibrary.IPinCallback;
import com.intercede.myIDSecurityLibrary.IReturnUserPin;
import com.intercede.myIDSecurityLibrary.ISigningResult;
import com.intercede.myIDSecurityLibrary.InvalidContextException;
import com.intercede.myIDSecurityLibrary.MyIDSecurityLibrary;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationMechanisms;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationParameters;
import com.intercede.myIDSecurityLibrary.MyIdSignDataResponse;
import com.intercede.myIDSecurityLibrary.MyIdSigningParameters;
import com.intercede.myIDSecurityLibrary.SigningAlgorithm;
import com.intercede.myIDSecurityLibrary.UnlicencedException;

import java.io.File;
import java.security.MessageDigest;
import java.util.Objects;
import java.util.Random;

import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 1/8/2019.
 */
public class AddCancelPinRequestScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public static String mQrData, mStatusScreen;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public RetroApiInterface apiService;
    public String myResponse;
    private String data = null;
    public SharedPreferences preferencese_splash;
    public SharedPreferences.Editor editor_splash;
    public String mDeviceIdValue;
    public LinearLayout mLinearProgressbar;
    public String mCancelMbcvdScreenStatus = "mCancelMbcvdScreenStatus";
    String authorzation_tokentype, mSerialNumber, CivilNo;
    public CustomButton mConfirmBtn;
    private IdentityWallet mWallet;
    public CustomEditTextView mOldPinEd;
    String mConfirmPinStr;
    byte[] sigBytes;
    public String sign;
    MyIDSecurityLibrary myIDSecurityLibrary;
    MyIdSigningParameters signingParameters;
    MyIdAuthenticationParameters authenticationParameters;
    Boolean pinlockedBooleanType;
    public Authentication authentication = null;
    public LinearLayout mPinBlockedRoot;
    public CustomTextView mPinLockedtext;

    public AddCancelPinRequestScreen() {
        // Required empty public constructor
    }

    public static AddCancelPinRequestScreen newInstance(String msg, String mStatus) {
        AddCancelPinRequestScreen fragment = new AddCancelPinRequestScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
            mStatusScreen = mStatus;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_add_cancel_pin_request_screen, container, false);
        mLinearProgressbar = profileBody.findViewById(R.id.progress_bar_ll);
        mConfirmBtn = profileBody.findViewById(R.id.confirm_new_request_pin);
        mOldPinEd = profileBody.findViewById(R.id.new_pin);
        mPinLockedtext = profileBody.findViewById(R.id.pinlocked_bold);
        mPinBlockedRoot = profileBody.findViewById(R.id.blocked_pin_ll);
        mConfirmBtn.setOnClickListener(this);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_new_request_pin:
                initialise(getActivity());
                pinlockedBooleanType = isPinLocked();
                if (!pinlockedBooleanType) {
                    if (mOldPinEd.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                    } else if (mOldPinEd.getText().toString().length() <= 3) {
                        Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                    } else {
                        try {
                            preferences = Objects.requireNonNull(getActivity().getSharedPreferences("plswait", MODE_PRIVATE));
                            editor = preferences.edit();
                            editor.apply();
                            mConfirmPinStr = preferences.getString("mConfirmPinED", "default");
                            if (mOldPinEd.getText().toString().equals(mConfirmPinStr)) {
                                mLinearProgressbar.setVisibility(View.VISIBLE);
                                preferencese_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
                                editor_splash = preferencese_splash.edit();
                                editor_splash.apply();
                                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                                editor = preferences.edit();
                                editor.apply();
                                mDeviceIdValue = preferences.getString(("mCarrierID"), ("default"));
                                if (Utilities.isNetworkAvailable(getActivity())) {
                                    mLinearProgressbar.setVisibility(View.VISIBLE);
                                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                                    if (Utilities.isNetworkAvailable(getActivity())) {
                                        getCivilIDDataList(getActivity(), mDeviceIdValue);
                                    }
                                }
                            } else {
                                // Toast.makeText(getActivity(), R.string.Invalid_PIN, Toast.LENGTH_SHORT).show();
                                try {
                                    byte[] randomBytes = new byte[20];
                                    new Random().nextBytes(randomBytes);
                                    sigBytes = randomBytes;
                                    new SignDataAsyntask(sigBytes).execute();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        } catch (Resources.NotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
        }

    }


    public void setImageResource() {
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialise(getActivity());
        pinlockedBooleanType = isPinLocked();
        if (pinlockedBooleanType) {
            mPinBlockedRoot.setVisibility(View.VISIBLE);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            mPinLockedtext.startAnimation(anim);
        } else {
            mPinBlockedRoot.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            trimCache(getActivity());
            // Toast.makeText(this,"onDestroy " ,Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void trimCache(FragmentActivity addCancelPinRequestScreen) {
        try {
            File dir = Objects.requireNonNull(addCancelPinRequestScreen).getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        // The directory is now empty so delete it
        assert dir != null;
        return dir.delete();
    }

    private void getCivilIDDataList(Activity activity, String mDeviceIdValue) {
        mLinearProgressbar.setVisibility(View.VISIBLE);
        mSerialNumber = preferences.getString("mCarrierID", "default");
        CivilNo = preferences.getString("CivilNo", "default");
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity identity = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity(CivilNo, mSerialNumber);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(CivilNo, "", mSerialNumber);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data);
        Call<JsonObject> call = apiService.getCancelMobileID(authorzation_tokentype, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mLinearProgressbar.setVisibility(View.GONE);
                    assert response.body() != null;
                    myResponse = response.body().toString();
                    if (response.isSuccessful()) {
                        mRemoveIdentity();
                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        editor.putString(("CivilNo"), ("default"));
                        SharedPreferences preferences = getActivity().getSharedPreferences("plswait", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                        Objects.requireNonNull(notificationManager).cancelAll();
                        try {
                            trimCache(getActivity());
                            // Toast.makeText(this,"onDestroy " ,Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        Intent intent = new Intent(getActivity(), SplashScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Objects.requireNonNull(getActivity()).finish();

                    } else {
                        Toast.makeText(getActivity(), "Server Exception", Toast.LENGTH_SHORT).show();
                        mLinearProgressbar.setVisibility(View.GONE);
                        Toast.makeText(getActivity(), String.valueOf(response.errorBody()), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Server Exception", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                mLinearProgressbar.setVisibility(View.GONE);
                //Log.d("onFailure:", t.getLocalizedMessage());
                //Log.d("throw:", t.getMessage());
                // Log.d("throws:", t.toString());
            }
        });
    }

    private void mRemoveIdentity() {
        try {
            mWallet = new IdentityWallet(getActivity());
            mWallet.removeIdentity();
            // Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
        } catch (UnlicencedException e) {
            // removeIdResult.setText(e.getLocalizedMessage());
            Toast.makeText(getActivity(), "Error:" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("StaticFieldLeak")
    public class SignDataAsyntask extends AsyncTask<Void, Void, Void> implements ISigningResult, IPinCallback {
        byte[] mSignBytes;
        int count = 0;
        //public AlertDialog pinDialog;

        public SignDataAsyntask(byte[] sigBytes) {
            this.mSignBytes = sigBytes;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(getActivity()));
                myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
                myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
                myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
                signingParameters = new MyIdSigningParameters();
                signingParameters.algorithm = SigningAlgorithm.SHA256_PKCS15_PKCS7;
                authenticationParameters = new MyIdAuthenticationParameters();
                authenticationParameters.permittedAuthentication = MyIdAuthenticationMechanisms.PIN_ONLY;
                authenticationParameters.pinCallback = this;
                myIDSecurityLibrary.signData(mSignBytes, signingParameters, authenticationParameters, this);
            } catch (InvalidContextException | PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // mLinearProgressbar.setVisibility(View.GONE);

        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            cancel(true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        public void provideUserPin(final IReturnUserPin callback) {
            /*AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            if (!callback.isFirstAttempt()) {
                dialog.setTitle(R.string.Invalid_PIN);
                dialog.setMessage("\n" + callback.attemptsRemaining());
                count = 1;
            } else if (callback.isFirstAttempt()) {
                callback.setResult(mOldPinEd.getText().toString());
            }
            dialog.setCancelable(false);
            if (count == 1) {
                dialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // pinDialog = null;
                        dialog.cancel();
                        callback.setResult("");
                        dialog.dismiss();
                        dialog.cancel();
                        mConfirmBtn.setEnabled(true);
                    }
                });
            }
            if (count == 1) {
                dialog.show();
            }*/
            final Dialog alert = new Dialog(Objects.requireNonNull(getActivity()));
            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alert.setContentView(R.layout.custom_dialog_wrong_pin);
            final CustomTextView mCountText = alert.findViewById(R.id.count_remaining);
            final CustomButton mOkBtn = alert.findViewById(R.id.ok_btn);

            if (!callback.isFirstAttempt()) {
                mCountText.setText(String.valueOf(callback.attemptsRemaining()));
                count = 1;
            } else if (callback.isFirstAttempt()) {
                callback.setResult(mOldPinEd.getText().toString());
            }
            alert.setCancelable(false);
            if (count == 1) {
                mOkBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.cancel();
                        callback.setResult("");
                        alert.dismiss();
                        alert.cancel();
                        mConfirmBtn.setEnabled(true);
                    }
                });
            }
            if (count == 1) {
                alert.show();
            }
        }

        @Override
        public void signingSuccess(MyIdSignDataResponse signDataResponse) {
            if (signDataResponse.signedData == null) {
                // Toast.makeText(getActivity(), "Data Was Not Signed", Toast.LENGTH_SHORT).show();
            } else {

            }
        }

        @Override
        public void signingError(Exception e) {
            Toast.makeText(getActivity(), "Error:" + e, Toast.LENGTH_SHORT).show();
        }
    }

    public void initialise(Context mainContext) {
        try {
            authentication = new Authentication(mainContext);
        } catch (UnlicencedException e) {
            e.printStackTrace();
        }
    }

    public boolean isPinLocked() {
        return authentication.isPinLocked();
    }
}