package kw.gov.paci.PACIMobileID.baseclasses;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Objects;

/**
 * Created by Sunil Kumar on 09/11/18.
 */
public class LocaleChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        if (Objects.requireNonNull(intent.getAction()). compareTo (Intent.ACTION_LOCALE_CHANGED) == 0)
        {
            // Log.v("LocaleChangedRecevier", "received ACTION_LOCALE_CHANGED");
            ((kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication) kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication.getContext()).updateLocale();
        }
    }
}