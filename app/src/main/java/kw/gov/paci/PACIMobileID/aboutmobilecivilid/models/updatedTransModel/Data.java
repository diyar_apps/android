
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.updatedTransModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("TransactionId")
    @Expose
    private String transactionId;
    @SerializedName("MobileCode")
    @Expose
    private String mobileCode;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getMobileCode() {
        return mobileCode;
    }

    public void setMobileCode(String mobileCode) {
        this.mobileCode = mobileCode;
    }

    /**
     * @param transactionId
     * @param mobileCode
     */
    public Data(String transactionId, String mobileCode) {
        super();
        this.transactionId = transactionId;
        this.mobileCode = mobileCode;
    }

}
