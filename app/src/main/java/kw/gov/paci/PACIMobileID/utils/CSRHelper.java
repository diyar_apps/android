package kw.gov.paci.PACIMobileID.utils;

import android.util.Base64;

public class CSRHelper {

    public static String encryptBASE64(byte[] key) {
        return Base64.encodeToString(key, Base64.NO_WRAP);
    }

}
