
package kw.gov.paci.PACIMobileID.home.models.MyIDpostGetAuthIdentityQR;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("CivilNo")
    @Expose
    private String civilNo;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param civilNo
     * @param deviceId
     */
    public Data(String deviceId, String civilNo) {
        super();
        this.deviceId = deviceId;
        this.civilNo = civilNo;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getCivilNo() {
        return civilNo;
    }

    public void setCivilNo(String civilNo) {
        this.civilNo = civilNo;
    }

}
