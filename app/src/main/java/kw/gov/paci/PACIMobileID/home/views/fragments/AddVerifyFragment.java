package kw.gov.paci.PACIMobileID.home.views.fragments;

/**
 * Created by SUNIL KUMAR V on 10/8/2018.
 */

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostGetAuthIdentityQR.Data;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostGetAuthIdentityQR.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostGetAuthIdentityQR.MyIDpostGetAuthIdentityQRModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.google.gson.JsonObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class AddVerifyFragment extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    private BottomNavigationView navigation;
    public CustomTextView mVerifyWithNameData;
    public String mVerifyWithNameDataStr;
    public OnAddHomeInteractionListener mListener;
    protected CustomButton mOkBtn;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public LinearLayout mRootBgImage;
    public String mCountryNameStr;
    public String mNotificationNumberDisplay;
    String authorzation_tokentype, authorzation_tokentype_str;
    String mPostGetAuthIdentityQRStatus = "mPostGetAuthIdentityQRStatus";
    public RetroApiInterface apiService;
    public String mDeviceIdValue, mCivilNo;
    public ImageView mQrCodeImageID;
    public LinearLayout mProgressbarll;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editors;
    private static final String Locale_Preference = "Locale Preference";
    private static final String Locale_KeyValue = "Saved Locale";
    String language, mVerifyWithNameDataArStr;

    public AddVerifyFragment() {
        // Required empty public constructor
    }

    public static AddVerifyFragment newInstance() {
        AddVerifyFragment fragment = new AddVerifyFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_verify_home_body_fragment, container, false);
        mRootBgImage = profileBody.findViewById(R.id.add_verify_home_bg_ll);
        navigation = profileBody.findViewById(R.id.navigation);
        mVerifyWithNameData = profileBody.findViewById(R.id.verify_data_with_name);
        mQrCodeImageID = profileBody.findViewById(R.id.get_qr_code_img);
        mProgressbarll = profileBody.findViewById(R.id.progress_bar_ll);
        mOkBtn = profileBody.findViewById(R.id.ok_btn);
        mOkBtn.setOnClickListener(this);
        getDataFromServer();
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);

        mFullFrame.addView(profileBody);
    }

    @SuppressLint("CommitPrefEdits")
    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
            if (mNotificationNumberDisplay.equals("0")) {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
            } else {
                ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
            }
            mVerifyWithNameDataStr = preferences.getString(("FullNameEn"), "default");
            mVerifyWithNameDataArStr = preferences.getString(("FullNameAr"), "default");
            mCountryNameStr = preferences.getString(("NationalityEn"), "default");
            if (mCountryNameStr.equals("KWT") || mCountryNameStr.equals("Kuwaiti") || mCountryNameStr.equals("Kuwait")) {
                mRootBgImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.screenbg_blue));
            } else {
                mRootBgImage.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.screenbg_liteorange));
            }
            //SetText
            sharedPreferences = getActivity().getSharedPreferences(Locale_Preference, Activity.MODE_PRIVATE);
            editors = sharedPreferences.edit();
            language = sharedPreferences.getString(Locale_KeyValue, "ar");
            if (language.equals("ar")) {
                String text = mVerifyWithNameDataArStr + "\n" + mVerifyWithNameDataStr;
                mVerifyWithNameData.setText(text);
            } else {
                Resources res = getResources();
                @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(res.getString(R.string.Verify_Mobile_Civil_ID_for_verification), mVerifyWithNameDataStr);
                mVerifyWithNameData.setText(text);
            }

            if (Utilities.isNetworkAvailable(getActivity())) {
                apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                if (Utilities.isNetworkAvailable(getActivity())) {
                    preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                    editor = preferences.edit();
                    editor.apply();
                    mDeviceIdValue = preferences.getString("mCarrierID", "default");
                    mCivilNo = preferences.getString("CivilNo", "default");
                    authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
                    mPostGetAuthIdentityQRStatusMethod(authorzation_tokentype, mDeviceIdValue, mCivilNo);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ok_btn:
                setFragment(R.id.root_content, AddHomeFragment.newInstance(), AddHomeFragment.class.getSimpleName());
                break;
        }
    }

    public void setImageResource() {

    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        fragmentTransaction.commit();
    }

    private void mPostGetAuthIdentityQRStatusMethod(String authorzationTokentype, String mDeviceIdValue, String mCivilNo) {
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Identity identity = new Identity(mDeviceIdValue, mCivilNo);
        Data data = new Data(mDeviceIdValue, mCivilNo);
        MyIDpostGetAuthIdentityQRModel myIDpostGetAuthIdentityQRModel = new MyIDpostGetAuthIdentityQRModel(identity, data);
        Call<JsonObject> call = apiService.postGetAuthIdentityQR(authorzationTokentype, myIDpostGetAuthIdentityQRModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mProgressbarll.setVisibility(View.GONE);
                    if (response.isSuccessful()) {
                        String mGetQRImage = ((JsonObject) response.body()).get("Data").toString().replace("\"", "");
                        byte[] decodedString = Base64.decode(mGetQRImage, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        mQrCodeImageID.setImageBitmap(decodedByte);
                        mQrCodeImageID.setVisibility(View.VISIBLE);
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.failed_txt), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure:", t.getLocalizedMessage());
                //Log.d("throw:", t.getMessage());
                // Log.d("throws:", t.toString());
            }
        });
    }
}