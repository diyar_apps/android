
package kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("RequestId")
    @Expose
    private String requestId;
    @SerializedName("PKCS7")
    @Expose
    private String pKCS7;
    @SerializedName("StatusId")
    @Expose
    private String statusId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param statusId
     * @param pKCS7
     * @param requestId
     * @param deviceId
     */
    public Data(String deviceId, String requestId, String pKCS7, String statusId) {
        super();
        this.deviceId = deviceId;
        this.requestId = requestId;
        this.pKCS7 = pKCS7;
        this.statusId = statusId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getPKCS7() {
        return pKCS7;
    }

    public void setPKCS7(String pKCS7) {
        this.pKCS7 = pKCS7;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

}
