
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.updatedTransModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDUpdateTransactionProgressModel {

    @SerializedName("Data")
    @Expose
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    /**
     * @param data
     */
    public MyIDUpdateTransactionProgressModel(Data data) {
        super();
        this.data = data;
    }
}
