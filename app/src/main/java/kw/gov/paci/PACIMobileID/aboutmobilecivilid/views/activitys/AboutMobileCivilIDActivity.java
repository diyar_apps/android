package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AboutMobileCivilIDHome;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AddConfirmPin;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AddGetMobileID;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AddPinCollectedSuccessfully;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AddPreRequisites;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.AddProcedure;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments.PleaseWaitCivilID;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilID;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import kw.gov.paci.PACIMobileID.R;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Locale;
import java.util.Objects;

public class AboutMobileCivilIDActivity extends MobileCivilID implements OnAboutMobileCivilIDInteractionListener {
    String message, mCanceledMobileID, mCivilIdStatus;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public RelativeLayout mRootViewBadge;
    Locale locale;
    private CustomTextView mHeaderCenterText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_root_view);
        super.onCreate(savedInstanceState);
        mRootViewBadge = findViewById(R.id.badge_rootview);
        mHeaderCenterText = findViewById(R.id.header_life_coach_txt);
        mHeaderCenterText.setText(R.string.My_Civil_ID);
        mRootViewBadge.setVisibility(View.INVISIBLE);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mCanceledMobileID = bundle.getString("CancelID");
            mCivilIdStatus = bundle.getString("CivilNo");
            if (mCanceledMobileID != null && mCanceledMobileID.equals("CancelCivilID")) {
                preferences = Objects.requireNonNull(AboutMobileCivilIDActivity.this).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                editor.putString(("CivilNo"), ("default"));
                locale = new Locale("ar");
                Locale.setDefault(locale);
                config.locale = locale;
                this.createConfigurationContext(config);
                this.getResources().updateConfiguration(config, this.getResources().getDisplayMetrics());
                enForcedLocale();
                navigationView.getMenu().findItem(R.id.nav_home).setVisible(false);
                navigationView.getMenu().findItem(R.id.nav_verify_identity).setVisible(false);
                //navigationView.getMenu().findItem(R.id.nav_logout).setVisible(false);
            } else {
                navigationView.getMenu().findItem(R.id.nav_home).setVisible(true);
                navigationView.getMenu().findItem(R.id.nav_verify_identity).setVisible(true);
               // navigationView.getMenu().findItem(R.id.nav_logout).setVisible(true);
            }
        }
        setFragment(R.id.root_content, AboutMobileCivilIDHome.newInstance(), AboutMobileCivilIDHome.class.getSimpleName());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            goToPreviousScreen();
        }
    }

    @Override
    public void onBackButtonClicked() {
        goToPreviousScreen();
    }

    @Override
    public void goToNextScreen() {

    }

    @Override
    public void goToPreRequisitesScreen() {
        replaceFragment(R.id.root_content, AddPreRequisites.newInstance(),
                AddPreRequisites.class.getSimpleName(), true);

    }

    @Override
    public void goToProcedureScreen() {
        replaceFragment(R.id.root_content, AddProcedure.newInstance(),
                AddProcedure.class.getSimpleName(), true);
    }

    @Override
    public void goToGetMobileIDScreen() {
        setFragment(R.id.root_content, AddGetMobileID.newInstance(),
                AddGetMobileID.class.getSimpleName());
    }

    @Override
    public void goToPleaseWaitCivilIDScreen() {
        setFragment(R.id.root_content, PleaseWaitCivilID.newInstance(message),
                PleaseWaitCivilID.class.getSimpleName());
    }

    @Override
    public void goToConfirmPinScreen() {
        setFragment(R.id.root_content, AddConfirmPin.newInstance("test"),
                AddConfirmPin.class.getSimpleName());

    }

    @Override
    public void goToAddPinCollectedSuccessfully() {
        setFragment(R.id.root_content, AddPinCollectedSuccessfully.newInstance(),
                AddPinCollectedSuccessfully.class.getSimpleName());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //Log.d("onActivityResult", "onActivityResult: .");
        if (resultCode == Activity.RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanResult.getContents() != null) {
                String re = scanResult.getContents();
                message = re;
                if (message.contains("PACMD-")) {
                    PleaseWaitCivilID argumentFragment = new PleaseWaitCivilID();//Get Fragment Instance
                    Bundle data = new Bundle();//Use bundle to pass data
                    data.putString("edttext", message);//put string, int, etc in bundle with a key value
                    argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                    replaceFragment(R.id.root_content, PleaseWaitCivilID.newInstance(message),
                            PleaseWaitCivilID.class.getSimpleName(), true);//now replace the argument fragment
                } else {
                    Toast.makeText(AboutMobileCivilIDActivity.this, R.string.invalid_qr_code, Toast.LENGTH_SHORT).show();
                    setFragment(R.id.root_content, AddGetMobileID.newInstance(),
                            AddGetMobileID.class.getSimpleName());
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        enForcedLocale();
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }
}
