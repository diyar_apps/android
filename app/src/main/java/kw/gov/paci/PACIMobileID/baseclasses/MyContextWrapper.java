package kw.gov.paci.PACIMobileID.baseclasses;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Build;
import android.text.TextUtils;

import java.util.Locale;

public class MyContextWrapper extends ContextWrapper {

    public MyContextWrapper(Context base) {
        super(base);
    }
    public static Context wrap(Context context, Locale locale) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1 || null == locale) {
            return context;
        }
        String language = locale.getLanguage();
        if (TextUtils.isEmpty(language)) {
            return context;
        }
        Configuration config = context.getResources().getConfiguration();
        Locale.setDefault(locale);
        config.setLocale(locale);
        return context.createConfigurationContext(config);
    }

}
