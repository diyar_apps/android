package kw.gov.paci.PACIMobileID.baseclasses;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import kw.gov.paci.PACIMobileID.BuildConfig;
/**
 * Created by Sunil Kumar on 09/11/18.
 */

public class LocaleBaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void attachBaseContext(Context newBase) {
        super.attachBaseContext(kw.gov.paci.PACIMobileID.baseclasses.MyContextWrapper.wrap(newBase, BuildConfig.LOCALE));
    }
    /*android:excludeFromRecents="true"*/
}