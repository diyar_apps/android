
package kw.gov.paci.PACIMobileID.home.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetAuthRequestDetailsModel {

    @SerializedName("Error")
    @Expose
    private Error error;
    @SerializedName("Data")
    @Expose
    private Data data;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
