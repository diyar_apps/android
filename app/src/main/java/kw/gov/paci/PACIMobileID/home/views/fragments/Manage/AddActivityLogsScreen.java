package kw.gov.paci.PACIMobileID.home.views.fragments.Manage;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.AddActivityLogAdapter;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.home.views.fragments.Authentication.HeadlessUnlockPinCallbackHandler;
import com.fxn.stash.Stash;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 1/8/2019.
 */
public class AddActivityLogsScreen extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public RetroApiInterface apiService;
    public SharedPreferences.Editor editor_splash;
    private RecyclerView recyclerView;

    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";
    String mNotificationNumberDisplay;
    static HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;
    Boolean pinlockedBooleanType;


    public AddActivityLogsScreen() {
        // Required empty public constructor
    }

    public static AddActivityLogsScreen newInstance() {
        AddActivityLogsScreen fragment = new AddActivityLogsScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_add_activity_logs_screen, container, false);
        recyclerView = profileBody.findViewById(R.id.recycler_view_activity);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        getAddLogsListActivity();
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        mNotificationNumberDisplay = preferences.getString(("NotificationsCount"), "0");
        if (mNotificationNumberDisplay.equals("0")) {
            ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(null);
        } else {
            ((AddHomeActivity) Objects.requireNonNull(getActivity())).mNotifiBadgeCount.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.badge_item_count_bell_icon));
        }
        mFullFrame.addView(profileBody);
    }

    private void getAddLogsListActivity() {

        headlessUnlockPinCallbackHandler = new HeadlessUnlockPinCallbackHandler(getActivity());
        headlessUnlockPinCallbackHandler.initialise(getActivity());
        pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        if (pinlockedBooleanType) {
            Date today = new Date();
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
            String dateToStr = format.format(today);
            System.out.println(dateToStr);
            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.pin_locked_activity_history), "", getString(R.string.Your_Pin_locked_Successfully_activity_history), dateToStr));
            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
        }

        Date today = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
        String dateToStr = format.format(today);
        System.out.println(dateToStr);

        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();

        ArrayList<AddActivityLogModel> userArrayListNew = Stash.getArrayList(ACTIVITY_LOGS, AddActivityLogModel.class);

        AddHomeActivity.adapter = new AddActivityLogAdapter(userArrayListNew, getActivity());
        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(eLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(AddHomeActivity.adapter);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_btn:
                break;
            case R.id.cancel_btn:
                setFragment(R.id.root_content, AddManageScreen.newInstance(), AddManageScreen.class.getSimpleName());
                break;
        }
    }

    public void setImageResource() {
    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        try {
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(fragmentContainer, fragment, id);
            //fragmentTransaction.commit();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        super.onConfigurationChanged(newConfig);
    }
    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }
}