package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.zxing.integration.android.IntentIntegrator;

import java.util.Locale;
import java.util.Objects;
import java.util.Random;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.ScannerActivity;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import static android.content.Context.MODE_PRIVATE;

public class AddGetMobileID extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    private OnAboutMobileCivilIDInteractionListener mListener;
    public ImageView header_Mobile_img;
    public CustomTextView header_Mobile_txt, body_Mobile_txt, header_Mobile_txt_verify;
    public String generatedRandomkey;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    private Random random;

    public AddGetMobileID() {
        // Required empty public constructor
    }

    public static AddGetMobileID newInstance() {
        AddGetMobileID fragment = new AddGetMobileID();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAboutMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.about_mobile_civilid_home_header, container, false);
        header_Mobile_img = header.findViewById(R.id.about_civil_header_img);
        header_Mobile_txt_verify = header.findViewById(R.id.header_about_civil_id_header_txt);
        header_Mobile_txt_verify.setVisibility(View.VISIBLE);
        //CharSequence text = String.format(getActivity().getResources().getString(R.string.Verify_Mobile_Civil_ID_for), generatedRandomkey);
        header_Mobile_img.setVisibility(View.GONE);
        mHeader.addView(header);
        mHeader.setVisibility(View.VISIBLE);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.add_get_mobile_id_qr_code, container, false);
        header_Mobile_txt = profileBody.findViewById(R.id.header_about_civil_id_header_txt);
        body_Mobile_txt = profileBody.findViewById(R.id.about_civil_id_body_txt);
        header_Mobile_txt.setVisibility(View.GONE);
        body_Mobile_txt.setVisibility(View.GONE);
        mQRCodeImg.setVisibility(View.VISIBLE);
        mGetMobileIDText.setVisibility(View.VISIBLE);
        mFullFrame.setVisibility(View.VISIBLE);
        random = new Random();
        setRandomKeyGenerate(random);
        //mGetMobileIDText.setText("GenerateID:"+generatedRandomkey);
        mQRCodeImg.setOnClickListener(this);
        setImageResource();
        mBody.setVisibility(View.GONE);
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.INVISIBLE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.VISIBLE);
        mInfoButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.info_btn:
                setFragment(R.id.root_content, AboutMobileCivilIDHome.newInstance(), AboutMobileCivilIDHome.class.getSimpleName());
                break;
            case R.id.qr_code_civil_id:
                IntentIntegrator integrator = new IntentIntegrator(getActivity());
                integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(true);
                integrator.setOrientationLocked(false);
                integrator.setPrompt("Scan a Qr code");
                integrator.addExtra("AddGetMobileIDScreen", "AddGetMobileIDScreenID");
                integrator.setCaptureActivity(ScannerActivity.class);
                integrator.initiateScan();
                break;
        }
    }

    public void setImageResource() {

    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(fragmentContainer, fragment, id);
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void setRandomKeyGenerate(Random random) {
        try {
            Resources res = getResources();
            // generatedRandomkey = String.format(Locale.ENGLISH, "%04d", random.nextInt(10000));
            generatedRandomkey = String.format(Locale.ENGLISH, "%04d", random.nextInt(10000));
            generatedRandomkey = generatedRandomkey.replaceAll("0", "1");
            @SuppressLint({"StringFormatInvalid", "LocalSuppress"}) String text = String.format(Locale.ENGLISH, res.getString(R.string.Verify_Mobile_Civil_ID_for), generatedRandomkey);
            header_Mobile_txt_verify.setText(text);
            // Write
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("randomkey", MODE_PRIVATE);
            editor = preferences.edit();
            editor.putString(("RkeyValue"), (generatedRandomkey));
            editor.apply(); // Or commit if targeting old devices
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }
}



