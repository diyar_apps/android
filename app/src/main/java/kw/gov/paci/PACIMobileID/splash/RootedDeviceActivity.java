package kw.gov.paci.PACIMobileID.splash;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import kw.gov.paci.PACIMobileID.R;

public class RootedDeviceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mroot_detection_activity);
        Window window = this.getWindow();
        window.setStatusBarColor(this.getResources().getColor(R.color.colorBlack));
    }

    public void onBackPressed() {
        //  super.onBackPressed();
        moveTaskToBack(true);

    }
}
