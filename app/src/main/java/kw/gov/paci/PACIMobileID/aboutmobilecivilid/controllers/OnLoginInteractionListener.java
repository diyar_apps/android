package kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers;

/**
 * Created by SUNIL KUMAR V on 9/29/2018.
 */
public interface OnLoginInteractionListener{
    //void goToForgotPassCodeScreen();
    void onBackPressed();
    void setAppPassCode();
}
