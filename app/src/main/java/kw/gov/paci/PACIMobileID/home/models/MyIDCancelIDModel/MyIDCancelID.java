
package kw.gov.paci.PACIMobileID.home.models.MyIDCancelIDModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDCancelID {

    @SerializedName("Identity")
    @Expose
    private Identity identity;
   /* @SerializedName("Data")
    @Expose
    private String data;*/

    /**
     * No args constructor for use in serialization
     *
     */
    public MyIDCancelID() {
    }

        public MyIDCancelID(Identity identity) {
        super();
        this.identity = identity;
       // this.data = data;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

   /* public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }*/

}
