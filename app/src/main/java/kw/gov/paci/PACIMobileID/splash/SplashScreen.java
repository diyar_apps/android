package kw.gov.paci.PACIMobileID.splash;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;

import kw.gov.paci.PACIMobileID.BuildConfig;
import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.MyIDGetRqdtsModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.activitys.AboutMobileCivilIDActivity;
import kw.gov.paci.PACIMobileID.baseclasses.BaseTokenServiceCallBack;
import kw.gov.paci.PACIMobileID.baseclasses.LocaleBaseActivity;
import kw.gov.paci.PACIMobileID.home.views.activitys.LoginActivityScreen;
import kw.gov.paci.PACIMobileID.splash.models.postGtpvsn.Data;
import kw.gov.paci.PACIMobileID.splash.models.postGtpvsn.MyIDPostGtpvsnModel;
import kw.gov.paci.PACIMobileID.utils.Constants;
import kw.gov.paci.PACIMobileID.utils.CustomDialog;
import kw.gov.paci.PACIMobileID.utils.CustomDialogAppVersion;
import kw.gov.paci.PACIMobileID.utils.Utilities;

import com.google.gson.JsonObject;
import com.intercede.IdentityWallet;
import com.intercede.myIDSecurityLibrary.UnlicencedException;
import com.scottyab.rootbeer.RootBeer;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.Objects;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreen extends LocaleBaseActivity {
    public static int SPLASH_TIME_OUT = 2000;
    public String android_id;
    public SharedPreferences preferences, preferences_pls_wait;
    public SharedPreferences.Editor editor, editor_pls_wait;
    private Intent intent;
    public String mCivilID, mConfirmPinStr;
    private RetroApiInterface apiService;
    public String mLoginScreenStatus = "mLoginScreenStatus";
    String authorzation_tokentype;
    private String data = null;
    public JSONObject jObject;
    public String device_ID, NotificationDevice_UniqueId, Civil_No, Valid_From, Valid_To;
    public int Id, Identity_Type, Device_OS, Language_ID, Status_ID, Notifications_Count;
    private String mdeviceID;
    String versionName = "";
    int versionCode = -1;
    public String mAppVersionName, mServiceAppVer, mInternalVerName;
    public Boolean mIsRootedstatusDevice = false;
    public String mDeviceIdValue, mSerialNumber, CivilNo;
    public String myResponse;
    private IdentityWallet mWallet;

    @SuppressLint("HardwareIds")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android_id = Secure.getString(this.getContentResolver(),
                Secure.ANDROID_ID);
        Fabric.with(this, new Crashlytics());
        Fabric.with(this, new Answers());
        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        // Write
        preferences = getSharedPreferences("splash_pref", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply(); // Or commit if targeting old devices

        preferences_pls_wait = Objects.requireNonNull(SplashScreen.this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor_pls_wait = preferences_pls_wait.edit();
        editor_pls_wait.apply();

        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.content_splash_screen);

        mGetDeviceRootedStatus();

        mCivilID = preferences_pls_wait.getString("CivilNo", "default");
        mConfirmPinStr = preferences_pls_wait.getString("mConfirmPinED", "default");

        try {
            getVersionInfo();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    if (Utilities.isNetworkAvailable(SplashScreen.this)) {
                        getAppVersionInfo();
                    }
                }
            }, 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void mGetDeviceRootedStatus() {
        try {
            RootBeer rootBeer = new RootBeer(SplashScreen.this);
            if (rootBeer.isRooted()) {
                //we found indication of root
                mIsRootedstatusDevice = rootBeer.isRooted();
                apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                if (Utilities.isNetworkAvailable(SplashScreen.this)) {
                    mDeviceIdValue = preferences_pls_wait.getString(("mCarrierID"), ("default"));
                    getCivilIDDataList(SplashScreen.this, mDeviceIdValue);
                }
                //Toast.makeText(SplashScreen.this, "The app cannot run on this device", Toast.LENGTH_SHORT).show();
                /*Intent intent = new Intent(getApplicationContext(), RootedDeviceActivity.class);
                startActivity(intent);
                SplashScreen.this.finish();*/
            } else {
                mIsRootedstatusDevice = rootBeer.isRooted();
                //we didn't find indication of root
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getAppVersionInfo() {
        Data data = new Data(Constants.ANDROID_APP_VERSION);
        MyIDPostGtpvsnModel myIDPostGtpvsnModel = new MyIDPostGtpvsnModel(data);
        Call<JsonObject> call = apiService.postGtpvsn(BuildConfig.mBasicAuthorization, myIDPostGtpvsnModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    String dataRes = response.body().toString();
                    if (dataRes != null) {
                        mServiceAppVer = response.body().get("Data").toString().replace("\"", "");
                        editor_pls_wait.putString("AppVersion", mServiceAppVer);
                        editor_pls_wait.apply();
                       /* if (!mServiceAppVer.equals(mAppVersionName)) {
                            CustomDialogAppVersion customDialogAppVersion = new CustomDialogAppVersion(SplashScreen.this);
                            Objects.requireNonNull(customDialogAppVersion.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                            customDialogAppVersion.show();
                        } else {
                            if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
                                SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.clear();
                                editor.apply();
                                new mInitialScreenHandler().invoke();
                            } else {
                                new mLoginScreenHandler().invoke();
                            }
                        }*/
                        mInternalVerName = preferences_pls_wait.getString("internalVersionName", "0.0.0");
                        compareVersionNames(mServiceAppVer, mInternalVerName);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mInternalVerName = preferences_pls_wait.getString("internalVersionName", "0.0.0");
                    compareVersionNames(mServiceAppVer, mInternalVerName);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(SplashScreen.this, "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SplashScreen.this, "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private class mInitialScreenHandler {
        void invoke() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    intent = new Intent(SplashScreen.this, AboutMobileCivilIDActivity.class);
                    startActivity(intent);
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private class mLoginScreenHandler {
        void invoke() {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                   /* Intent intent = new Intent(SplashScreen.this, LoginActivityScreen.class);
                    startActivity(intent);
                    SplashScreen.this.finish();*/
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    getDeviceStatus();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    private void getDeviceStatus() {
        if (Utilities.isNetworkAvailable(SplashScreen.this)) {
           /* tokenIdentity = new TokenIdentity(SplashScreen.this);
            tokenIdentity.mGetAccessTokenRequest(mLoginScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);*/
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            mdeviceID = preferences_pls_wait.getString("mCarrierID", "default");
            if (Utilities.isNetworkAvailable(SplashScreen.this)) {
                getDeviceDetails(mdeviceID);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void getDeviceDetails(String deviceID) {
        Identity identity = new Identity(mCivilID, deviceID);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data1 = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(mCivilID, "", deviceID);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data1);
        Call<JsonObject> call = apiService.getdvst(BuildConfig.mBasicAuthorization, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    data = response.body().toString();
                    if (data != null) {
                        jObject = new JSONObject(data);
                        if (jObject.isNull("Data")) {
                            SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            new mInitialScreenHandler().invoke();
                        } else {
                            try {
                                jObject = new JSONObject(data);
                                JSONObject objectData = jObject.getJSONObject("Data");
                                Status_ID = objectData.getInt("Status");
                                if (Status_ID == 2) {
                                    Id = objectData.getInt("Id");
                                    device_ID = objectData.get("DeviceID").toString();
                                    Device_OS = objectData.getInt("DeviceOS");
                                    Language_ID = objectData.getInt("LanguageID");
                                    NotificationDevice_UniqueId = objectData.get("NotificationDeviceUniqueId").toString();
                                    // mAppVersionName=objectData.get("AppVersion").toString();
                                    Civil_No = objectData.get("CivilNo").toString();
                                    Valid_From = objectData.get("ValidFrom").toString();
                                    Valid_To = objectData.get("ValidTo").toString();
                                    Identity_Type = objectData.getInt("IdentityType");
                                    Notifications_Count = objectData.getInt("NotificationsCount");
                                    editor.putString(("Id"), (Integer.toString(Id)));
                                    editor.putString(("mCarrierID"), (device_ID));
                                    editor.putString(("DeviceOS"), (Integer.toString(Device_OS)));
                                    editor.putString(("LanguageID"), (Integer.toString(Language_ID)));
                                    editor.putString(("NotificationDeviceUniqueId"), (NotificationDevice_UniqueId));
                                    editor.putString(("CivilNo"), (Civil_No));
                                    editor.putString(("ValidFrom"), (Valid_From));
                                    editor.putString(("ValidTo"), (Valid_To));
                                    editor.putString(("Status"), (Integer.toString(Status_ID)));
                                    editor.putString(("NotificationsCount"), (Integer.toString(Notifications_Count)));
                                    editor.apply();
                                    Intent intent = new Intent(getApplicationContext(), LoginActivityScreen.class);
                                    startActivity(intent);
                                    SplashScreen.this.finish();
                                } else {
                                    SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.clear();
                                    editor.apply();
                                    new mInitialScreenHandler().invoke();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear();
                        editor.apply();
                        new mInitialScreenHandler().invoke();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(SplashScreen.this, "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(SplashScreen.this, "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //get the current version number and name
    private void getVersionInfo() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            editor_pls_wait.putString("internalVersionName", versionName);
            editor_pls_wait.apply();
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void compareVersionNames(String oldVersionName, String newVersionName) {

        String[] oldNumbers = oldVersionName.split("\\.");
        String[] newNumbers = newVersionName.split("\\.");

        // To avoid IndexOutOfBounds
        int maxIndex = Math.min(oldNumbers.length, newNumbers.length);

        for (int i = 0; i < maxIndex; i++) {
            int oldVersionPart = Integer.valueOf(oldNumbers[i]);
            int newVersionPart = Integer.valueOf(newNumbers[i]);

            if (oldVersionPart < newVersionPart) {
                //  res = -1;
                // Toast.makeText(SplashScreen.this, "new version", Toast.LENGTH_LONG).show();
                if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
                    SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    new mInitialScreenHandler().invoke();
                } else {
                    new mLoginScreenHandler().invoke();
                }
                break;
            } else if (oldVersionPart > newVersionPart) {
                // res = 1;
                // Toast.makeText(SplashScreen.this, "old version", Toast.LENGTH_LONG).show();
                CustomDialogAppVersion customDialogAppVersion = new CustomDialogAppVersion(SplashScreen.this);
                Objects.requireNonNull(customDialogAppVersion.getWindow()).setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                customDialogAppVersion.show();
                break;
            } else {
                if (mCivilID.equals("default") || mConfirmPinStr.equals("default")) {
                    SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear();
                    editor.apply();
                    new mInitialScreenHandler().invoke();
                } else {
                    new mLoginScreenHandler().invoke();
                }
                break;
            }
        }

    }

    private void getCivilIDDataList(Activity activity, String mDeviceIdValue) {
        mSerialNumber = preferences_pls_wait.getString("mCarrierID", "default");
        CivilNo = preferences_pls_wait.getString("CivilNo", "default");
        authorzation_tokentype = preferences_pls_wait.getString("authorzation_tokentype", "default");
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity identity = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Identity(CivilNo, mSerialNumber);
        kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data data = new kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDGetRQDTS.Data(CivilNo, "", mSerialNumber);
        MyIDGetRqdtsModel myIDGetRqdtsModel = new MyIDGetRqdtsModel(identity, data);
        Call<JsonObject> call = apiService.getCancelMobileID(authorzation_tokentype, myIDGetRqdtsModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    assert response.body() != null;
                    myResponse = response.body().toString();
                    if (response.isSuccessful()) {
                      mRootCompleteDataClear();
                    } else {
                        //Toast.makeText(getActivity(), "Server Exception", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getActivity(), String.valueOf(response.errorBody()), Toast.LENGTH_SHORT).show();
                        mRootCompleteDataClear();
                    }
                } catch (Exception e) {
                    //Toast.makeText(getActivity(), "Server Exception", Toast.LENGTH_SHORT).show();
                    mRootCompleteDataClear();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure:", t.getLocalizedMessage());
                //Log.d("throw:", t.getMessage());
                // Log.d("throws:", t.toString());
                mRootCompleteDataClear();
            }
        });
    }

    private void mRootCompleteDataClear() {
        mRemoveIdentity();
        preferences = Objects.requireNonNull(SplashScreen.this).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        editor.putString(("CivilNo"), ("default"));
        SharedPreferences preferences = SplashScreen.this.getSharedPreferences("plswait", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
        NotificationManager notificationManager = (NotificationManager) SplashScreen.this.getSystemService(Context.NOTIFICATION_SERVICE);
        Objects.requireNonNull(notificationManager).cancelAll();

        Intent intent = new Intent(SplashScreen.this, RootedDeviceActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Objects.requireNonNull(SplashScreen.this).finish();
    }

    private void mRemoveIdentity() {
        try {
            mWallet = new IdentityWallet(SplashScreen.this);
            mWallet.removeIdentity();
            // Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
        } catch (UnlicencedException e) {
            // removeIdResult.setText(e.getLocalizedMessage());
            Toast.makeText(SplashScreen.this, "Error:" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
