
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Certificate {

    @SerializedName("Certificate")
    @Expose
    private String certificate;

    public Certificate(String certificate) {
        this.certificate=certificate;

    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

}
