package kw.gov.paci.PACIMobileID.mobileidverifier.controllers;

/**
 * Created by SUNIL KUMAR V on 1/28/2019.
 */
public interface OnMobileIDVerifyInteractionListener {
    void goToAddMIDHomeScanner();
}
