package kw.gov.paci.PACIMobileID.aboutmobilecivilid.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.OnAboutMobileCivilIDInteractionListener;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class AddProcedure extends MobileCivilIDBaseFragment implements View.OnClickListener {
    public static final String IS_EDIT = "type";
    private OnAboutMobileCivilIDInteractionListener mListener;
    public ImageView header_Mobile_img;
    public CustomTextView header_Mobile_txt, body_Mobile_txt, body_Mobile_txt_two;
    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;
    public String mStatus;
    public static Boolean mStatusLogin;
    //public String mCivilNo;
    public String AddConfirmPinScreenSuccess;

    public AddProcedure() {
        // Required empty public constructor
    }

    public static AddProcedure newInstance() {
        AddProcedure fragment = new AddProcedure();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = super.onCreateView(inflater, container, savedInstanceState);
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAboutMobileCivilIDInteractionListener) {
            mListener = (OnAboutMobileCivilIDInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAboutMobileCivilIDInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.about_mobile_civilid_home_header, container, false);
        header_Mobile_img = header.findViewById(R.id.about_civil_header_img);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.pre_requisites_home_body, container, false);
        header_Mobile_txt = profileBody.findViewById(R.id.header_about_civil_id_header_txt);
        body_Mobile_txt = profileBody.findViewById(R.id.about_civil_id_body_txt);
        body_Mobile_txt_two = profileBody.findViewById(R.id.about_civil_id_body_two);
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        AddConfirmPinScreenSuccess = preferences.getString("AddConfirmPinScreenSuccess", "default");
        setImageResource();
        mBody.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.VISIBLE);
        mNextButton.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.button_bg_rounded_corners));
        mPreviousButton.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getActivity()), R.drawable.button_bg_rounded_corners));
        if (null != mNextButton) {
            mNextButton.setOnClickListener(this);
        }
        if (null != mPreviousButton) {
            mPreviousButton.setOnClickListener(this);
        }
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        AddConfirmPinScreenSuccess = preferences.getString(("AddConfirmPinScreenSuccess"), ("default"));
        if (AddConfirmPinScreenSuccess.equals("default")) {
            mNextButton.setText(R.string.Next);
        } else {
            mNextButton.setText(R.string.Home);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next_btn:
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                AddConfirmPinScreenSuccess = preferences.getString(("AddConfirmPinScreenSuccess"), ("default"));
                if (AddConfirmPinScreenSuccess.equals("default")) {
                    mListener.goToGetMobileIDScreen();
                } else {
                    Intent intent = new Intent(getActivity(), AddHomeActivity.class);
                    startActivity(intent);
                    Objects.requireNonNull(getActivity()).finish();
                }
                break;
            case R.id.previous_btn:
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
                break;
        }
    }

    public void setImageResource() {
        header_Mobile_img.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.procedure));
        header_Mobile_txt.setText(R.string.screenThree_content_one);
        body_Mobile_txt.setText(R.string.screenThree_content_two);
        body_Mobile_txt_two.setText(R.string.screenThree_content_three);
    }
}



