package kw.gov.paci.PACIMobileID.home.views.fragments.Authentication;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.LinearLayout;
import android.widget.Toast;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.controllers.RetroApiInterface;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.AddActivityLogModel;
import kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.ApiClientURL;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDBaseFragment;
import kw.gov.paci.PACIMobileID.home.controllers.OnAddHomeInteractionListener;
import kw.gov.paci.PACIMobileID.home.models.GetAuthRequestDetailsModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.MyIDGetAuthRequestModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDIntializeUnlockPinModel.Data;
import kw.gov.paci.PACIMobileID.home.models.MyIDIntializeUnlockPinModel.Identity;
import kw.gov.paci.PACIMobileID.home.models.MyIDIntializeUnlockPinModel.MyIDInitialPinUnlockModel;
import kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.MyIDpostAuthenticateRequestModel;
import kw.gov.paci.PACIMobileID.home.views.activitys.AddHomeActivity;
import kw.gov.paci.PACIMobileID.home.views.activitys.LoginActivityScreen;
import kw.gov.paci.PACIMobileID.home.views.fragments.Manage.AddManageScreen;
import kw.gov.paci.PACIMobileID.utils.CSRHelper;
import kw.gov.paci.PACIMobileID.utils.Utilities;
import kw.gov.paci.PACIMobileID.utils.views.CustomButton;
import kw.gov.paci.PACIMobileID.utils.views.CustomEditTextView;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;

import com.fxn.stash.Stash;
import com.google.gson.JsonObject;
import com.intercede.Authentication;
import com.intercede.IReturnChallengeResponseAndUserPin;
import com.intercede.OfflineUnlockCallback;
import com.intercede.UnlockPinRequest;
import com.intercede.myIDSecurityLibrary.IPinCallback;
import com.intercede.myIDSecurityLibrary.IReturnUserPin;
import com.intercede.myIDSecurityLibrary.ISigningResult;
import com.intercede.myIDSecurityLibrary.InvalidContextException;
import com.intercede.myIDSecurityLibrary.MyIDSecurityLibrary;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationMechanisms;
import com.intercede.myIDSecurityLibrary.MyIdAuthenticationParameters;
import com.intercede.myIDSecurityLibrary.MyIdSignDataResponse;
import com.intercede.myIDSecurityLibrary.MyIdSigningParameters;
import com.intercede.myIDSecurityLibrary.SigningAlgorithm;
import com.intercede.myIDSecurityLibrary.UnlicencedException;

import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SUNIL KUMAR V on 10/15/2018.
 */
public class AddPinRequestScreen extends MobileCivilIDBaseFragment implements View.OnClickListener, OfflineUnlockCallback {
    public static final String IS_EDIT = "type";
    public OnAddHomeInteractionListener mListener;
    public static String mQrData, mStatusScreen;
    public CustomTextView mPlsEnterHeadertxt, mPinMustBetxt, mItisHighlytxt, pinlockedBold, pinlocked;
    public LinearLayout mOldPinViewLL, mConfirmPinViewLL, mEnterPinViewLL;
    public CustomButton mConfirmRequestPinBtn;
    public SharedPreferences preferences, preferencese_splash;
    public SharedPreferences.Editor editor, editor_splash;
    public String mConfirmPinStr;
    public CustomEditTextView mEnterPinValue;
    public RetroApiInterface apiService;
    private String data = null;
    public GetAuthRequestDetailsModel myResponse;
    public String mDeviceIdValue, mRequestIdvalue, mPKCS7Value, mStatusIdValue;
    public String sign;
    public String mChallangeID, mTransID;
    public CustomEditTextView mConfirmPInED;
    public String mSdkChallenge;
    byte[] sigBytes;
    public LinearLayout mProgressbarll/* mPinUnlockedLL*/;
    MyIDSecurityLibrary myIDSecurityLibrary;
    MyIdSigningParameters signingParameters;
    MyIdAuthenticationParameters authenticationParameters;
    public Authentication authentication = null;
    private LocalBroadcastManager localBroadcastManager;
    BroadcastReceiver receiver;
    @SuppressLint("StaticFieldLeak")
    //static HeadlessUnlockPinCallbackHandler headlessUnlockPinCallbackHandler;
    public String mIntialunlockPinScreenStatus = "mIntialunlockPinScreenStatus";
    public String mAddAuthSignDecSignPINPostScreenStatus = "mAddAuthSignDecSignPINPostScreenStatus";
    String authorzation_tokentype, mSerialNumber, CivilNo;

    public static int SPLASH_TIME_OUT = 500;
    Boolean pinlockedBooleanType;
    String mUnlockCode;
    public UnlockPinRequest request;
    public IReturnChallengeResponseAndUserPin returnUserPinCallback;

    static public String UNLOCK_PIN_SUCCESS = "unlock_pin_success";
    static public String UNLOCK_PIN_FAILED = "unlock_pin_failed";
    static public String UNLOCK_PIN_ERROR = "unlock_pin_error";

    private ArrayList<AddActivityLogModel> addActivityLogModelsArrayList;
    public static final String ACTIVITY_LOGS_PIN_REQ = "ACTIVITY_LOGS_PIN_REQ";
    public static final String ACTIVITY_LOGS_PIN_REQ_FAIL = "ACTIVITY_LOGS_PIN_REQ_FAIL";
    public static final String ACTIVITY_LOGS = "ACTIVITY_LOGS";

    Context mContext;
    byte[] mSignBytes;
    // public AlertDialog pinDialog;
    Boolean mCallAsyntask = false;

    public AddPinRequestScreen() {
    }

    public static AddPinRequestScreen newInstance(String msg, String mStatus) {
        AddPinRequestScreen fragment = new AddPinRequestScreen();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        if (msg != null) {
            mQrData = msg;
            mStatusScreen = mStatus;
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = super.onCreateView(inflater, container, savedInstanceState);
        if (mStatusScreen != null && mStatusScreen.equals("ManageScreen")) {
            String[] a = mQrData.split(",");
            String s1 = a[0];
            String[] b = s1.split(":");
            mTransID = b[1];
            mTransID = mTransID.replace("\"", "");
            //Log.d("s2", mTransID);//PACMD-1a9672db-237e-4dfa-829a-e5167a64cb3b
            mTransID = mTransID.replace("PACMD-", "");
            String s3 = a[1];
            String[] b1 = s3.split(":");
            String s4 = b1[1];
            mChallengeIDget(s4);
            // Log.d("s4", s4);
        }
        initialise(getActivity());
        localBroadcastManager = LocalBroadcastManager.getInstance(Objects.requireNonNull(getActivity()));
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(HeadlessUnlockPinCallbackHandler.UNLOCK_PIN_SUCCESS)) {
                    // mResultField.setText("unlock successful");
                    //mStatusField.setText(R.string.label_pin_not_locked);
                }
                if (intent.getAction().equals(HeadlessUnlockPinCallbackHandler.UNLOCK_PIN_FAILED)) {
                    String errorDescription = intent.getStringExtra("description");
                    //mResultField.setText("Error: " + errorDescription);
                }
                if (intent.getAction().equals(HeadlessUnlockPinCallbackHandler.UNLOCK_PIN_ERROR)) {
                    String errorDescription = intent.getStringExtra("description");
                    //mResultField.setText("Error: " + errorDescription);
                }
            }
        };
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAddHomeInteractionListener) {
            mListener = (OnAddHomeInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAddHomeInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setHeader(ViewGroup container, LayoutInflater inflater) {
        View header;
        header = inflater.inflate(R.layout.add_home_header_fragment, container, false);
        mHeader.setVisibility(View.GONE);
        mHeader.addView(header);
    }

    @Override
    public void setBody(ViewGroup container, LayoutInflater inflater) {
        View profileBody = inflater.inflate(R.layout.fragment_home_enter_pin, container, false);
        pinlocked = profileBody.findViewById(R.id.pinlocked);
        pinlockedBold = profileBody.findViewById(R.id.pinlocked_bold);
        // mPinUnlockedLL = profileBody.findViewById(R.id.Pin_unlock_ll);
        mPlsEnterHeadertxt = profileBody.findViewById(R.id.header_enter_pin_edtxt);
        mEnterPinValue = profileBody.findViewById(R.id.et_new_pin);
        mConfirmPInED = profileBody.findViewById(R.id.et_confrm_pin);
        mConfirmRequestPinBtn = profileBody.findViewById(R.id.confirm_request_pin);
        mItisHighlytxt = profileBody.findViewById(R.id.it_is_highly_txt);
        mPinMustBetxt = profileBody.findViewById(R.id.pin_must_txt);
        mOldPinViewLL = profileBody.findViewById(R.id.pin_old_layout_root);
        mConfirmPinViewLL = profileBody.findViewById(R.id.pin_confrm_layout_root);
        mEnterPinViewLL = profileBody.findViewById(R.id.pin_new_layout_root);
        mProgressbarll = profileBody.findViewById(R.id.progress_bar_ll);
        mPlsEnterHeadertxt.setText(R.string.AuthoPIN_Content_one);
        mItisHighlytxt.setVisibility(View.INVISIBLE);
        mPinMustBetxt.setVisibility(View.INVISIBLE);
        mConfirmPinViewLL.setVisibility(View.INVISIBLE);
        mOldPinViewLL.setVisibility(View.GONE);
        mBody.setVisibility(View.GONE);
        mFullFrame.setVisibility(View.VISIBLE);
        setImageResource();
        mContentView.setVisibility(View.VISIBLE);
        mConfirmRequestPinBtn.setOnClickListener(this);
        if (mStatusScreen != null && mStatusScreen.equals("ManageScreen")) {
            mPlsEnterHeadertxt.setText(R.string.Unlock_PIN_content);
            mConfirmPinViewLL.setVisibility(View.VISIBLE);
            mItisHighlytxt.setVisibility(View.VISIBLE);
            mPinMustBetxt.setVisibility(View.VISIBLE);
            pinlocked.setVisibility(View.GONE);
        }
        mFullFrame.addView(profileBody);
    }

    @Override
    public void setFooter(ViewGroup container) {
        super.setFooter(container);
        mPreviousButton.setVisibility(View.GONE);
        mNextButton.setVisibility(View.GONE);
        mInfoButton.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.confirm_request_pin:
                mConfirmRequestPinBtn.setEnabled(false);
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                mConfirmPinStr = preferences.getString(("mConfirmPinED"), "default");
                if (mStatusScreen != null && mStatusScreen.equals("ManageScreen")) {
                    if (mEnterPinValue.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else if ((mConfirmPInED.getText().toString().length() <= 0)) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else if (Pattern.matches("^(0123|1234|2345|3456|4567|5678|6789|3210|4321|5432|6543|7654|8765|9876|0000|1111|2222|3333|4444|5555|6666|7777|8888|9999)$", mEnterPinValue.getText().toString().trim())) {
                        Toast.makeText(getContext(), R.string.s_1_11_consecutive_passcode_alert_txt, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else if (mEnterPinValue.getText().toString().equals(mConfirmPInED.getText().toString())) {
                        if (mConfirmPInED.getText().toString().length() <= 3) {
                            Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                            mConfirmRequestPinBtn.setEnabled(true);
                        } else {
                            // Toast.makeText(getContext(),"suucess", Toast.LENGTH_SHORT).show();
                            getDataFromServerManageQR();
                        }
                    } else {
                        Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    if (mEnterPinValue.getText().toString().length() <= 0) {
                        Toast.makeText(getContext(), R.string.Please_enter_your_PIN, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else if (mEnterPinValue.getText().toString().length() <= 3) {
                        Toast.makeText(getContext(), R.string.Your_PIN_and_Confirm_PIN_do_not_match, Toast.LENGTH_SHORT).show();
                        mConfirmRequestPinBtn.setEnabled(true);
                    } else {
                        if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                            getDataFromServer();
                        } else {
                            getDataFromServer();
                           /* //Toast.makeText(getContext(), "wrongPIN", Toast.LENGTH_SHORT).show();
                            boolean exceptionThrown = false;
                            try {
                                Authentication auth = new Authentication(getActivity());
                                auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                            } catch (UnlicencedException | PinLockedException | IdentityDoesNotExistException | NewPinMustBeDifferentException | PinPolicyNotMetException e) {
                                e.printStackTrace();
                            } catch (IncorrectPinException e) {
                                exceptionThrown = true;
                                StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                                errorMsg.append("\n");
                                errorMsg.append(e.numberOfAttemptsRemaining);
                                errorMsg.append(" attempts remaining.");
                                Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                            if (!exceptionThrown) {
                                //result.setText(getString(R.string.pin_successfully_changed));
                                if (Utilities.isNetworkAvailable(getActivity())) {
                                    getDataFromServer();
                                }
                            }*/

                        }
                    }
                    // Toast.makeText(getContext(), "other screen", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void getDataFromServerManageQR() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mConfirmPinStr = preferences.getString("mConfirmPinED", "default");
            initialise(getActivity());
            offlineUnlockPin();
            if (mEnterPinValue.getText().toString().equals(mConfirmPInED.getText().toString())) {
                editor.putString(("mConfirmPinED"), (mConfirmPInED.getText().toString()));
                editor.apply(); // Or commit if targeting old devices
                preferencese_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
                editor_splash = preferencese_splash.edit();
                editor_splash.apply();
                mDeviceIdValue = preferences.getString("mCarrierID", "default");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mGetUnlockSdkChallenge();
                    }
                }, SPLASH_TIME_OUT);

            } else {
                Toast.makeText(getActivity(), R.string.Invalid_PIN, Toast.LENGTH_SHORT).show();
                mEnterPinValue.setText("");
                mConfirmPInED.setText("");
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    private void getDataFromServer() {
        try {
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            mConfirmPinStr = preferences.getString(("mConfirmPinED"), "default");
            if (mEnterPinValue.getText().toString().equals(mConfirmPinStr)) {
                if (Utilities.isNetworkAvailable(getActivity())) {
                    mProgressbarll.setVisibility(View.VISIBLE);
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    if (pinlockedBooleanType != null && !pinlockedBooleanType) {
                        mQrData = mQrData.replace("PACMD-", "");
                        mGetAuthRequestDetails(mQrData);
                    } else {
                        Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                /*boolean exceptionThrown = false;
                try {
                    Authentication auth = new Authentication(getActivity());
                    auth.changeUserPin(mConfirmPinStr, mEnterPinValue.getText().toString());
                } catch (IncorrectPinException e) {
                    exceptionThrown = true;
                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                    errorMsg.append("\n");
                    errorMsg.append(e.numberOfAttemptsRemaining);
                    errorMsg.append(" attempts remaining.");
                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    exceptionThrown = true;
                    StringBuilder errorMsg = new StringBuilder(getString(R.string.change_user_pin_exception, e.getClass().getSimpleName()));
                    errorMsg.append("\n");
                    errorMsg.append(e.getLocalizedMessage());
                    Toast.makeText(getActivity(), "" + errorMsg, Toast.LENGTH_SHORT).show();
                }
                if (!exceptionThrown) {
                    //result.setText(getString(R.string.pin_successfully_changed));
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        mProgressbarll.setVisibility(View.VISIBLE);
                        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                        mGetAuthRequestDetails(mQrData);
                    }

                }*/
                mQrData = mQrData.replace("PACMD-", "");
                mGetAuthRequestDetails(mQrData);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setImageResource() {

    }

    public void mGetAuthRequestDetails(final String mQrData) {
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.Identity identity = new kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.Identity(mSerialNumber, CivilNo);
        kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.Data data_object = new kw.gov.paci.PACIMobileID.home.models.MyIDGetAuthenRequestModel.Data(mQrData);
        MyIDGetAuthRequestModel myIDGetAuthRequestModel = new MyIDGetAuthRequestModel(identity, data_object);
        apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
        Call<GetAuthRequestDetailsModel> call = apiService.getAuthRequestDetailsUser(authorzation_tokentype, myIDGetAuthRequestModel);
        call.enqueue(new Callback<GetAuthRequestDetailsModel>() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onResponse(@NonNull Call<GetAuthRequestDetailsModel> call, @NonNull Response<GetAuthRequestDetailsModel> response) {
                try {
                    myResponse = response.body();
                    data = String.valueOf(response.body().getData());
                    if (data != null && data.length() > 0) {
                        editor.putString(("Id"), (Integer.toString(myResponse.getData().getId())));
                        editor.putString(("ServiceProviderId"), (myResponse.getData().getServiceProviderId()));
                        editor.putString(("PromptAr"), (myResponse.getData().getPromptAr()));
                        editor.putString(("PromptEn"), (myResponse.getData().getPromptEn()));
                        editor.putString(("RequestChannelId"), (Integer.toString(myResponse.getData().getRequestChannelId())));
                        // editor.putString(("PersonCivilNo"), (myResponse.getData().getPersonCivilNo()));
                        //  editor.putString(("AdditionalSPData"), (myResponse.getData().getAdditionalSPData()));
                        editor.putString(("Challenge"), (myResponse.getData().getChallenge()));
                        editor.putString(("CallbackURL"), (myResponse.getData().getCallbackURL()));
                        //editor.putString(("ResponseData"), (myResponse.getData().getResponseData()));
                        editor.putString(("StatusId"), (Integer.toString(myResponse.getData().getStatusId())));
                        editor.putString(("CreatedAt"), (myResponse.getData().getCreatedAt()));
                        editor.apply();
                        //mListener.goToAddPinSuccessfulScreen();
                        preferencese_splash = Objects.requireNonNull(getActivity()).getSharedPreferences("splash_pref", MODE_PRIVATE);
                        editor_splash = preferencese_splash.edit();
                        editor_splash.apply();

                        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                        editor = preferences.edit();
                        editor.apply();
                        mDeviceIdValue = preferences.getString(("mCarrierID"), ("default"));
                        mDeviceIdValue = (mDeviceIdValue);

                        // mRequestIdvalue = preferences.getString(("RequestId"), (Integer.toString(myResponse.getData().getId())));
                        mRequestIdvalue = preferences.getString(("RequestId"), (Integer.toString(myResponse.getData().getId())));
                        mRequestIdvalue = Integer.toString(myResponse.getData().getId());
                        editor.putString("RequestId", Integer.toString(myResponse.getData().getId()));
                        editor.apply();

                        //mPKCS7Value = preferences.getString(("PKCS7"), (myResponse.getData().getChallenge()));
                        mPKCS7Value = myResponse.getData().getChallenge();

                        mStatusIdValue = preferences.getString(("StatusId"), (Integer.toString(myResponse.getData().getStatusId())));
                        mStatusIdValue = (mStatusIdValue);

                        if (Utilities.isNetworkAvailable(getActivity())) {
                            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                            initialise(getActivity());
                            pinlockedBooleanType = isPinLocked();
                            if (pinlockedBooleanType) {
                                // mAuthenticateRequestSign(mDeviceIdValue, mRequestIdvalue, mPKCS7Value, mStatusIdValue);
                                Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();
                            } else {
                                // Toast.makeText(getActivity(), R.string.PIN_is_currently_locked_Please_unlcok_your_PIN, Toast.LENGTH_SHORT).show();
                                mAuthenticateRequestSign(mDeviceIdValue, mRequestIdvalue, mPKCS7Value, mStatusIdValue);
                            }
                        }

                    } else if (data == null) {
                        setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                    } else {
                        setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<GetAuthRequestDetailsModel> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                    setFragment(R.id.root_content, AddAuthenticateScreen.newInstance(), AddAuthenticateScreen.class.getSimpleName());
                }
            }
        });
    }

    private void mAuthenticateRequestSign(String mDeviceIdValue, String mRequestIdvalue, String mPKCS7Value, String mStatusIdValue) {

        try {
            sigBytes = mPKCS7Value.getBytes("UTF-8");
            //mMyIDAuthenSignData(sigBytes);
            new SignDataAsyntask(sigBytes).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

   /* private void mMyIDAuthenSignData(byte[] sigBytes) {
        try {
            myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(getActivity()));
            myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
            myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
            myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
            signingParameters = new MyIdSigningParameters();
            signingParameters.algorithm = SigningAlgorithm.SHA256_PKCS15_PKCS7;
            authenticationParameters = new MyIdAuthenticationParameters();
            authenticationParameters.permittedAuthentication = MyIdAuthenticationMechanisms.PIN_ONLY;
            authenticationParameters.pinCallback = this;
            myIDSecurityLibrary.signData(sigBytes, signingParameters, authenticationParameters, this);
        } catch (InvalidContextException | PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }*/


    private void mAuthenticateRequestSignPost(String mDeviceIdValue, String mRequestIdvalue, String sign, String mStatusIdValue) {
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Identity identity_oj = new kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Identity(mSerialNumber, CivilNo);
        kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Data data_obj = new kw.gov.paci.PACIMobileID.home.models.MyIDpostAuthenticateRequest.Data(mDeviceIdValue, mRequestIdvalue, sign, mStatusIdValue);
        MyIDpostAuthenticateRequestModel myIDpostAuthenticateRequestModel = new MyIDpostAuthenticateRequestModel(identity_oj, data_obj);
        Call<JsonObject> call = apiService.postAuthenticateRequest(authorzation_tokentype, myIDpostAuthenticateRequestModel);
        call.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mProgressbarll.setVisibility(View.GONE);
                    // data = String.valueOf(response.body().getData());
                    if (response.isSuccessful()) {
                        // Toast.makeText(getActivity(), "Success", Toast.LENGTH_SHORT).show();
                        Date today = new Date();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                        String dateToStr = format.format(today);
                        System.out.println(dateToStr);
                        MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.activity_log_document_authe_signed_succesfully), dateToStr));
                        saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                        String failStatus = "successStatus";
                        AddPinSuccessfulScreen argumentFragment = new AddPinSuccessfulScreen();//Get Fragment Instance
                        Bundle data = new Bundle();//Use bundle to pass data
                        data.putString("failStatus", failStatus);//put string, int, etc in bundle with a key value
                        argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                        setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance(failStatus),
                                AddPinSuccessfulScreen.class.getSimpleName());
                    } else {
                        String failStatus = "failStatus";
                        //Toast.makeText(getActivity(), "failsign", Toast.LENGTH_SHORT).show();
                        // mListener.goToAddPinSuccessfulScreen();
                        Date today = new Date();
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                        String dateToStr = format.format(today);
                        System.out.println(dateToStr);
                        MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.activity_log_document_authe_cancel_signed_succesfully), dateToStr));
                        saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);
                        AddPinSuccessfulScreen argumentFragment = new AddPinSuccessfulScreen();//Get Fragment Instance
                        Bundle data = new Bundle();//Use bundle to pass data
                        data.putString("failStatus", failStatus);//put string, int, etc in bundle with a key value
                        argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                        setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance(failStatus),
                                AddPinSuccessfulScreen.class.getSimpleName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    //message = "Socket Time out. Please try again.";
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    protected void setFragment(int fragmentContainer, Fragment fragment, String id) {
        try {
            FragmentTransaction fragmentTransaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            fragmentTransaction.add(fragmentContainer, fragment, id);
            //fragmentTransaction.commit();
            fragmentTransaction.commitAllowingStateLoss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mChallengeIDget(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == '}') {
            mChallangeID = str.substring(0, str.length() - 1).replace("\"", "");
            //Log.d("str", mChallangeID);
        }
    }

    private void mIntialPINUnLock(String mDeviceIdValue, String mTransID, String mSdkChallenge, String mChallangeID) {
        preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
        editor.putString("mManageScreenTransID", mTransID);
        editor.apply();
        mProgressbarll.setVisibility(View.VISIBLE);
        authorzation_tokentype = preferences.getString("authorzation_tokentype", "default");
        mSerialNumber = preferences.getString(("mCarrierID"), "default");
        CivilNo = preferences.getString("CivilNo", "default");
        Identity identity = new Identity(mSerialNumber, CivilNo);
        Data data_obj = new Data(mDeviceIdValue, (mTransID), mSdkChallenge, mChallangeID);
        MyIDInitialPinUnlockModel myIDInitialPinUnlockModel = new MyIDInitialPinUnlockModel(identity, data_obj);
        Call<JsonObject> call = apiService.mInitiatePINUnlockPost(authorzation_tokentype, myIDInitialPinUnlockModel);
        call.enqueue(new Callback<JsonObject>() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onResponse(@NonNull Call<JsonObject> call, @NonNull Response<JsonObject> response) {
                try {
                    mProgressbarll.setVisibility(View.GONE);
                    if (response.code() == 200) {
                        if (response.isSuccessful()) {
                            String mdeviceType = response.body().get("Data").getAsJsonObject().get("DeviceTypeName").toString().replace("\"", "");
                            String mSerialNumbere = response.body().get("Data").getAsJsonObject().get("SerialNumber").toString().replace("\"", "");
                            mUnlockCode = response.body().get("Data").getAsJsonObject().get("UnlockCode").toString().replace("\"", "");
                            //AddPinRequestScreen.headlessUnlockPinCallbackHandler.setCurrentHeadlessContext(getActivity());
                            setCurrentHeadlessContext(getActivity());
                            onRequireChallengeResponseAndUserPin(request, returnUserPinCallback, null);
                          /*  Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.RequestPinUnlock), getString(R.string.initial_pin_lock), getString(R.string.pinlock), getString(R.string.successfully_txt), dateToStr));
                            saveToSharedPreference(addActivityLogModelsArrayList);*/
                           /* Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.Your_Pin_Unlocked_Successfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);*/

                            String failStatus = "successStatusManageScreen";
                            AddPinSuccessfulScreen argumentFragment = new AddPinSuccessfulScreen();//Get Fragment Instance
                            Bundle data = new Bundle();//Use bundle to pass data
                            data.putString("successStatusManageScreenID", failStatus);//put string, int, etc in bundle with a key value
                            argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                            setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance(failStatus),
                                    AddPinSuccessfulScreen.class.getSimpleName());
                        } else {
                           /* Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            addActivityLogModelsArrayList = new ArrayList<>();
                            addActivityLogModelsArrayList.add(new AddActivityLogModel(getString(R.string.RequestPinUnlock), getString(R.string.initial_pin_lock), getString(R.string.pinlock), getString(R.string.failed_txt), dateToStr));
                            saveToSharedPreferenceFail(addActivityLogModelsArrayList);*/

                          /*  Date today = new Date();
                            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.ENGLISH);
                            String dateToStr = format.format(today);
                            System.out.println(dateToStr);
                            MobileCivilIDApplication.mGlobalAddactivitylogArraylist.add(new AddActivityLogModel(getString(R.string.activity_log_title_authenticate), getString(R.string.activity_log_msubtitle_status_auth_sign), "", getString(R.string.Your_Pin_Unlocked_Successfully), dateToStr));
                            saveToSharedPreference(MobileCivilIDApplication.mGlobalAddactivitylogArraylist);*/

                            String failStatus = "successStatusManageScreen";
                            AddPinSuccessfulScreen argumentFragment = new AddPinSuccessfulScreen();//Get Fragment Instance
                            Bundle data = new Bundle();//Use bundle to pass data
                            data.putString("successStatusManageScreenID", failStatus);//put string, int, etc in bundle with a key value
                            argumentFragment.setArguments(data);//Finally set argument bundle to fragment
                            setFragment(R.id.root_content, AddPinSuccessfulScreen.newInstance(failStatus),
                                    AddPinSuccessfulScreen.class.getSimpleName());
                            Toast.makeText(getActivity(), String.valueOf(response.errorBody()), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getActivity(), String.valueOf(response.errorBody()), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                //Log.d("onFailure_pelasewait:", t.getLocalizedMessage());
                if (t instanceof SocketTimeoutException) {
                    Toast.makeText(getActivity(), "Socket Time out. Please try again.", Toast.LENGTH_SHORT).show();
                    setFragment(R.id.root_content, AddManageScreen.newInstance(), AddManageScreen.class.getSimpleName());
                }
            }
        });
    }

    private void saveToSharedPreference(ArrayList<AddActivityLogModel> addActivityLogModelsArrayList) {
        Stash.put(ACTIVITY_LOGS, addActivityLogModelsArrayList);
    }

    /*@Override
    public void signingSuccess(MyIdSignDataResponse signDataResponse) {
        // Log.d("signdataResp", "" + signDataResponse);

        if (signDataResponse.signedData == null) {
            Toast.makeText(getActivity(), "Data Was Not Signed", Toast.LENGTH_SHORT).show();
        } else {
            byte[] signedBytes = signDataResponse.signedData;
            try {
                sign = CSRHelper.encryptBASE64(signedBytes);
            } catch (Exception e) {
                e.printStackTrace();
            }
            preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
            editor = preferences.edit();
            editor.apply();
            editor.putString("DigitalSignQr", sign);
            editor.apply();
            Log.d("==sign1==", sign);
            if (signDataResponse.certificateData != null) {
                // Log.d("==sign1==", ""+signDataResponse.certificateData);
            } else {
                Toast.makeText(getActivity(), "No certificate data returned", Toast.LENGTH_SHORT).show();
            }
            if (Utilities.isNetworkAvailable(getActivity())) {
                tokenIdentity = new TokenIdentity(getActivity());
                tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecSignPINPostScreenStatus, mGrant_type, mUserName, mPassword, this);

            }
        }
    }

    @Override
    public void signingError(Exception e) {
        Toast.makeText(getActivity(), "Error:" + e, Toast.LENGTH_SHORT).show();
    }*/

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        initialise(getActivity());
        //pinlockedBooleanType = headlessUnlockPinCallbackHandler.isPinLocked();
        pinlockedBooleanType = isPinLocked();
        if (!pinlockedBooleanType) {
            //   mPinUnlockedLL.setVisibility(View.GONE);
            pinlocked.setVisibility(View.GONE);
        } else {
            // mPinUnlockedLL.setVisibility(View.VISIBLE);
            if (mStatusScreen != null && mStatusScreen.equals("ManageScreen")) {
                pinlocked.setVisibility(View.GONE);
            } else {
                pinlocked.setVisibility(View.VISIBLE);
                pinlocked.setText(R.string.PIN_is_currently_locked_Please_unlcok_your_PIN);
                Animation anim = new AlphaAnimation(0.0f, 1.0f);
                anim.setDuration(500);
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                pinlocked.startAnimation(anim);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (myIDSecurityLibrary != null) {
            myIDSecurityLibrary.onStop();
            myIDSecurityLibrary = null;
        }
        localBroadcastManager.unregisterReceiver(receiver);
    }

    private void mGetUnlockSdkChallenge() {
        if (Utilities.isNetworkAvailable(getActivity())) {
            apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
            //AddPinRequestScreen.headlessUnlockPinCallbackHandler.setCurrentHeadlessContext(getActivity());
            setCurrentHeadlessContext(getActivity());
            mSdkChallenge = preferences.getString("challengeUnlock", "default");
            if (mSdkChallenge.equals("default")) {
                Toast.makeText(getActivity(), "challenge not found", Toast.LENGTH_SHORT).show();
            } else {
                apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                if (Utilities.isNetworkAvailable(getActivity())) {
                    mIntialPINUnLock(mDeviceIdValue, mTransID, mSdkChallenge, mChallangeID);
                }
                // tokenIdentity = new TokenIdentity(getActivity());
                //tokenIdentity.mGetAccessTokenRequest(mIntialunlockPinScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
                // mIntialPINUnLock(mDeviceIdValue, mTransID, mSdkChallenge, mChallangeID);
            }
        }
    }

    @Override
    public void onOfflineUnlockSuccess() {
        localBroadcastManager.sendBroadcast(new Intent(UNLOCK_PIN_SUCCESS));
    }

    @Override
    public void onOfflineUnlockFail(Exception e) {
        Intent failedIntent = new Intent(UNLOCK_PIN_FAILED);
        failedIntent.putExtra("description", e.getLocalizedMessage());
        localBroadcastManager.sendBroadcast(failedIntent);
    }

    @Override
    public void onRequireChallengeResponseAndUserPin(UnlockPinRequest unlockPinRequest, IReturnChallengeResponseAndUserPin iReturnChallengeResponseAndUserPin, Exception e) {
        this.returnUserPinCallback = iReturnChallengeResponseAndUserPin;
        this.request = unlockPinRequest;
        if (this.request != null) {
            try {
                if (request.deviceType != null) {
                    editor.putString("deviceType", request.deviceType);
                    editor.putString("serialNumber", request.serialNumber);
                    editor.putString("challengeUnlock", request.challenge);
                    editor.apply();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (mUnlockCode != null) {
                this.returnUserPinCallback.setResult(mUnlockCode, mConfirmPInED.getText().toString());
            }
        }
    }

    public void initialise(Context mainContext) {
        try {
            authentication = new Authentication(mainContext);
        } catch (UnlicencedException e) {
            e.printStackTrace();
        }
    }

    public void offlineUnlockPin() {
        authentication.offlineUnlockPin(this);
    }

    public boolean isPinLocked() {
        return authentication.isPinLocked();
    }

    public void setCurrentHeadlessContext(Context context) {
        mContext = context;
        onRequireChallengeResponseAndUserPin(request, returnUserPinCallback, null);
    }


    @SuppressLint("StaticFieldLeak")
    public class SignDataAsyntask extends AsyncTask<Void, Void, Void> implements ISigningResult, IPinCallback {
        byte[] mSignBytes;
        int corePoolSize = 60;
        int maximumPoolSize = 80;
        int keepAliveTime = 10;
        int count = 0;
        //public AlertDialog pinDialog;

        public SignDataAsyntask(byte[] sigBytes) {
            this.mSignBytes = sigBytes;

        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                myIDSecurityLibrary = MyIDSecurityLibrary.onStart(Objects.requireNonNull(getActivity()));
                myIDSecurityLibrary.identitySource(MyIDSecurityLibrary.SecurityLibraryIdentitySourcePreference.softwareKeystore);
                myIDSecurityLibrary.loggingLevel(MyIDSecurityLibrary.SecurityLibraryLogging.infoLogging);
                myIDSecurityLibrary.authenticationType(MyIDSecurityLibrary.SecurityLibraryAuthenticationPreference.keyboardPin);
                signingParameters = new MyIdSigningParameters();
                signingParameters.algorithm = SigningAlgorithm.SHA256_PKCS15_PKCS7;
                authenticationParameters = new MyIdAuthenticationParameters();
                authenticationParameters.permittedAuthentication = MyIdAuthenticationMechanisms.PIN_ONLY;
                authenticationParameters.pinCallback = this;
                myIDSecurityLibrary.signData(mSignBytes, signingParameters, authenticationParameters, this);
            } catch (InvalidContextException | PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mCallAsyntask = true;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            cancel(true);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mProgressbarll.setVisibility(View.GONE);

        }

        @Override
        protected void onCancelled(Void aVoid) {
            super.onCancelled(aVoid);
            cancel(true);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            if (mCallAsyntask) {
                new SignDataAsyntask(sigBytes).execute();
            }
        }

        @Override
        public void provideUserPin(final IReturnUserPin callback) {
           /* AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
            if (!callback.isFirstAttempt()) {
                dialog.setTitle(R.string.Invalid_PIN);
                dialog.setMessage("\n" + callback.attemptsRemaining());
                count = 1;
            } else if (callback.isFirstAttempt()) {
                callback.setResult(mEnterPinValue.getText().toString());
            }
            dialog.setCancelable(false);
            if (count == 1) {
                dialog.setPositiveButton(R.string.OK, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // pinDialog = null;
                        dialog.cancel();
                        callback.setResult("");
                        dialog.dismiss();
                        dialog.cancel();
                        mConfirmRequestPinBtn.setEnabled(true);
                    }
                });
            }
            if (count == 1) {
                dialog.show();
            }*/
            final Dialog alert = new Dialog(Objects.requireNonNull(getActivity()));
            Objects.requireNonNull(alert.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alert.setContentView(R.layout.custom_dialog_wrong_pin);
            final CustomTextView mCountText = alert.findViewById(R.id.count_remaining);
            final CustomButton mOkBtn = alert.findViewById(R.id.ok_btn);

            if (!callback.isFirstAttempt()) {
                mCountText.setText(String.valueOf(callback.attemptsRemaining()));
                count = 1;
            } else if (callback.isFirstAttempt()) {
                callback.setResult(mEnterPinValue.getText().toString());
            }
            alert.setCancelable(false);
            if (count == 1) {
                mOkBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.cancel();
                        callback.setResult("");
                        alert.dismiss();
                        alert.cancel();
                        mConfirmRequestPinBtn.setEnabled(true);
                    }
                });
            }
            if (count == 1) {
                alert.show();
            }
        }

        @Override
        public void signingSuccess(MyIdSignDataResponse signDataResponse) {
            if (signDataResponse.signedData == null) {
                Toast.makeText(getActivity(), "Data Was Not Signed", Toast.LENGTH_SHORT).show();
            } else {
                byte[] signedBytes = signDataResponse.signedData;
                try {
                    sign = CSRHelper.encryptBASE64(signedBytes);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                preferences = Objects.requireNonNull(getActivity()).getSharedPreferences("plswait", MODE_PRIVATE);
                editor = preferences.edit();
                editor.apply();
                editor.putString("DigitalSignQr", sign);
                editor.apply();
                //Log.d("==sign1==", sign);
                if (signDataResponse.certificateData != null) {
                    // Log.d("==sign1==", ""+signDataResponse.certificateData);
                } else {
                    Toast.makeText(getActivity(), "No certificate data returned", Toast.LENGTH_SHORT).show();
                }
                if (Utilities.isNetworkAvailable(getActivity())) {
                    // tokenIdentity = new TokenIdentity(getActivity());
                    //  tokenIdentity.mGetAccessTokenRequest(mAddAuthSignDecSignPINPostScreenStatus, BuildConfig.mGrant_type, BuildConfig.mUserName, BuildConfig.mPassword, this);
                    apiService = ApiClientURL.getClient().create(RetroApiInterface.class);
                    if (Utilities.isNetworkAvailable(getActivity())) {
                        //mAuthenticateRequestSignPost(mDeviceIdValue, mRequestIdvalue, sign, "3");
                        mAuthenticateRequestSignPost(mDeviceIdValue, mRequestIdvalue, sign, mStatusIdValue);
                    }
                }
            }
        }

        @Override
        public void signingError(Exception e) {
            Toast.makeText(getActivity(), "Error:" + e, Toast.LENGTH_SHORT).show();
        }
    }
}