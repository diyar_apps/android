
package kw.gov.paci.PACIMobileID.home.models.MyIDpostGetAuthIdentityQR;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDpostGetAuthIdentityQRModel {

    @SerializedName("Identity")
    @Expose
    private Identity identity;
    @SerializedName("Data")
    @Expose
    private Data data;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MyIDpostGetAuthIdentityQRModel() {
    }

    /**
     * 
     * @param identity
     * @param data
     */
    public MyIDpostGetAuthIdentityQRModel(Identity identity, Data data) {
        super();
        this.identity = identity;
        this.data = data;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
