
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.MyIDpostFinalizeEntrollModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MyIDFinalEntrollModel {

    @SerializedName("Identity")
    @Expose
    private Identity identity;
    @SerializedName("Data")
    @Expose
    private Data data;

    /**
     * No args constructor for use in serialization
     * 
     */
    public MyIDFinalEntrollModel() {
    }

    /**
     * 
     * @param identity
     * @param data
     */
    public MyIDFinalEntrollModel(Identity identity, Data data) {
        super();
        this.identity = identity;
        this.data = data;
    }

    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

    public Object getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}
