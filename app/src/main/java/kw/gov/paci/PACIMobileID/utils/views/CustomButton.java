package kw.gov.paci.PACIMobileID.utils.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.utils.Utilities;

/**
 * Created by VAMANPALLI SUNIL KUMAR on 09/08/2018.
 */
public class CustomButton extends AppCompatButton {
    public CustomButton(Context context) {
        super(context);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    /**
     * To set Custom Font from xml Attribute
     *
     * @param ctx
     * @param attrs
     */
    private void setCustomFont(Context ctx, AttributeSet attrs) {

        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.DIYARTextView);
        String font = a.getString(R.styleable.DIYARTextView_default_font);

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), font);
        } catch (Exception e) {
            Utilities.sendException(e);
        }

        setTypeface(tf);
        a.recycle();
    }
}
