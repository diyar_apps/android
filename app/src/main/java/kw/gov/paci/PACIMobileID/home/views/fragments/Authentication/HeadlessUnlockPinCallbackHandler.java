package kw.gov.paci.PACIMobileID.home.views.fragments.Authentication;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import kw.gov.paci.PACIMobileID.R;
import com.intercede.Authentication;
import com.intercede.IReturnChallengeResponseAndUserPin;
import com.intercede.OfflineUnlockCallback;
import com.intercede.PinPolicy;
import com.intercede.UnlockPinRequest;
import com.intercede.myIDSecurityLibrary.UnlicencedException;

import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by nitin.surti on 26/07/2018.
 */

public class HeadlessUnlockPinCallbackHandler implements OfflineUnlockCallback {

    static public String UNLOCK_PIN_SUCCESS = "unlock_pin_success";
    static public String UNLOCK_PIN_FAILED = "unlock_pin_failed";
    static public String UNLOCK_PIN_ERROR = "unlock_pin_error";

    private LocalBroadcastManager localBroadcastManager;
    Context mContext;
    private IReturnChallengeResponseAndUserPin returnUserPinCallback;
    private Authentication authentication = null;
    private UnlockPinRequest request;

    public SharedPreferences preferences;
    public SharedPreferences.Editor editor;


    public HeadlessUnlockPinCallbackHandler(Context mainContext) {
        localBroadcastManager = LocalBroadcastManager.getInstance(mainContext);
        preferences = mainContext.getSharedPreferences("plswait", MODE_PRIVATE);
        editor = preferences.edit();
        editor.apply();
    }

    public void setCurrentHeadlessContext(Context context) {
        mContext = context;
        onRequireChallengeResponseAndUserPin(request, returnUserPinCallback, null);
    }

    public void initialise(Context mainContext) {
        try {
            authentication = new Authentication(mainContext);
        } catch (UnlicencedException e) {
            e.printStackTrace();
        }
    }

    public void offlineUnlockPin() {
        authentication.offlineUnlockPin(this);
    }


    public boolean isPinLocked() {
        return authentication.isPinLocked();
    }


    @Override
    public void onOfflineUnlockSuccess() {
        localBroadcastManager.sendBroadcast(new Intent(UNLOCK_PIN_SUCCESS));
    }

    @Override
    public void onOfflineUnlockFail(Exception exception) {

        Intent failedIntent = new Intent(UNLOCK_PIN_FAILED);
        failedIntent.putExtra("description", exception.getLocalizedMessage());
        localBroadcastManager.sendBroadcast(failedIntent);
    }

    @Override
    public void onRequireChallengeResponseAndUserPin(final UnlockPinRequest request,
                                                     final IReturnChallengeResponseAndUserPin callback,
                                                     final Exception exception) {

        this.returnUserPinCallback = callback;
        this.request = request;

        try {
            if (request.deviceType!=null) {
                editor.putString("deviceType", request.deviceType);
                editor.putString("serialNumber", request.serialNumber);
                editor.putString("challengeUnlock", request.challenge);
                editor.apply();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean meetsPinPolicy(final String pin, final PinPolicy pinPolicy) {
        if (pin.length() < pinPolicy.minLength)
            return false;

        return pin.length() <= pinPolicy.maxLength;
    }

    /*void cleanupPinDialog() {
        pinDialog = null;
        request = null;
    }*/

}
