
package kw.gov.paci.PACIMobileID.mobileidverifier.models.MyIDpostGetAuthIdentityData;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("AuthIdentityQR")
    @Expose
    private String authIdentityQR;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Data() {
    }

    /**
     * 
     * @param authIdentityQR
     */
    public Data(String authIdentityQR) {
        super();
        this.authIdentityQR = authIdentityQR;
    }

    public String getAuthIdentityQR() {
        return authIdentityQR;
    }

    public void setAuthIdentityQR(String authIdentityQR) {
        this.authIdentityQR = authIdentityQR;
    }

}
