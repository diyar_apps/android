package kw.gov.paci.PACIMobileID.utils.views;

/**
 * Created by VAMANPALLI SUNIL KUMAR on 09/08/2018.
 */
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.utils.Utilities;

public class CustomTextView extends AppCompatTextView {
    public CustomTextView(Context context) {
        super(context);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context, attrs);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * To set Custom Font from xml Attribute
     *
     * @param ctx
     * @param attrs
     */
    private void setCustomFont(Context ctx, AttributeSet attrs) {

        TypedArray a = ctx.obtainStyledAttributes(attrs, R.styleable.DIYARTextView);
        String font = a.getString(R.styleable.DIYARTextView_default_font);

        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), font);
        } catch (Exception e) {
            Utilities.sendException(e);
        }

        setTypeface(tf);
        a.recycle();
    }
}
