package kw.gov.paci.PACIMobileID.mobileidverifier.views.activitys;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import kw.gov.paci.PACIMobileID.R;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilID;
import kw.gov.paci.PACIMobileID.baseclasses.MobileCivilIDApplication;
import kw.gov.paci.PACIMobileID.mobileidverifier.controllers.OnMobileIDVerifyInteractionListener;
import kw.gov.paci.PACIMobileID.mobileidverifier.views.fragments.MIDHomeScanner;
import kw.gov.paci.PACIMobileID.mobileidverifier.views.fragments.MobileIDVerifyDataDisplay;
import kw.gov.paci.PACIMobileID.utils.views.CustomTextView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Created by SUNIL KUMAR V on 1/28/2019.
 */
public class MobileIDVerifierScanActivity extends MobileCivilID implements OnMobileIDVerifyInteractionListener {
    public String KeyQRcodeStr;
    private CustomTextView mHeaderCenterText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_root_view);
        super.onCreate(savedInstanceState);
        mHeaderCenterText = findViewById(R.id.header_life_coach_txt);
        mHeaderCenterText.setText(R.string.Mobile_ID_Verifier);
        //mHeaderCenterText.setPadding(0, 0, 100, 0);
        setFragment(R.id.root_content, MIDHomeScanner.newInstance(), MIDHomeScanner.class.getSimpleName());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            goToPreviousScreen();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == Activity.RESULT_OK) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
            if (scanResult.getContents() != null) {
                String re = scanResult.getContents();
                KeyQRcodeStr = re;
            }
            MobileIDVerifyDataDisplay argumentFragment = new MobileIDVerifyDataDisplay();//Get Fragment Instance
            Bundle data = new Bundle();//Use bundle to pass data
            data.putString("KeyQrCode", KeyQRcodeStr);//put string, int, etc in bundle with a key value
            argumentFragment.setArguments(data);//Finally set argument bundle to fragment
            replaceFragment(R.id.root_content, MobileIDVerifyDataDisplay.newInstance(KeyQRcodeStr),
                    MobileIDVerifyDataDisplay.class.getSimpleName(), true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        enForcedLocale();
    }

    private void enForcedLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((MobileCivilIDApplication) MobileCivilIDApplication.getContext()).updateLocale();
        }
    }

    @Override
    public void goToAddMIDHomeScanner() {

    }
}
