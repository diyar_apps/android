
package kw.gov.paci.PACIMobileID.aboutmobilecivilid.models.CivilIDPersonalDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MobileCredentialSettings {

    @SerializedName("CredentialRenewalPeriod")
    @Expose
    private Integer credentialRenewalPeriod;

    public Integer getCredentialRenewalPeriod() {
        return credentialRenewalPeriod;
    }

    public void setCredentialRenewalPeriod(Integer credentialRenewalPeriod) {
        this.credentialRenewalPeriod = credentialRenewalPeriod;
    }

}
